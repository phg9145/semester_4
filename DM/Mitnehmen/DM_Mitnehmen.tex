\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[german]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{xcolor}
\usepackage{float}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{listings}
\usepackage{fancyhdr}
\usepackage{tabto}
\usepackage{subcaption}
\usepackage{tcolorbox}
\usepackage{pifont}
\usepackage[a4paper,left=1cm,right=1cm,top=1cm,bottom=1cm,bindingoffset=0mm]{geometry}

\newcommand{\definition}[1]{Definition \textbf{#1}:}



\newcommand{\cmark}{\ding{51}}
\newcommand{\xmark}{\ding{55}}

\newcommand{\modulo}{\>\text{mod}\>}



\newcommand{\datum}[1]{
\begin{flushright}
\date{#1}
\end{flushright}
}

\newcommand{\centergr}[2]{
\begin{center}
\includegraphics[width=#1\linewidth]{../Latex/Bilder/#2}
\end{center}
}


\newcommand{\Landau}{\mathcal{O}}
\newcommand{\landau}{o}

%\fancyfoot[R]{Letzte Änderung: \today}
\pagestyle{empty}


\numberwithin{equation}{section}

\title{
Diskrete Mathematik\\
-\\
Zusammenfassung
}
\author{Philipp von Perponcher}
\date{26.06.2020}

\begin{document}

%\maketitle
%newpage

%\tableofcontents

\newpage
\section{Grundlagen}
\subsection{Wahrscheinlichkeit}
\subsubsection{Bedingte Wahrscheinlichkeit}
\textbf{Definition}:\\
Seien $A$ und $B$ Ereignisse und Pr$(B) > 0$. Die Wahrscheinlichkeit für $A$ \textit{unter der Bedingung} $B$ ist definiert als
\[ \text{Pr}(A|B) = \frac{\text{Pr} (A \cap B)}{\text{Pr}(B)}. \]

\subsubsection{Satz von Bayes}
\textbf{Definition}:\\
Sind $A$ und $B$ Ereignisse mit Pr$(A) > 0$ und Pr$(B) > 0$, so gilt
\[ \text{Pr}(B) \cdot \text{Pr}(A|B) = \text{Pr}(A) \cdot \text{Pr}(B|A). \]

\subsubsection{Zufallsvariable \& Erwartungswert}
\textbf{Definition}:\\
Sei $S$ eine endliche Ergebnismenge mit $\text{Pr} : P(S) \rightarrow \mathbb{R}$ der Wahrscheinlichkeitsverteilung auf $S$. Eine \textit{Zufallsvariable} auf $S$ ist eine Funktion
\[ X : S \rightarrow \mathbb{R}, \]
die jedem Ergebnis einen Wert $X$ zuordnet (z.B. Anzahl der Augen auf einem Würfel bei einem Wurf).\\
Der \textit{Erwartungswert} von $X$ ist definiert als
\[ E[X] = \sum_{s \in S} \text{Pr}(s) \cdot X(s). \]
Beispiel:\\
Sei $S = \{(i,j) : 1 \leq i,j \leq 6 \}$ die Menge aller Ergebnisse eines Wurfs zweier Würfel.\\
Die Funktion
\[ X : S \rightarrow \mathbb{R}, (i,j) \mapsto i+j \]
ist eine Zufallsvariable auf $S$. Sie ordnet jedem Wurf die Summe der beiden gewürfelten Werte zu. Der Erwartungswert ist somit
\[ \begin{split}
E[X] & = \frac{1}{36}*2 + \frac{2}{36}*3 + \frac{3}{36}*4 + \frac{4}{36}*5 + \frac{5}{36}*6 + \frac{6}{36}*7 + \frac{5}{36}*8 + \frac{4}{36}*9 + \frac{3}{36}*10 + \frac{2}{36}*11 + \frac{1}{36}*12 =\\
& = 7
\end{split} \]


\subsection{Erweiterter euklidischer Algorithmus}
\label{euklid}
Der euklidische Algorithmus berechnet den gcd zweier Zahlen $a,b$.\\
Die Erweiterung kann nun auch die Werte für $x$ und $y$ in der Gleichung $x \cdot a + y \cdot b = \text{gcd}(a,b)$ mit $a > b$ bestimmen.\\
Dafür gilt $x = (-1)^k x_k \: \: , \: \: y = (-1)^{k+1} y_k$\\
\ \\
\glqq Standard-Algorithmus\grqq :
\begin{enumerate}
\item \textit{Wenn $b=0$ ist, dann ist} gcd$(a,b) = |a|$
\item \textit{Wenn $b \neq 0$ ist, dann ist} gcd$(a,b) = $ gcd$(|b|, a $ mod $|b|)$.
\end{enumerate}
Schritte des erweiterten Algorithmus zur Bestimmung von $x$ und $y$:
\begin{enumerate}
\item Euklidischen Algorithmus ausführen.
\item Variablen initialisieren: $x_0 = 1, x_1 = 0; y_0 = 0, y_1 = 1$.
\item Variablen bis zum Ende ausfüllen, dem folgenden Schema folgend:\\
$x_{k+1} = x_k \cdot v_k + x_{k-1}$\\
$y_{k+1}$ analog.
\end{enumerate}
\newpage
\noindent
Beispiel für gcd$(565,492)$:\\
\ \\
\textbf{Euklidischer Algorithmus} (Schritt 1):\\
\ \\
\begin{tabular}{lll}
$a$ & = $v_k \cdot b$ & + Rest\\
\hline
565 & = 1 $\cdot$ 492 & + 73\\
492 & = 6 $\cdot$ 73  & + 54\\
73  & = 1 $\cdot$ 54  & + 19\\
54  & = 2 $\cdot$ 19  & + 16\\
19  & = 1 $\cdot$ 16  & + 3\\
16  & = 5 $\cdot$ 3   & + 1\\
3   & = 3 $\cdot$ 1   & + 0\\
\end{tabular}\\
\ \\
\textbf{Tabellen} (Schritte 2 \& 3):\\
\ \\
\begin{minipage}[t]{0.5\linewidth}
\centering
Tabelle 1 (initialisieren)\\
\ \\
\begin{tabular}{c||c|c|c|c|c|c|c|c}
$k$ & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7\\ \hline \hline
$q_k$ & 565 & 492 & 73 & 54 & 19 & 16 & 3 & 1\\ \hline
$v_k$ &  & 1 & 6 & 1 & 2 & 1 & 5 & 3\\ \hline
$x_k$ & 1 & 0 &  &  &  &  &  & \\ \hline
$y_k$ & 0 & 1 &  &  &  &  &  & \\
\end{tabular}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth}
\centering
Tabelle 2 (ausgefüllt)\\
\ \\
\begin{tabular}{c||c|c|c|c|c|c|c|c}
$k$ & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7\\ \hline \hline
$q_k$ & 565 & 492 & 73 & 54 & 19 & 16 & 3 & 1\\ \hline
$v_k$ &  & 1 & 6 & 1 & 2 & 1 & 5 & 3\\ \hline
$x_k$ & 1 & 0 & 1 & 6 & 7 & 20 & 27 & 155\\ \hline
$y_k$ & 0 & 1 & 1 & 7 & 8 & 23 & 31 & 178\\
\end{tabular}
\end{minipage}
\ \\
Beispiel $x_5 = 20$, da $x_{k+1} = x_k \cdot v_k + x_{k-1} = 7 \cdot 2 + 6 = 20$\\
\ \\
Also:\\
$k = 7$\\
$\Rightarrow x = (-1)^7 x_7 = (-1) \cdot 155 = -155 \: \: , \: \: y = (-1)^{7+1} y_7 = 1 \cdot 178 = 178$\\
$\Rightarrow  -155 \cdot 565 + 178 \cdot 492 = 1$

\section{Ordnungen, Gruppen, Verbände}
\subsection{Ordnungen}
\label{ordnungen}
Eine zweistellige Relation $\circ$ auf einer Menge $S$ ist eine Ordnung, falls für alle $s,t,u \in S$ gilt
\begin{itemize}
\item $s \circ s$ (Reflexivität)
\item $s \circ t \wedge t \circ s \Rightarrow s = t$ (Antisymmetrie)
\item $s \circ t \wedge t \circ u \Rightarrow s \circ u$ (Transitivität)
\end{itemize}
\noindent
\textit{Beispielaufgabe}:\\
Entscheiden Sie, ob die Relation $R$ auf der Menge $\mathbb{Z}$ mit $mRn$, gdw. $m+n$ gerade ist, eine Ordnung ist. $(m,n \in \mathbb{Z})$
\begin{itemize}
\item Reflexivität:\\
Ist $mRm$ wahr $\forall m \in \mathbb{Z}$? $\Rightarrow m+m = 2m$ ist eine gerade Zahl \checkmark
\item Antisymmetrie:\\
$(mRn \wedge nRm) \Rightarrow n=m$?\\
Gegenbeispiel: $2R4 \wedge 4R2 \rightarrow 2+4 = 4+2$ würde passen (da es gerade ist), ist aber nicht antisymmetrisch, da $4 \neq 2$.
\end{itemize}
$\Rightarrow$ keine Ordnung

\subsection{Halbgruppe, Monoid, Gruppe}
\label{halbgruppe_monoid_gruppe}
Es werden gegebene Verknüpfungen untersucht.
\subsubsection{Halbgruppe}
\textit{Kriterium}: Assoziativität $\Rightarrow$ $(a \circ b) \circ c = a \circ (b \circ c)$

\subsubsection{Monoid}
\textit{Kriterium}: Halbgruppe gegeben \& neutrales Element $e$ $\Rightarrow$ $a \circ e = e \circ a = a$

\subsubsection{Gruppe}
\textit{Kriterium}: Monoid \& jedes Element invertierbar $\Rightarrow$ mit $e$ = neutrales Element und $b$ = Inverse von $a$ muss gelten: $a \circ b = b \circ a = e$

\subsubsection{Beispiele}
\begin{minipage}[t]{0.3\linewidth}
$0 \circ 0 = 1$\\
$0 \circ 1 = 0$\\
$1 \circ 0 = 1$\\
$1 \circ 1 = 1$\\
\ \\
$(0 \circ 1) \circ 0 = 0 \circ 0 = 1$\\
$0 \circ (1 \circ 0) = 0 \circ 1 = 0$\\
Nicht assoziativ!\\
\ \\
$\Rightarrow$ keine Halbgruppe,\\
kein Monoid,\\
keine Gruppe
\end{minipage}
\begin{minipage}[t]{0.3\linewidth}
$0 \circ 0 = 0$\\
$0 \circ 1 = 1$\\
$1 \circ 0 = 0$\\
$1 \circ 1 = 1$\\
\ \\
$(0 \circ 1) \circ 0 = 1 \circ 0 = 0$\\
$0 \circ (1 \circ 0) = 0 \circ 0 = 0$\\
Assoziativ \checkmark\\
\ \\
neutrales Element?\\
$a \circ e = e \circ a = a$?\\
Nein!\\
\ \\
$\Rightarrow$ Halbgruppe,\\
kein Monoid,\\
keine Gruppe
\end{minipage}
\begin{minipage}[t]{0.3\linewidth}
$0 \circ 0 = 0$\\
$0 \circ 1 = 0$\\
$1 \circ 0 = 0$\\
$1 \circ 1 = 1$\\
\ \\
$(0 \circ 1) \circ 0 = 0 \circ 0 = 0$\\
$0 \circ (1 \circ 0) = 0 \circ 0 = 0$\\
Assoziativ \checkmark\\
\ \\
neutrales Element \checkmark\\
$e = 1$\\
\ \\
$\Rightarrow$ Halbgruppe,\\
Monoid,\\
aber keine Gruppe, da nicht invertierbar
\end{minipage}

\subsection{Gruppen}
\subsubsection{Ordnung von primen Restklassen}
\label{ordnung_klasse}
Die Gruppe der primen Restklassen modulo $m$ heißt \textit{prime Restklassengruppe} modulo $m$ und wird mit $(\mathbb{Z}/m\mathbb{Z})^*$ bezeichnet. Ihre Ordnung bezeichnet man mit $\varphi (m)$.\\
$\varphi (m)$ ist die Anzahl der Zahlen $a$ in $\{1,2,\ldots,m\}$ mit gcd$(a,m) = 1$.\\
\ \\
Beispiel: $(\mathbb{Z}/ 12 \mathbb{Z})^* = \{1 + 12 \mathbb{Z}, 5 + 12 \mathbb{Z}, 7 + 12 \mathbb{Z}, 11 + 12 \mathbb{Z} \}$ist die prime Restklassengruppe mod 12. Also ist $\varphi (12) = 4$.\\
\ \\
Weitere Berechnungen für $\varphi$:
\begin{enumerate}
\item Wenn $m$ eine Primzahl ist, dann gilt
\[ \varphi (p) = p-1. \]
Beispiel: $\varphi (13) = 12$
\item \[ \varphi (m) = m \cdot \prod_{p|n} \left( 1- \frac{1}{p} \right), \]
wobei $p$ die Primfaktoren von $m$ sind.\\
\ \\
Beispiel:\\
$640 = 2 \cdot 2 \cdot 2 \cdot 2 \cdot 2 \cdot 2 \cdot 2 \cdot 5 = 2^7 \cdot 5^1$
\[ \varphi (640) = 640 \cdot \left( 1 - \frac{1}{2} \right) \cdot \left( 1 - \frac{1}{5} \right) = 640 \cdot \frac{1}{2} \cdot \frac{4}{5} = 256 \]
\end{enumerate}

\subsubsection{Ordnung von Gruppenelementen}
\label{ordnung_elemente}
Gegeben: Gruppe mit $x + k\mathbb{Z}$ in $(\mathbb{Z}/k\mathbb{Z})^*$. $x$ sind die Elemente der Restklassengruppe.\\
Gesucht: Ein Wert für $r$, sodass $x^r \modulo k = 1$.\\
\ \\
Beispiel für $(\mathbb{Z} / 15 \mathbb{Z})^*$:\\
$(\mathbb{Z} / 15 \mathbb{Z})^* = \{1 + 15\mathbb{Z}, 2 + 15\mathbb{Z}, 4 + 15\mathbb{Z}, 7 + 15\mathbb{Z}, 8 + 15\mathbb{Z}, 11 + 15\mathbb{Z}, 13 + 15\mathbb{Z}, 14 + 15\mathbb{Z} \}$.\\
\ \\
Ordnungen der Gruppenelemente:\\
$1 + 15\mathbb{Z} = 1$\\
$2 + 15\mathbb{Z} = 4$, da $2^4 = 16 = 1 \modulo 15$\\
$\ldots$

\newpage
\subsection{Weitere Sätze \& Definitionen}
\subsubsection{Der kleinste Satz von Fermat}
\label{fermat}
\begin{center}
Wenn gcd$(a,m) = 1$ ist, dann folgt $a^{\varphi(m)} \equiv 1 \modulo m$.
\end{center}
\textbf{Berechnung der Inverse}:
\begin{center}
$a^{\varphi (m) - 1} + m \mathbb{Z}$ ist die inverse Restklasse von $a + m \mathbb{Z}$.
\end{center}
\textit{Beispiel}:\\
Invertieren Sie 7 in $(\mathbb{Z} / 12 \mathbb{Z})^*$.\\
\ \\
$a = 7, m = 12, $gcd$(7,12) = 1$\\
$\varphi (12) = 4$\\
\ \\
$7^{-1} = 7^{\varphi (12) - 1} = 7^{4-1} = 7^3 = 7^2 \cdot 7 = 49 \cdot 7 = (1 \modulo 12) \cdot 7 = 7 \modulo 12$, d.h. die Inverse von 7 in $(\mathbb{Z} / 12 \mathbb{Z})^*$ ist 7.\\
\ \\
\textbf{Andere Anwendung}:\\
Aufgabe: Berechnen Sie $2^{20} \modulo 7$.\\
$\varphi (7) = 7 - 1 = 6$\\
gcd$(2,7) = 1 \Rightarrow 2^6 = 1 \modulo 7$\\
\ \\
$2^{20} = 2^{18} \cdot 2^2 = (2^6)^3 \cdot 2^2 = (1^3 \cdot 2^2) \modulo 7 = 4 \modulo 7$

\subsubsection{Der chinesische Restsatz}
\label{chinesischer}
Am besten an einer Beispielaufgabe zu erklären.\\
\ \\
Finde eine Zahl $x$, für die gilt:
\[
x \equiv a_1 \modulo m_1, x \equiv a_2 \modulo m_2, \ldots, x \equiv a_n \modulo m_n .
\]
Setze
\[
m = \prod^n_{i=1} m_i, M_i = \frac{m}{m_i}, 1 \leq i \leq n.
\]
Es sei
\[
y_i M_i \equiv 1 \modulo m_i
\]
Dann gilt
\[
x_{all} = \sum^n_{i=1} a_i \cdot M_i \cdot y_i \equiv x \modulo m .
\]

%\newpage
\noindent
\textit{Beispielaufgabe}:\\
\glqq Lösen Sie das simultane Kongruenzsystem mit $\ldots$ \grqq\\
$x \equiv 1 \modulo 7$\\
$x \equiv 3 \modulo 12$\\
$x \equiv 5 \modulo 17$\\
\ \\
$m = 7 \cdot 12 \cdot 17 = 1428$\\
$\Rightarrow$ $M_1 = \frac{1428}{7} = 204, M_2 = \frac{1428}{12} = 119, M_3 = \frac{1428}{17} = 84$\\
\ \\
$i = 1: y_1 \cdot 204 \equiv 1 \modulo 7 \Rightarrow y_1 = 1$\\
$i = 2: y_2 \cdot 119 \equiv 1 \modulo 12 \Rightarrow y_2 = 11$\\
$i = 3: y_3 \cdot 84 \equiv 1 \modulo 17 \Rightarrow y_3 = 16$\\
\ \\
Tabelle aufstellen:\\
\ \\
\begin{tabular}{|c|c|c|c|l|}
\hline
$i$ & $a_i$ & $M_i$ & $y_i$ & Produkt ($a_i*M_i*y_i$)\\
\hline
1 & 1 & 204 & 1  & 204\\
2 & 3 & 119 & 11 & 3927\\
3 & 5 & 84  & 16 & 6720\\
\hline
\hline
\multicolumn{4}{|r|}{Summe der Produkte:} & $x_{all} = 10851 \equiv 855 \modulo 1428$\\
\hline
\end{tabular}\\
\ \\
$\Rightarrow x = 855$

\newpage
\section{Graphentheorie}
\subsection{Begriffe}
Der \textbf{Grad} eines Knotens ist die Anzahl der ein-/ausgehenden Knoten.\\
\ \\
Sind alle besuchten Kanten bei einem Kantenzug unterschiedlich, dann ist ein offener Kantenzug ein \textbf{Weg} und ein geschlossener Kantenzug ein \textbf{Kreis}.

\subsubsection{Eulerscher Weg \& Kreis}
In einem eulerschen Weg / Kreis wird jede \textit{Kante} genau einmal besucht.\\
Ein endlich zusammenhängender Graph enthält genau dann einen \textbf{eulerschen Weg}, wenn er höchstens 2 Knoten mit ungeradem Grad gibt.\\
Ein endlich zusammenhängender Graph enthält genau dann einen \textbf{eulerschen Kreis}, wenn der Grad jedes Knotens gerade ist.\\
\ \\
Ein Graph $G$ heißt eulersch, falls $G$ einen eulerschen Kreis enthält.

\subsubsection{Hamiltonscher Kreis}
In einem hamiltonschen Kreis wird jeder \textit{Knoten} genau einmal besucht.\\
Ein Graph $G$ heißt hamiltonsch, falls $G$ einen hamiltonschen Kreis enthält.\\
\ \\
\textbf{Satz von Dirac}\\
Sei $G$ ein zusammenhängender Graph mit $n \geq 3$ Knoten. Hat jeder Knoten von $G$ einen Grad $\geq \frac{n}{2}$, dann ist $G$ hamiltonsch.\\
\ \\
\textbf{Satz}\\
Sei $G$ ein hamiltonscher Graph mit $n$ Knoten.\\
Entfernt man jetzt $k \leq n$ Knoten aus dem Graphen, so zerfällt dieser in maximal $k$ Zusammenhangskomponenten.

\subsection{Hypercubes \& Gray-Codes}
\begin{figure}[h]
\begin{subfigure}{0.5\linewidth}
\centering
\includegraphics[width=0.8\linewidth]{../Latex/Bilder/cubes_graf_1.png}
\caption{Zwei 1-Cubes}
\end{subfigure}
\begin{subfigure}{0.5\linewidth}
\centering
\includegraphics[width=0.8\linewidth]{../Latex/Bilder/cubes_graf_2.png}
\caption{Zwei 1-Cubes werden verbunden}
\end{subfigure}
\begin{subfigure}{0.5\linewidth}
\centering
\includegraphics[width=0.8\linewidth]{../Latex/Bilder/cubes_graf_3.png}
\caption{Die Beziehungen werden ergänzt}
\end{subfigure}
\begin{subfigure}{0.5\linewidth}
\centering
\includegraphics[width=0.2\linewidth]{../Latex/Bilder/cubes_graf_4.png}
\caption{Der resultierende 2-Cube}
\end{subfigure}
\end{figure}
\noindent
\textbf{Beschreibung des Gray-Codes}:\\
$G_1 = 0,1$
Ein $n+1$-Cube wird nach folgendem Algorithmus erstellt:\\
(Beispiel $G_2 = 00,01,11,10 \Rightarrow G_3$)
\begin{enumerate}
\item Dupliziere die Folge des $n$-Cubes\\
$G_2 = 00,01,11,10$\\
$G_2 = 00,01,11,10$
\item Drehe die 2. Folge um\\
$G_2 = 00,01,11,10$\\
$G_2^u = 10,11,01,00$
\item Erweitere $G_n$ um das Zeichen 0, $G_2^u$ um das Zeichen 1(jeweils am Anfang)\\
$G_2' = 000,001,011,010$\\
$G_2'' = 110,111,101,100$
\item $G_{n+1} = G_n' + G_n''$\\
$G_3 = \underbrace{000,001,011,010}_{G_2'},\underbrace{110,111,101,100}_{G_2''}$
\end{enumerate}

\subsection{Verbände}
\subsubsection{Definition}
Ein \textit{Verband} ist ein Tupel $(S, \leq, \sqcup, \sqcap )$ mit den Eigenschaften
\begin{itemize}
\item $(S, \leq )$ ist eine Ordnung (d.h. $\leq$ ist eine Ordnung auf $S$) und
\item jede zwei Elemente aus $S$ haben eine größte untere Schranke (guS, Infimum) und eine kleinste obere Schranke (koS, Supremum).
\end{itemize}
Ein Verband heißt vollständig, falls jede Teilmenge $T \subseteq S$ eine guS und eine koS besitzt.
\subsubsection{Beispiele}
\textbf{Beispiel 1}:\\
Die beiden folgenden Diagramme stellen keine Verbände dar.\\
Die linke Ordnung hat für jede zwei Elemente eine koS, aber beispielsweise haben $b$ und $c$ keine guS (es gibt keine Element, welches kleiner als $b$ \textbf{und} kleiner als $c$ ist).\\
Die rechte Ordnung hat für jede zwei Elemente eine guS, aber $a$ und $b$ haben keine koS (es gibt kein Element, welches größer als $a$ \textbf{und} größer als $b$ ist).
\centergr{0.6}{ordnung_kein_verband.png}
\textbf{Beispiel 2}:\\
Hasse-Diagramm der Ordnung $(P, \subseteq)$, wobei $P$ die Potenzmenge der Menge $M := \{a,b,c\}$ ist. Wir müssen überprüfen, ob jede zwei Elemente eine guS und eine koS haben:\\
\begin{figure}[h]
\begin{subfigure}{0.5\linewidth}
\tikz{
\node (0) at (2,0) [rectangle] {$\{\} = \bot$};
\node (a) at (0,2) [rectangle] {$\{a\}$};
\node (b) at (2,2) [rectangle] {$\{b\}$};
\node (c) at (4,2) [rectangle] {$\{c\}$};
\node (ab) at (0,4) [rectangle] {$\{a,b\}$};
\node (ac) at (2,4) [rectangle] {$\{a,c\}$};
\node (bc) at (4,4) [rectangle] {$\{b,c\}$};
\node (abc) at (2,6) [rectangle] {$\{a,b,c\} = \top$};

\draw (0)  edge[->] (a)
	  (0)  edge[->] (b)
	  (0)  edge[->] (c)
	  (a)  edge[->] (ab)
	  (a)  edge[->] (ac)
	  (b)  edge[->] (ab)
	  (b)  edge[->] (bc)
	  (c)  edge[->] (ac)
	  (c)  edge[->] (bc)
	  (ab) edge[->] (abc)
	  (ac) edge[->] (abc)
	  (bc) edge[->] (abc);
}
\end{subfigure}
\begin{subfigure}{0.5\linewidth}
\begin{tabular}{cccc}
1. Element & 2. Element & guS & koS\\
\hline
$\{ a \}$ & $T \subseteq M'$ & $\{  \}$ & $T$\\
$\{ a \}$ & $\{ b \}$ & $\{  \}$ & $\{ a,b \}$\\
$\{ a \}$ & $\{ c \}$ & $\{  \}$ & $\{ a,c \}$\\
$\{ a \}$ & $\{ a,b \}$ & $\{ a \}$ & $\{ a,b \}$\\
$\{ a \}$ & $\{ a,c \}$ & $\{ a \}$ & $\{ a,c \}$\\
$\{ a \}$ & $\{ b,c \}$ & $\{  \}$ & $\{ a,b,c \}$\\
$\{ a \}$ & $\{ a,b,c \}$ & $\{ a \}$ & $\{ a,b,c \}$\\
\end{tabular}
\end{subfigure}
\end{figure}

\newpage
\subsection{Funktionen auf Ordnungen}
Seien $(S, \leq_S)$ und $(T, \leq_T)$ Ordnungen. Eine Funktion $\varphi : S \rightarrow T$ heißt
\begin{itemize}
\item \textit{monoton}, falls $s_1 \leq_S s_2 \Rightarrow \varphi (s_1) \leq_T \varphi (s_2)$ für alle $s_1, s_2 \in S$,
\item eine \textit{Einbettung}, falls $s_1 \leq_S s_2 \Leftrightarrow \varphi (s_1) \leq_T \varphi (s_2)$ für alle $s_1, s_2 \in S$,
\item ein \textit{Isomorphismus}, falls $\varphi$ eine surjektive Abbildung ist.
\end{itemize}
Beispiele:\\
\centergr{0.8}{fixpunkte_beispiel.png}
A) $\varphi_1$ ist nicht monoton (da $a \leq b$, aber $\varphi_1 (a) \geq \varphi_1 (b)$).\\
B) $\varphi_2$ ist monoton, aber keine Einbettung (da $\varphi_2 (b) \leq \varphi_2 (c), \: b \not\leq c$).\\
C) $\varphi_3$ ist eine Einbettung, aber kein Isomorphismus (die Elemente $t_1, t_2$ rechts im Bild haben keine Urbilder).\\
D) $\varphi_4$ ist ein Isomorphismus. Die Struktur auf der rechten Seite entspricht der Struktur auf der linken Seite.

\newpage
\subsection{Matrizendarstellung eines Graphen}
In einer Adjazenzmatrix $A$ beschreibt die Reihe den \glqq von\grqq -Knoten, die Spalte den \glqq zu\grqq -Knoten. Der Wert in der ursprünglichen $A^1$-Matrix gibt an, ob es einen Weg gibt ($=1$) oder nicht ($=0$). Wenn der Graph ungerichtet ist, so ist die Adj.matrix symmetrisch.\\
\textbf{Beispiele}:
\centergr{0.4}{adjmatrix_beispiel.png}
\[
A_{\text{unger}} = \begin{bmatrix}
0 & 1 & 1 & 0 & 0\\
1 & 0 & 1 & 1 & 0\\
1 & 1 & 0 & 0 & 1\\
0 & 1 & 0 & 0 & 1\\
0 & 0 & 1 & 1 & 0
\end{bmatrix}
\: , \:
A_{\text{ger}} = \begin{bmatrix}
0 & 0 & 0 & 0 & 0\\
1 & 0 & 0 & 0 & 0\\
1 & 1 & 0 & 0 & 1\\
0 & 1 & 0 & 0 & 0\\
0 & 0 & 0 & 1 & 0
\end{bmatrix}
\]
Wir betrachten als Beispiel die Matrix des ungerichteten Graphen (links) und interpretieren diese.\\
Ist $a_{i,j} = 1$, so gibt es genau einen Kantenzug der Länge 1 zwischen $v_i$ und $v_j$, nämlich $(v_i ; v_j)$. Ist $a_{i,j} = 0$, so gibt es keinen Kantenzug der Länge 1.\\
Um die gesuchte Länge zu ändern, kann man die Matrix mit sich selber multiplizieren:
\[ B^{(2)} := A^2 = \begin{bmatrix}
0 & 1 & 1 & 0 & 0\\
1 & 0 & 1 & 1 & 0\\
1 & 1 & 0 & 0 & 1\\
0 & 1 & 0 & 0 & 1\\
0 & 0 & 1 & 1 & 0
\end{bmatrix}
\begin{bmatrix}
0 & 1 & 1 & 0 & 0\\
1 & 0 & 1 & 1 & 0\\
1 & 1 & 0 & 0 & 1\\
0 & 1 & 0 & 0 & 1\\
0 & 0 & 1 & 1 & 0
\end{bmatrix}
=\begin{bmatrix}
2 & 1 & 1 & 1 & 1\\
1 & 3 & 1 & 0 & 2\\
1 & 1 & 3 & 2 & 0\\
1 & 0 & 2 & 2 & 0\\
1 & 2 & 0 & 0 & 2
\end{bmatrix}
\]
Interpretation:\\
$b_{1,1} = 2$: Es gibt genau 2 Kantenzüge der Länge 2 von $v_1$ nach $v_1$: $(v_1, v_2, v_1)$ und $(v_1, v_3, v_1)$.\\
$b_{1,2} = 1$: Es gibt genau 1 Kantenzug der Länge 2 von $v_1$ nach $v_2$: $(v_1, v_3, v_2)$.\\
$b_{2,2} = 3$: Es gibt genau 3 Kantenzüge der Länge 2 von $v_2$ nach $v_2$: $(v_2, v_1, v_2)$,$(v_2,v_3,v_2)$ und $(v_2, v_4, v_2)$.\\
$b_{2,4} = 0$: Es gibt keinen Kantenzug der Länge 2 von $v_2$ nach $v_4$.\\
\ \\
\textbf{Satz (Koeffizienten in einer Potenz einer Adjazenzmatrix)}:\\
Seien $A$ die Adjazentmatrix eines endlichen Graphen $G$ und $B^{(m)} := A^m$. Der Eintrag $b^{(m)}_{i,j}$ der Matrix $B^{(m)}$ gibt die Anzahl der Kantenzüge der Länge $m$ von $v_i$ nach $v_j$ an.\\
\ \\
\textbf{Korollar (Summe der Anzahl von Kantenzügen verschiedener Längen)}:\\
Es sei $G = (V,E)$ ein Graph mit Adjazenzmatrix $A$. Dann gibt der Eintrag $s^m_{i,j}$ der Matrix
\[
S^{(m)} = \sum^m_{k=1} A^k
\]
die Anzahl der Kantenzügen der Länge $\leq m$ von $v_i$ nach $v_j$ an.

\newpage
\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 0$\\
$0 \circ 1 = 0$\\
$1 \circ 0 = 0$\\
$1 \circ 1 = 0$\\
\ \\
Halbgruppe? \cmark \\
Monoid? \xmark \\
Gruppe? \xmark \\
\end{minipage}
\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 0$\\
$0 \circ 1 = 0$\\
$1 \circ 0 = 0$\\
$1 \circ 1 = 1$\\
\ \\
Halbgruppe? \cmark \\
Monoid? \cmark $e = 1$ \\
Gruppe? \xmark \\
\end{minipage}
\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 0$\\
$0 \circ 1 = 0$\\
$1 \circ 0 = 1$\\
$1 \circ 1 = 0$\\
\ \\
Halbgruppe? \xmark \\
Monoid? \xmark \\
Gruppe? \xmark \\
\end{minipage}
\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 0$\\
$0 \circ 1 = 0$\\
$1 \circ 0 = 1$\\
$1 \circ 1 = 1$\\
\ \\
Halbgruppe? \cmark \\
Monoid? \xmark \\
Gruppe? \xmark \\
\end{minipage}
\hrule
\ \\

\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 0$\\
$0 \circ 1 = 1$\\
$1 \circ 0 = 0$\\
$1 \circ 1 = 0$\\
\ \\
Halbgruppe? \xmark \\
Monoid? \xmark \\
Gruppe? \xmark \\
\end{minipage}
\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 0$\\
$0 \circ 1 = 1$\\
$1 \circ 0 = 0$\\
$1 \circ 1 = 1$\\
\ \\
Halbgruppe? \cmark \\
Monoid? \xmark \\
Gruppe? \xmark \\
\end{minipage}
\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 0$\\
$0 \circ 1 = 1$\\
$1 \circ 0 = 1$\\
$1 \circ 1 = 0$\\
\ \\
Halbgruppe? \cmark \\
Monoid? \cmark $e = 0$ \\
Gruppe? \cmark $1^{-1} = 1$ \\
\end{minipage}
\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 0$\\
$0 \circ 1 = 1$\\
$1 \circ 0 = 1$\\
$1 \circ 1 = 1$\\
\ \\
Halbgruppe? \cmark \\
Monoid? \cmark $e = 0$ \\
Gruppe? \xmark \\
\end{minipage}
\hrule
\ \\

\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 1$\\
$0 \circ 1 = 0$\\
$1 \circ 0 = 0$\\
$1 \circ 1 = 0$\\
\ \\
Halbgruppe? \xmark \\
Monoid? \xmark \\
Gruppe? \xmark \\
\end{minipage}
\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 1$\\
$0 \circ 1 = 0$\\
$1 \circ 0 = 0$\\
$1 \circ 1 = 1$\\
\ \\
Halbgruppe? \cmark \\
Monoid? \cmark $e = 1$ \\
Gruppe? \cmark $0^{-1} = 0$ \\
\end{minipage}
\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 1$\\
$0 \circ 1 = 0$\\
$1 \circ 0 = 1$\\
$1 \circ 1 = 0$\\
\ \\
Halbgruppe? \xmark \\
Monoid? \xmark \\
Gruppe? \xmark \\
\end{minipage}
\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 1$\\
$0 \circ 1 = 0$\\
$1 \circ 0 = 1$\\
$1 \circ 1 = 1$\\
\ \\
Halbgruppe? \xmark \\
Monoid? \xmark \\
Gruppe? \xmark \\
\end{minipage}
\hrule
\ \\

\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 1$\\
$0 \circ 1 = 1$\\
$1 \circ 0 = 0$\\
$1 \circ 1 = 0$\\
\ \\
Halbgruppe? \xmark \\
Monoid? \xmark \\
Gruppe? \xmark \\
\end{minipage}
\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 1$\\
$0 \circ 1 = 1$\\
$1 \circ 0 = 0$\\
$1 \circ 1 = 1$\\
\ \\
Halbgruppe? \xmark \\
Monoid? \xmark \\
Gruppe? \xmark \\
\end{minipage}
\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 1$\\
$0 \circ 1 = 1$\\
$1 \circ 0 = 1$\\
$1 \circ 1 = 0$\\
\ \\
Halbgruppe? \xmark \\
Monoid? \xmark \\
Gruppe? \xmark \\
\end{minipage}
\begin{minipage}[m]{0.23\linewidth}
$0 \circ 0 = 1$\\
$0 \circ 1 = 1$\\
$1 \circ 0 = 1$\\
$1 \circ 1 = 1$\\
\ \\
Halbgruppe? \cmark \\
Monoid? \xmark \\
Gruppe? \xmark \\
\end{minipage}




\end{document}