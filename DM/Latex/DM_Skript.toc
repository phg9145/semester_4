\babel@toc {german}{}
\contentsline {section}{\numberline {1}Grundlagen}{3}%
\contentsline {subsection}{\numberline {1.1}Ganze Zahlen}{3}%
\contentsline {subsubsection}{\numberline {1.1.1}Schreibweisen und Definitionen}{3}%
\contentsline {subsubsection}{\numberline {1.1.2}Vollständige Induktion}{3}%
\contentsline {subsubsection}{\numberline {1.1.3}Teilbarkeit}{4}%
\contentsline {subsubsection}{\numberline {1.1.4}Darstellung ganzer Zahlen}{5}%
\contentsline {subsubsection}{\numberline {1.1.5}Größter gemeinsamer Teiler}{5}%
\contentsline {subsubsection}{\numberline {1.1.6}Zerlegung in Primzahlen}{6}%
\contentsline {subsection}{\numberline {1.2}Wahrscheinlichkeit}{7}%
\contentsline {subsubsection}{\numberline {1.2.1}Grundbegriffe}{7}%
\contentsline {subsubsection}{\numberline {1.2.2}Bedingte Wahrscheinlichkeit}{8}%
\contentsline {subsection}{\numberline {1.3}Zufallsvariablen}{9}%
\contentsline {subsubsection}{\numberline {1.3.1}Geburtstagsparadox}{9}%
\contentsline {subsection}{\numberline {1.4}Berechnungsprobleme}{10}%
\contentsline {subsection}{\numberline {1.5}Algorithmen für ganze Zahlen}{11}%
\contentsline {subsubsection}{\numberline {1.5.1}Addition, Multiplikation und Division mit Rest}{11}%
\contentsline {subsubsection}{\numberline {1.5.2}Euklidischer Algorithmus}{11}%
\contentsline {section}{\numberline {2}Gruppen, Verbände und Ordnungen}{12}%
\contentsline {subsection}{\numberline {2.1}Kongruenzen}{12}%
\contentsline {subsection}{\numberline {2.2}Halbgruppen}{14}%
\contentsline {subsection}{\numberline {2.3}Gruppen}{15}%
\contentsline {subsection}{\numberline {2.4}Restklassenringe}{16}%
\contentsline {subsection}{\numberline {2.5}Körper}{17}%
\contentsline {subsection}{\numberline {2.6}Division im Restklassenring}{17}%
\contentsline {subsection}{\numberline {2.7}Ordnung von Gruppen (Prime Restklassengruppen)}{17}%
\contentsline {subsection}{\numberline {2.8}Ordnung von Gruppenelementen}{18}%
\contentsline {subsection}{\numberline {2.9}Untergruppen}{19}%
\contentsline {subsection}{\numberline {2.10}Der kleine Satz von Fermat}{19}%
\contentsline {subsection}{\numberline {2.11}Der Chinesische Restsatz}{20}%
\contentsline {section}{\numberline {3}Graphentheorie}{21}%
\contentsline {subsection}{\numberline {3.1}Grundlagen}{21}%
\contentsline {subsection}{\numberline {3.2}Eulersche Kreise}{23}%
\contentsline {subsection}{\numberline {3.3}Hamiltonsche Kreise}{25}%
\contentsline {subsection}{\numberline {3.4}Hypercubes}{26}%
\contentsline {subsection}{\numberline {3.5}Gerichtete Graphen}{27}%
\contentsline {subsubsection}{\numberline {3.5.1}Ordnungen}{27}%
\contentsline {subsubsection}{\numberline {3.5.2}Wohlordnung}{28}%
\contentsline {subsubsection}{\numberline {3.5.3}Fundierte Menge und Induktion}{28}%
\contentsline {subsection}{\numberline {3.6}Verbände}{30}%
\contentsline {subsection}{\numberline {3.7}Funktionen auf Ordnungen und Fixpunkte}{32}%
\contentsline {subsection}{\numberline {3.8}Wälder und Bäume}{33}%
\contentsline {subsection}{\numberline {3.9}Darstellung von Graphen in Computern}{33}%
\contentsline {subsubsection}{\numberline {3.9.1}Matrizendarstellung eines Graphen}{33}%
\contentsline {subsubsection}{\numberline {3.9.2}Adjazenzlisten}{34}%
