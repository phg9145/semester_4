\babel@toc {german}{}
\contentsline {section}{\numberline {1}Grundlagen}{4}%
\contentsline {subsection}{\numberline {1.1}Wahrscheinlichkeit}{4}%
\contentsline {subsubsection}{\numberline {1.1.1}Bedingte Wahrscheinlichkeit}{4}%
\contentsline {subsubsection}{\numberline {1.1.2}Satz von Bayes}{4}%
\contentsline {subsubsection}{\numberline {1.1.3}Zufallsvariable \& Erwartungswert}{4}%
\contentsline {subsection}{\numberline {1.2}Erweiterter euklidischer Algorithmus}{4}%
\contentsline {section}{\numberline {2}Ordnungen, Gruppen, Verbände}{5}%
\contentsline {subsection}{\numberline {2.1}Ordnungen}{5}%
\contentsline {subsection}{\numberline {2.2}Halbgruppe, Monoid, Gruppe}{6}%
\contentsline {subsubsection}{\numberline {2.2.1}Halbgruppe}{6}%
\contentsline {subsubsection}{\numberline {2.2.2}Monoid}{6}%
\contentsline {subsubsection}{\numberline {2.2.3}Gruppe}{6}%
\contentsline {subsection}{\numberline {2.3}Gruppen}{6}%
\contentsline {subsubsection}{\numberline {2.3.1}Ordnung von primen Restklassen}{6}%
\contentsline {subsubsection}{\numberline {2.3.2}Ordnung von Gruppenelementen}{7}%
\contentsline {subsection}{\numberline {2.4}Weitere Sätze \& Definitionen}{7}%
\contentsline {subsubsection}{\numberline {2.4.1}Der kleinste Satz von Fermat}{7}%
\contentsline {subsubsection}{\numberline {2.4.2}Der chinesische Restsatz}{7}%
\contentsline {section}{\numberline {3}Graphentheorie}{8}%
\contentsline {subsection}{\numberline {3.1}Begriffe}{8}%
\contentsline {subsubsection}{\numberline {3.1.1}Eulerscher Weg \& Kreis}{8}%
\contentsline {subsubsection}{\numberline {3.1.2}Hamiltonscher Kreis}{8}%
\contentsline {subsection}{\numberline {3.2}Hypercubes \& Gray-Codes}{9}%
\contentsline {subsection}{\numberline {3.3}Verbände}{9}%
\contentsline {subsubsection}{\numberline {3.3.1}Definition}{9}%
\contentsline {subsubsection}{\numberline {3.3.2}Beispiele}{9}%
\contentsline {subsection}{\numberline {3.4}Funktionen auf Ordnungen}{10}%
\contentsline {subsection}{\numberline {3.5}Matrizendarstellung eines Graphen}{11}%
\contentsline {section}{\numberline {4}Beispielaufgaben}{12}%
\contentsline {subsection}{\numberline {4.1}Grundlagen}{12}%
\contentsline {subsubsection}{\numberline {4.1.1}Euklid 1}{12}%
\contentsline {subsubsection}{\numberline {4.1.2}Euklid 2}{12}%
\contentsline {subsubsection}{\numberline {4.1.3}Euklid 3}{12}%
\contentsline {subsection}{\numberline {4.2}Ordnungen, Gruppen, Verbände}{12}%
\contentsline {subsubsection}{\numberline {4.2.1}Halbgruppe, Monoid, Gruppe}{12}%
\contentsline {subsubsection}{\numberline {4.2.2}Gruppen 1}{12}%
\contentsline {subsubsection}{\numberline {4.2.3}Gruppen 2}{12}%
\contentsline {subsubsection}{\numberline {4.2.4}Chinesischer Restsatz 1}{12}%
\contentsline {subsection}{\numberline {4.3}Graphentheorie}{13}%
\contentsline {subsubsection}{\numberline {4.3.1}Eulerscher \& hamiltonscher Weg/Kreis}{13}%
\contentsline {subsubsection}{\numberline {4.3.2}Hasse-Diagramm \& Verband}{13}%
\contentsline {subsubsection}{\numberline {4.3.3}Adjazenzmatrizen}{13}%
\contentsline {section}{\numberline {5}Beispielaufgaben Lösungen}{14}%
\contentsline {subsection}{\numberline {5.1}Grundlagen}{14}%
\contentsline {subsubsection}{\numberline {5.1.1}Euklid 1}{14}%
\contentsline {subsubsection}{\numberline {5.1.2}Euklid 2}{14}%
\contentsline {subsubsection}{\numberline {5.1.3}Euklid 3}{14}%
\contentsline {subsection}{\numberline {5.2}Ordnungen, Gruppen, Verbände}{15}%
\contentsline {subsubsection}{\numberline {5.2.1}Halbgruppe, Monoid, Gruppe}{15}%
\contentsline {subsubsection}{\numberline {5.2.2}Gruppen 1}{15}%
\contentsline {subsubsection}{\numberline {5.2.3}Gruppen 2}{15}%
\contentsline {subsubsection}{\numberline {5.2.4}Chinesischer Restsatz}{16}%
\contentsline {subsection}{\numberline {5.3}Graphentheorie}{17}%
\contentsline {subsubsection}{\numberline {5.3.1}Eulerscher \& hamiltonscher Weg/Kreis}{17}%
\contentsline {subsubsection}{\numberline {5.3.2}Hasse-Diagramm \& Verband}{17}%
\contentsline {subsubsection}{\numberline {5.3.3}Adjazenzmatrizen}{18}%
