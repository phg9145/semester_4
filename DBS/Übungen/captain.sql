-- Ermitteln Sie die Nummer und den Namen jedes Wettkampfspielers,
-- sowie die Nummer und den Namen des Kapitäns jeder Mannschaft,
-- für die dieser Spieler jemals gekämpft hat.
-- Das Ergebnis darf keine Wettkampfspieler enthalten,
-- die Kapitäne einer Mannschaft sind.

SELECT DISTINCT p.playerno, p.lastname, m.teamno, cpt.playerno AS cpt_pno, cpt.lastname AS cpt_lastname
FROM ((matches AS m JOIN players AS p ON (m.playerno = p.playerno))
      JOIN teams AS t ON (m.teamno = t.teamno))
      JOIN players AS cpt ON (t.playerno = cpt.playerno)

WHERE m.playerno != t.playerno

ORDER BY playerno