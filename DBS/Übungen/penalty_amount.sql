-- Geben Sie die Spielernummer für die Spieler aus,
-- die die gleiche Anzahl an Strafen haben wie Spieler 6

SELECT playerno, COUNT(*)
FROM penalties
WHERE playerno <> 6
GROUP BY playerno
HAVING COUNT(*) = (SELECT COUNT(*) -- COUNT(*) --> zählt Einträge
		   FROM penalties
		   WHERE playerno = 6
		   GROUP BY playerno)