-- Finden Sie die Spieler-Nummern und Namen aller Spieler,
-- die in der gleichen Stadt leben wie Spieler Nr. 27

SELECT playerno, lastname, initials
FROM players NATURAL JOIN (SELECT town FROM players WHERE playerno = 27) AS p2
WHERE playerno <> 27
;

SELECT p1.playerno, p1.lastname, p1.initials
FROM players AS p1
     JOIN players AS p2 USING (town)
WHERE p2.playerno   = 27
AND   p1.playerno != 27
;