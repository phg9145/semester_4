-- For each player, get the player number, the name,
-- and the difference between the year in which he or she
-- joined the club and the average year in which players
-- who live in the same town joined the club.

-- joined = eintrittsjahr des Spielers
-- average = AVG in same town
-- difference = (joined - average)

SELECT p.playerno, p.lastname,
       (p.joined - (SELECT AVG(joined)
		    FROM players
		    WHERE town = p.town)) AS difference
FROM players AS p
ORDER BY difference;

-- oder:
SELECT p.playerno, p.lastname,
       (p.joined - average) AS difference
FROM players AS p, (SELECT town, AVG(joined) AS average
		    FROM players
		    GROUP BY town) AS towns
WHERE p.town = towns.town
ORDER BY difference