-- Für jede Strafe nehmen Sie den Wert plus die Summe aller Strafen
-- mit einer kleineren Strafennummer (kumulativer Wert)

SELECT p1.paymentno, p1.amount, SUM(p2.amount) AS summe
FROM penalties AS p1, penalties AS p2
WHERE p2.paymentno <= p1.paymentno
GROUP BY p1.paymentno
ORDER BY p1.paymentno;

-- oder:
SELECT p1.paymentno, p1.amount,
(SELECT SUM(amount) FROM penalties WHERE paymentno <= p1.paymentno)
 AS summe
FROM penalties AS p1
ORDER BY p1.paymentno;