-- Für jeden Spieler, geboren nach Juli 1920,
-- geben Sie die SPielernummer, den Namen und seine Strafen aus

-- Impliziter Join:
SELECT players.playerno, lastname, amount
FROM players, penalties

-- Wichtig: WHERE x.a = y.a und AND
WHERE players.playerno = penalties.playerno
AND birthdate > '1920-06-30'

ORDER BY players.playerno
;

-- Expliziter Join:
SELECT players.playerno, lastname, amount
;
FROM players INNER JOIN penalties
	ON (players.playerno = penalties.playerno)
WHERE birthdate > '1920-06-30'
ORDER BY players.playerno
;