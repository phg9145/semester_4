-- Mit einer Select-Anweisung ale Spieler ausgeben,
-- die älter sind als Parmenter, R.

SELECT playerno, lastname, birthdate
FROM players
WHERE birthdate < (SELECT birthdate
		   FROM players
		   WHERE lastname = 'Parmenter' AND initials = 'R')
;

SELECT A.playerno, A.lastname, A.birthdate
FROM players AS A, players AS B
WHERE B.lastname = 'parmenter'
AND B.initials = 'R'
AND A.birthdate < B.birthdate
ORDER BY B.birthdate