-- Geben Sie die Spielernummer und den Gesamtbetrag seiner Strafen aus,
-- für den Spieler, der die höchste Gesamtstrafe erhalten hat.

SELECT playerno, SUM(amount) AS gesamt
FROM penalties
GROUP BY playerno
HAVING gesamt >= MAX(gesamt)
;

SELECT playerno, SUM(amount) AS gesamt
FROM penalties
GROUP BY playerno
HAVING gesamt >= ALL(SELECT SUM(amount)
		     FROM penalties
		     GROUP BY playerno)
;

SELECT playerno, SUM(amount) AS gesamt
FROM penalties
GROUP BY playerno
-- ORDER BY 2 DESC
-- ORDER BY gesamt DESC
ORDER BY SUM(amount) DESC
LIMIT 1
;