-- Erstelle eine gespeicherte Prozedur zur Abfrage der Adresse eines Spielers

DELIMITER $$

CREATE PROCEDURE give_address
   (IN p_playerno SMALLINT,
	OUT p_street VARCHAR(30),
	OUT p_houseno VARCHAR(4),
	OUT p_town VARCHAR(30),
	OUT p_postcode VARCHAR(6))

BEGIN

	SELECT town, street, houseno, postcode
	INTO p_town, p_street, p_houseno, p_postcode
	FROM players
	WHERE playerno = p_playerno;

END $$

DELIMITER;