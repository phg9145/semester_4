DELIMITER $$
CREATE PROCEDURE DIFFERENCE
   (IN P1 INTEGER,
    IN P2 INTEGER,
    OUT P3 VARCHAR(20))

BEGIN
    
    IF P1 > P2 THEN
	SET P3 = "P1 GRÖSSER P2";
    ELSEIF P1 = P2 THEN
	SET P3 = "P1 GLEICH P2";
    ELSE
	SET P3 = "P1 KLEINER P2";
    END IF;

END
$$