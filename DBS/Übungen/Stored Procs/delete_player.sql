-- Erstelle eine gespeicherte Prozedur,
-- die einen Spieler nach folgenden Regeln löscht:
-- Der Spieler hat keine Strafen bezahlt und ist nicht Kapitän eines Teams

DELIMITER $$

CREATE PROCEDURE delete_player
	(IN p_playerno INTEGER)
	
	-- raus_hier: Loop
	raus_hier:BEGIN
	
		DECLARE number_of_penalties INTEGER;
		DECLARE number_of_teams INTEGER;
	
		-- Hat der Spieler Strafen bezahlt?
		SELECT COUNT(*)
		INTO   number_of_penalties
		FROM   players
		WHERE  playerno = p_playerno;
		
		-- Ist der Spieler Kapitän eines Teams?
		SELECT COUNT(*)
		INTO   number_of_teams
		FROM   teams
		WHERE  playerno = p_playerno;
		
		-- IF number_of_penalties > 0 THEN
		--	LEAVE raus_hier;
		-- END IF;		
		-- Hier hat der Spieler keine Strafen mehr
		
		-- IF number_of_teams > 0 THEN
		--	LEAVE raus_hier;
		-- END IF;
		-- Hier ist der Spieler kein Kapitän mehr
		
		IF number_of_penalties = 0 AND number_of_teams = 0 THEN
			CALL DELETE_MATCHES (p_playerno);
			DELETE FROM committee_members WHERE playerno = p_playerno;
			DELETE FROM players 		  WHERE playerno = p_playerno;
		END IF;

END $$

DELIMITER;