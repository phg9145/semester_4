-- Welche Tabelle hat mehr Zeilen, PLAYERS oder PENALTIES?

DELIMITER $$

CREATE PROCEDURE LARGEST
    (OUT T CHAR(10))

BEGIN
    DECLARE count_players INTEGER DEFAULT (SELECT COUNT(*) FROM players);
    DECLARE count_penalties INTEGER DEFAULT (SELECT COUNT(*) FROM penalties);

    IF count_players > count_penalties THEN
	SET T = 'PLAYERS';
    ELSEIF count_players = count_penalties THEN
	SET T = 'GLEICH';
    ELSE
	SET T = 'PENALTIES';
   END IF;
   
END
$$

DELIMITER ;