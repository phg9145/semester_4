-- T1 = '1991-01-12'
-- T2 = '1999-07-09'
-- Different, Abstand: DATEDIFF(T1;T2) -> Sekunden, Millisekunden, Jahren
-- Angaben in Jahren, Monaten, Tagen

DELIMITER $$

CREATE PROCEDURE AGE(
	IN  start_date  DATE,
	IN  end_date    DATE,
	OUT years		INTEGER,
	OUT months		INTEGER,
	OUT days		INTEGER)

BEGIN
	DECLARE next_date, previous_date DATE;
	
	SET years = 0;
	SET previous_date = start_date;
	SET next_date = start_date + INTERVAL 1 YEAR;
	
	WHILE next_date <= end_date DO
		SET years = years + 1;
		SET previous_date = next_date;
		SET next_date = next_date + INTERVAL 1 YEAR;
	END WHILE;
	
	SET months = 0;
	SET next_date = previous_date + INTERVAL 1 MONTH;
	WHILE next_date <= end_date DO
		SET months = months + 1;
		SET previous_date = next_date;
		SET next_date = next_date + INTERVAL 1 MONTH;
	END WHILE;
	
	SET days = 0;
	SET next_date = previous_date + INTERVAL 1 DAY;
	WHILE next_date <= end_date DO
		SET days = days + 1;
		SET previous_date = next_date;
		SET next_date = next_date + INTERVAL 1 DAY;
	END WHILE;



END
$$
DELIMITER ;