DELIMITER $$

DROP PROCEDURE IF EXISTS total_number_of_parents$$

CREATE PROCEDURE total_number_of_parents
	(IN p_playerno INTEGER,
	 INOUT NUMBER INTEGER)

BEGIN
	DECLARE v_father, v_mother INTEGER;
	
	IF NUMBER IS NULL THEN
		SET NUMBER = 0;
	END IF;
	
	SET v_father = (SELECT father_playerno
					FROM players_with_parents
					WHERE playerno = p_playerno);

	SET v_mother = (SELECT mother_playerno
					FROM players_with_parents
					WHERE playerno = p_playerno);

	IF v_father IS NOT NULL THEN
		CALL total_number_of_parents(v_father, NUMBER);
		SET NUMBER = NUMBER + 1;
	END IF;
	
	IF v_mother IS NOT NULL THEN
		CALL total_number_of_parents(v_mother, NUMBER);
		SET NUMBER = NUMBER + 1;
	END IF;







END
$$

DELIMITER ;