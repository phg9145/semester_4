-- Erstelle eine gespeicherte Prozedur zur Abfrage der Adresse eines Spielers

DELIMITER $$

CREATE PROCEDURE give_address_2
    (IN p_playerno SMALLINT)

BEGIN

	SELECT town, street, houseno, postcode
	FROM players
	WHERE playerno = p_playerno;

END $$

DELIMITER;