-- Welche Tabelle hat mehr Zeilen, PLAYERS oder PENALTIES?

DELIMITER $$

CREATE PROCEDURE WAIT
    (IN wait_seconds INTEGER,
     OUT ist_schaltjahr BOOLEAN)

BEGIN
    -- Systemzeit + wait_seconds
    DECLARE end_time DATETIME DEFAULT NOW() + INTERVAL wait_seconds SECOND;
    
    DECLARE end_of_february DATETIME DEFAULT '2020-03-01 00:00:00' - INTERVAL 10 MINUTE;
    IF DAY(end_of_february) = 29 THEN
	SET ist_schaltjahr = 1;
    ELSE
	SET ist_schaltjahr = 0;
    END IF;
    
    wait_loop:LOOP
	IF NOW() > end_time THEN
	    LEAVE wait_loop;
	END IF;
   END LOOP wait_loop;
   
END
$$

DELIMITER ;