-- Erzeugen Sie eine Liste mit den Nummern der Spieler,
-- die Vorstandsmitglieder waren + eine Liste aller Spielernummern,
-- die min. zwei Strafen bezahlt haben

SELECT playerno
FROM committee_members

UNION

SELECT playerno
FROM penalties
GROUP BY playerno
HAVING COUNT(*) >= 2

ORDER BY playerno