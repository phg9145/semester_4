-- Für jeden Spieler, der eine Strafe bezahlt hat und Kapitän ist,
-- geben Sie die Spielernummer, den Namen, die Anzahl der Strafen
-- und die Anzahl der Teams, in denen er Kapitän ist, aus

-- PLAYERNO NAME NUMBER_OF_PENALTIES NUMBER_OF_TEAMS
-- -------- ---- ------------------- ---------------
-- 6 Parmenter 1 1
-- 27 Collins 2 1


SELECT players.playerno, lastname, number_of_penalties, number_of_teams
FROM players,
	(SELECT playerno, COUNT(*) AS number_of_penalties
	 FROM penalties
	 GROUP BY playerno) AS number_pen,
	(SELECT playerno, COUNT(*) AS number_of_teams
	 FROM teams
	 GROUP BY playerno) AS number_t
WHERE players.playerno = number_pen.playerno
AND   players.playerno = number_t.playerno

-- Wenn mit Views:
-- FROM players,
--      number_pen,
--      number_t