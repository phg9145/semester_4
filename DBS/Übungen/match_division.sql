-- Suchen Sie für jedes Match die Matchnummer, die Spielernummer,
-- den Nachnamen und die Abteilung des Teams. Benutzen Sie ein explizites JOIN

SELECT m.matchno, p.playerno, p.lastname, t.division

FROM
matches AS m
-- JOIN players AS p ON (m.playerno = p.playerno)
JOIN players AS p USING (playerno)
-- JOIN teams AS t ON (m.teamno = t.teamno)
JOIN teams AS t USING (teamno)
     
ORDER BY matchno