\babel@toc {german}{}
\contentsline {section}{\numberline {1}Grundlagen Datenbanktechnik}{3}%
\contentsline {subsection}{\numberline {1.1}Grundbegriffe der Datenbanktechnik}{3}%
\contentsline {subsection}{\numberline {1.2}Phasen des Datenbankentwurfs}{5}%
\contentsline {subsection}{\numberline {1.3}Einfache Datenbankumgebung}{6}%
\contentsline {subsection}{\numberline {1.4}Komponenten eines Datenbanksystems}{6}%
\contentsline {subsection}{\numberline {1.5}Typische Komponenten eines DBMS}{7}%
\contentsline {subsection}{\numberline {1.6}Typische grundlegende Systemarchitektur DBS-basierter Anwendungssysteme}{8}%
\contentsline {subsection}{\numberline {1.7}Kriterien für eine DBS-Auswahl}{8}%
\contentsline {subsection}{\numberline {1.8}Forderungen an ein DBS}{9}%
\contentsline {subsection}{\numberline {1.9}Inhaltliche Anforderungen an ein Datenbanksystem}{10}%
\contentsline {subsection}{\numberline {1.10}Datenunabhängigkeit}{11}%
\contentsline {subsection}{\numberline {1.11}Schlüssel in Datenbanken}{12}%
\contentsline {subsection}{\numberline {1.12}Integrität einer Datenbank}{13}%
\contentsline {subsubsection}{\numberline {1.12.1}Allgemeine Begriffsbestimmung}{13}%
\contentsline {subsubsection}{\numberline {1.12.2}Physische und semantische Integrität}{14}%
\contentsline {subsubsection}{\numberline {1.12.3}Werteabhängige und operationale (dynamische) Integrität}{14}%
\contentsline {subsubsection}{\numberline {1.12.4}Strukturelle Integrität}{14}%
\contentsline {subsubsection}{\numberline {1.12.5}Semantische Integrität relationaler Datenbanken}{15}%
