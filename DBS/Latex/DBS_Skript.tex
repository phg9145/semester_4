\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[german]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{xcolor}
\usepackage{float}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{listings}
\usepackage{fancyhdr}
\usepackage[a4paper,left=3cm,right=3cm,top=3cm,bottom=3cm,bindingoffset=5mm]{geometry}

\newcommand{\definition}[1]{Definition \textbf{#1}:}
\newcommand{\cmark}{\ding{51}}
\newcommand{\xmark}{\ding{55}}

\newcommand{\datum}[1]{
\begin{flushright}
\date{#1}
\end{flushright}
}
\newcommand{\changefont}{\fontsize{9}{11}\selectfont}

\fancyhf{}
\fancyfoot[L]{\changefont \slshape \leftmark}
\fancyhead[R]{\changefont \slshape \rightmark}
\fancyfoot[R]{Letzte Änderung: \today}
\fancyfoot[C]{\thepage}
\pagestyle{fancy}

\title{Datenbanksysteme}
\author{Philipp von Perponcher}
\date{21.03.2020}

\begin{document}

\maketitle
\newpage

\tableofcontents

\newpage
\datum{21.03.2020}
\section{Grundlagen Datenbanktechnik}
\subsection{Grundbegriffe der Datenbanktechnik}
\begin{itemize}
\item \textbf{Datenmodell}\\
Legt die Konstrukte fest, mittels derer die in einem Modell enthaltenen Daten und ihrer Zusammenhänge beschrieben werden müssen. Es bildet damit die \glqq Sprache\grqq zur Modellierung.
\item \textbf{Diskursbereich}\\
Ein Diskursbereich ist ein Ausschnitt aus der realen Welt. Der Diskursbereich kann auch fiktive Daten enthalten
\item \textbf{Information}\\
Kenntnis über Sachverhalte und Vorgänge. Informationen werden durch Syntax, Semantik und Pragmatik beschrieben
\item \textbf{Konzeptioneller Entwurf / Modell}\\
\textit{Abstrakte} und \textit{vollständige} Beschreibung des betrachteten Diskursbereichs
\item \textbf{Logischer Entwurf / Modell}\\
\textit{Verarbeitungsorientierte} Darstellung der im konzeptionellen Entwurf enthaltenen Daten und ihre Beziehungen (z.B. Tabellen)
\item \textbf{Physischer Entwurf}\\
Entwurf der Datenorganisation zur physischen Speicherung der Daten mit Festlegung der in dem DBMS zur Verfügung stehenden Parameter
\item \textbf{Schema}\\
\textit{Formale} Beschreibung des logischen und physischen Entwurfs für ein bestimmtes DBMS. Es wird mit Hilfe der konkreten Sprache des DBMS ausgedrückt (z.B. SQL)
\item \textbf{Datenbank Management System (DBMS)}\\
Ein System zum Aufbau, zur Kontrolle und zur Manipulation von Datenbanken
\item \textbf{Daten}\\
Einformationen, die in einer DV-Anlage maschinell verarbeitet werden können
\item \textbf{Datenbank und Datenbankmanagementsystem}\\
\includegraphics[width=0.4\linewidth]{Bilder/db_dbms.png}
\item \textbf{Datenbank/Datenbasis}\\
Menge zusammengehöriger Daten, die als Ganzes verwaltet werden
\item \textbf{Dateneinheit, Datum}\\
Kleinste isolierbare Einheit einer Menge von Daten, wird in Datenbanken i.d.R. Datenfeld oder Datenelement genannt
\item \textbf{DB-Managementsystem, DB-Verwaltungssystem}\\
System zum Aufbau, zur Manipulation und zur Kontrolle von Datenbanken
\item \textbf{Datenbankkommunikationsschnittstelle}\\
Schnittstelle zur Kommunikation der Datenbank mit dem Anwenderprogramm, wird häufig dem Datenbankverwaltungssystem zugeordnet
\item \textbf{Datenbanksystem (DBS)}\\
Zusammenfassung der Begriffe Datenbank und Datenbankverwaltungssystem zzgl. einer Datenkommunikationsschnittstelle
\item \textbf{Datenunabhängigkeit}\\
Invarianz eines Anwendungssystems bei Änderung der DB-Struktur oder anderer Anwendungssysteme und Invarianz des Schemas gegenüber Hardware- und internen Datenstrukturänderungen
\item \textbf{Externe Ebene}\\
Individuelle Sichten der Benutzer und Benutzergruppen
\item \textbf{Konzeptionelle Ebene}\\
Logische Gesamtsicht aller Daten und Beziehungen
\item \textbf{Interne Ebene}\\
Physische Sicht der Datenorganisation: Darstellung der Daten und Beziehungen durch Speicherstrukturen
\item \textbf{Redundanz}\\
Mehrmaliges Vorhandensein einer Information in einem Datenbestand
\item \textbf{Integrität}\\
Gewährleistung einer korrekten und widerspruchsfreien Speicherung der Daten
\item \textbf{Schlüssel}\\
Schlüssel dienen in Datenbanken zur Identifizierung eines Datensatzes oder einer Gruppe von Datensätzen
\end{itemize}

\newpage
\subsection{Phasen des Datenbankentwurfs}
\begin{enumerate}
\item \textbf{Anforderungsanalyse}\\
Ermittlung der Anforderungen der Nutzer\\
\textit{Einsatzkonzept, Fragebogen}
\item \textbf{Konzeptioneller Entwurf}\\
Entwurf der konzeptionellen Globalansicht\\
\textit{Informationsmodell (z.B. ER-Modell oder UML) für das Ist- und Soll-Modell}
\item \textbf{Wahl des Ziel-DBMS}\\
Wahl des konkreten zu verwendenden DBS\\
\textit{Kriterienkatalog}
\item \textbf{Verteilungsentwurf} (nicht immer)\\
Festlegung der Fragmentierung des DBS\\
\textit{Fragmentierungsdesign für verteilte Datenbanken}
\item \textbf{Logischer Entwurf}\\
Transformation der konzentrierten Sicht in das verarbeitungsorientierte logische Modell\\
\textit{z.B. relationale Tabellenstruktur}
\item \textbf{Physischer Entwurf}\\
Definition des Schemas mit der Datenorganisation und -struktur\\
\textit{Optimierung der Struktur (Datenbankbefehle)}
\item \textbf{Realisierung}\\
Programmierung der Elemente und Sichten\\
\textit{Detaillierte Implementierung der Datenbank mit SQL}
\end{enumerate}

\newpage
\subsection{Einfache Datenbankumgebung}
\includegraphics[width=0.75\linewidth]{Bilder/simple_environment.png}

\subsection{Komponenten eines Datenbanksystems}
\includegraphics[width=0.5\linewidth]{Bilder/components.png}

\newpage
\subsection{Typische Komponenten eines DBMS}
\includegraphics[width=\linewidth]{Bilder/components_dbms.png}\\
Genauerer Blick auf die \textbf{Datenebene}:\\
\includegraphics[width=\linewidth]{Bilder/datenebene.png}

\newpage
\subsection{Typische grundlegende Systemarchitektur DBS-basierter Anwendungssysteme}
\includegraphics[width=\linewidth]{Bilder/architecture_dbs.png}

\subsection{Kriterien für eine DBS-Auswahl}
\begin{itemize}
\item \textbf{Effektivität}\\
Effektiver Leistungsumfang
\item \textbf{Effizienz}\\
Geringer Aufwand
\item \textbf{Kosten}\\
Lizenz- und Folgekosten
\item \textbf{Schnittstellen}\\
Passen unterschiedlicher Werkzeuge
\item \textbf{Verfügbarkeit}\\
System existiert in der Zielumgebung
\end{itemize}

\newpage
\subsection{Forderungen an ein DBS}
\begin{itemize}
\item \textbf{Redundanzfreiheit der Daten}
\begin{itemize}
\item Hierunter versteht man die Ablage von Daten ohne mehrfache Speicherung der gleichen Daten für eine Nutzergruppe.
\item Vorteile der Redundanzfreiheit sind Speicherplatzersparnis und Sicherung der Konsistenz.
\item In heutigen Datenbank-Applikationen wird meistens nur noch die redundanzarme Datenspeicherung (also kontrollierte Mehrfachspeicherung) verlangt.
\end{itemize}
\item \textbf{Multiuser-Betrieb}
\begin{itemize}
\item Die Datenbank muss einen Datenbankzugriff für mehrere parallel arbeitende Nutzer anbieten.
\item Nutzer, die auf den gleichen Datenbestand zugreifen, dürfen sich möglichst nicht behindern.
\end{itemize}
\item \textbf{Entfernter Zugriff auf Daten}
\begin{itemize}
\item Zunehmend wird ein entfernter Zugriff auf die Datenbank gefordert.
\item Es müssen vernetzte Daten schnell verfügbar sein.
\end{itemize}
\item \textbf{Kopplungsmöglichkeiten von Programmiersprachen}
\begin{itemize}
\item Die Kopplung von Datenbanken und Programmiersprachen muss realisierbar sein, um fehlende oder uneffektive Komponenten der DB-Sprachen mit Programmiersprachen nachzubilden.
\end{itemize}
\item \textbf{Datenunabhängigkeit}
\begin{itemize}
\item Mit der Datenunabhängigkeit in Datenbanken soll eine korrekte Weiterarbeit nach Änderungen von Strukturen oder der Hardware gewährleistet werden.
\item Änderungen von DB-Programmen dürfen keine Auswirkungen auf die Datenbank haben und umgekehrt.
\end{itemize}
\item \textbf{Kurze Antwortzeiten}
\begin{itemize}
\item Ein akzeptables Antwortzeitverhalten bei Datenbankanfragen wird erwartet.
\item Insbesondere bei sehr großen oder vernetzten Datenbanken ist das Antwortzeitverhalten zu beachten.
\end{itemize}
\item \textbf{Datensicherheit}
\begin{itemize}
\item Organisatorische und technische Maßnahmen müssen ungewollte Datenverluste verhindern.
\item Im Fehlerfall sind Restart-Maßnahmen vorzusehen.
\item Für die Datenbankkonsistenz ist das Sperren von Datenbereichen bei Mehrfachzugriff notwendig
\end{itemize}
\item \textbf{Effektive Datenorganisation}
\begin{itemize}
\item Die Ablage der Daten ist so zu wählen, dass Datenmanipulationen in vertretbaren Zeiteinheiten erfolgen können
\item Bei verteilten Datenbeständen ist die Ortstransparenz (Kennzeichnung des Speicherungsortes der Daten) zu beachten
\end{itemize}
\end{itemize}

\newpage
\datum{18.04.2020}
\subsection{Inhaltliche Anforderungen an ein Datenbanksystem}
\begin{itemize}
\item \textbf{Datenbankeingabemöglichkeit}
\begin{itemize}
\item Das DBS muss die Eingabe von Daten unterschiedlicher Typen (z.B. multimediale Daten) zulassen.
\end{itemize}
\item \textbf{Realisierung unterschiedlicher Sichten}
\begin{itemize}
\item Das unabhängige Arbeiten verschiedener Anwender auf dem gleichen Datenbestand ist zu gewährleisten.
\item Jeder Nutzer der Datenbank kann nur auf den für ihn vorgesehenen Datenbereich zugreifen.
\end{itemize}
\item \textbf{Datendefinition und -manipulation}
\begin{itemize}
\item In der Datenbank müssen die erforderlichen Datenstrukturen formulierbar sein und die erhaltenen Daten sollten möglichst deklarativ aufgerufen und verändert werden können.
\end{itemize}
\item \textbf{Integritätskonzepte}
\begin{itemize}
\item Es müssen Konzepte in der Datenbank enthalten sein, die eine Angabe von Bedingungen zur fehlerfreien Speicherung der Daten ermöglichen.
\end{itemize}
\item \textbf{Ad-hoc-Anfragen}
\begin{itemize}
\item Eine einfache Anfragesprache für den Aufruf und die Manipulation der Daten muss bereitgestellt werden.
\end{itemize}
\item \textbf{Effektiver Änderungsdienst}
\begin{itemize}
\item Daten- und Programmänderungen müssen einfach handhabbar sein
\end{itemize}
\end{itemize}

\newpage
\subsection{Datenunabhängigkeit}
\begin{center}
\includegraphics[width=0.5\linewidth]{Bilder/datenunabh.png}
\end{center}
\textbf{Stufen der Datenunabhängigkeit}:
\begin{itemize}
\item \textbf{Ziel der Datenunabhängigkeit}
\begin{itemize}
\item Ziel der Datenunabhängigkeit ist es, die Auswirkungen nach DB-Änderungen bzw. Programmänderungen möglichst gering zu halten (Trennung von DB und Anwendungen).
\end{itemize}
\item \textbf{Unterscheidung}
\begin{itemize}
\item physische Datenunabhängigkeit (Invarianz des Schemas bei physischer Änderung).\\
Diese Unabhängigkeit wird realisiert durch die Geräte- und Speicherungsstrukturunabhängigkeit.
\item logische Datenunabhängigkeit (Invarianz eines Anwendungssystems bei Änderung der DB-Struktur oder anderer Anwendungssysteme).\\
Diese Unabhängigkeit wird realisiert durch die Datenstruktur- und Anwendungsunabhängigkeit.
\end{itemize}
\item \textbf{Geräteunabhängigkeit}
\begin{itemize}
\item Die Anwendungssysteme und das sie verwendende Schema sollen unabhängig von der Hardware sein.
\end{itemize}
\item \textbf{Speicherungsstrukturunabhängigkeit}
\begin{itemize}
\item Die Anwendung und das Schema sollen unabhängig von den internen Speicherungsstrukturen (z.B. Bäume) sein.
\end{itemize}
\item \textbf{Datenstrukturunabhängigkeit}
\begin{itemize}
\item Änderungen der DB-Struktur sollen keine Auswirkungen auf Anwendungsprogramme haben.
\end{itemize}
\item \textbf{Anwendungsunabhängigkeit}
\begin{itemize}
\item Die Anwendungssysteme sollen untereinander unabhängig sein.
\end{itemize}
\end{itemize}

\newpage
\subsection{Schlüssel in Datenbanken}
Begriffe:
\begin{itemize}
\item \textbf{Schlüssel}\\
Schlüssel dienen in Datenbanken zur Identifizierung eines Datensatzes oder einer Gruppe von Datensätzen.
\item \textbf{Schlüssel in Relationen}\\
Ein Schlüssel einer Relation stellt eine minimale Menge von Attributen dar, deren Werte die Tupel innerhalb der Relation eindeutig identifizieren.
\item \textbf{Schlüsselkandidat}\\
Ein Schlüsselkandidat ist ein Datenelement bzw. Kombination von Datenelementen, das bzw. die eine Schlüssel (i.d.R. den Primärschlüssel) bilden können.
\item \textbf{Eindeutiger Schlüssel}\\
Ein eindeutiger Schlüssel liegt vor, wenn jeder unterschiedliche Datensatz durch einen unterschiedlichen Schlüsselwert charakterisiert wird.
\item \textbf{Mehrdeutiger Schlüssel}\\
Bei mehrdeutigen Schlüsseln können mehrere Datensätze den gleichen Schlüsselwerte besitzen.
\item \textbf{Einfacher Schlüssel}\\
Ein einfacher Schlüssel besteht aus einem einzigen Identifikationsbegriff (im relationalen Modell aus einem Attribut).
\item \textbf{Zusammengesetzter Schlüssel}\\
Ein zusammengesetzter Schlüssel besteht aus mindestens zwei Identifikationsbegriffen.
\item \textbf{Primärschlüssel}\\
Als Primärschlüssel (häufig nur als Schlüssel bezeichnet) wird der eindeutige Schlüssel benannt, mit dessen Hilfe normalerweise die Datensätze verarbeitet werden.
\item \textbf{Sekundärschlüssel}\\
Als Sekundärschlüssel wird ein Schlüssel bezeichnet, mit dessen Hilfe seltener als mit Hilfe von Primärschlüsseln auf die Datensätze zugegriffen wird.\\
Sekundärschlüssel brauchen nicht eindeutig zu sein.
\item \textbf{Fremdschlüssel}\\
Fremdschlüssel sind Datenfelder (Attribute), die auf fremde Primärschlüssel referenzieren.
\item \textbf{Eingebetteter Schlüssel}\\
Ist der Schlüssel Bestandteil des Datensatzes (z.B. des Tupels), wird er eingebetteter Schlüssel genannt.
\item \textbf{Nicht eingebetteter Schlüssel}\\
Ein nicht eingebetteter Schlüssel ist nicht Bestandteil des Datensatzes. Er wird häufig vom DBMS gebildet und steht auf einem separaten Feld.
\item \textbf{Komplexer Schlüssel}\\
Komplexe Schlüssel treten in neueren Datenmodellen auf. Sie werden aus komplexen, nicht-atomaren Attributen gebildet (Beispiel: Das Attribut (PLZ Ort) sei ein Attribut, das den Primärschlüssel bildet).
\end{itemize}

\newpage
\subsection{Integrität einer Datenbank}
\subsubsection{Allgemeine Begriffsbestimmung}
Integrität der Datenbank bedeutet die Gewährleistung einer korrekten und widerspruchsfreien Speicherung von Daten und die Vermeidung aller möglichen Störungen bzw. eine Reaktion eines integeren Zustandes auf diese Störungen.\\
\ \\
\textbf{Reichweite von Integritätsbedingungen}\\
Die Reichweite einer Integritätsbedingung sollte möglichst gering gehalten werden.\\
\ \\
\textbf{Realisierung in DBMS}\\
Viele Datenbank-Produkte gestatten die Vereinbarung von Integritätsbedingungen.\\
In den kommerziellen klassischen Datenbanken sind jedoch meistens nur wenige Integritätskonzepte realisiert.

\begin{center}
\textbf{Integritätsbedingungen in (relationalen Datenbanken}\\
\includegraphics[width=0.8\linewidth]{Bilder/integrity.png}
\end{center}

\newpage
\subsubsection{Physische und semantische Integrität}
\begin{itemize}
\item \textbf{Physische Integrität}
\begin{itemize}
\item Behandelt alle Aspekte der physikalischen Speicherung von Daten
\item Berücksichtigt Hard- und Softwarefehler
\item Behandelt die Probleme des gleichzeitigen Zugriffs auf Daten
\end{itemize}
\item \textbf{Semantische Integrität}
\begin{itemize}
\item Die semantische Integrität beschreibt die zulässigen Zustände der Datenbank.
\item Sie legt fest, welche Werte erlaubt sein sollen und welche Beziehungen zwischen Datenelementen möglich sind.
\item Das DBS überprüft mit semantischen Integritätsbedingungen, ob eine gewünschte Änderung der Datenbank zulässig ist.
\item Semantische Integritätsbedingungen werden nach dem sie behandelnden Inhalt unterschieden in:
\begin{itemize}
\item Strukturelle Integritätsbedingungen
\item Werteabhängige Integritätsbedingungen
\item Operationale Integritätsbedingungen
\end{itemize}
\end{itemize}
\end{itemize}


\subsubsection{Werteabhängige und operationale (dynamische) Integrität}
\textbf{Werteabhängige Integritätsbedingungen}
\begin{itemize}
\item Die werteabhängige Integrität bezeichnet die Forderung, nicht alle durch Entity-Typen und Relationship-Typen definierten Datenbankzustände zuzulassen.
\item Werteabhängige Integritätsbedingungen treffen Aussagen über den Zusammenhang von Werten zwischen Datenelementen.
\item Sie beschränken die im Anwendungsprogramm möglichen Datenelemente auf Teilmengen.
\item Da sie zulässige \textit{Datenbankzustände} beschreiben, bezeichnet man sie auch als \textit{statische Integritätsbedingungen}.
\end{itemize}

\textbf{Operationale (dynamische) Integritätsbedingungen}
\begin{itemize}
\item Durch operationale Integritätsbedingungen werden korrekte \textit{Zustandsübergänge} gewährleistet.
\item Operationale Integritätsbedingungen überwachen die Ausführung von Operationen.
\item Wenn die Integrität verletzt wird, werden operationale Integritätsbedingungen integritätssicherstellend aktiv.
\item Durch die Angabe von Integritätsbedingungen wird die Ausführung einer Transaktion von der Einhaltung von Vor- und Nachbedingungen abhängig gemacht.
\end{itemize}

\subsubsection{Strukturelle Integrität}
\textbf{Strukturelle Integritätsbedingungen}
\begin{itemize}
\item Strukturelle Integritätsbedingungen sind vom Datenmodell abhängig.
\item Im relationalen Datenmodell werden unterschieden:
\begin{itemize}
\item Typintegrität
\item Schlüsselintegrität
\item Referentielle Integrität
\end{itemize}
\end{itemize}

\ \\
\textbf{Referentielle Integrität}
\begin{itemize}
\item Die Voraussetzung der Existent bestimmter Datenobjekte zur korrekten Interpretation der in anderen Datenobjekten enthaltenen Identifikatoren wird referentielle Integrität genannt.
\item Die referentielle Integrität stellt beispielsweise sicher, dass Bezüge auf referenzierte Datenobjekte nur dann korrekt sind, wenn diese Datenobjekte auch vorhanden sind
\item[] \includegraphics[width=0.7\linewidth]{Bilder/referential_integrity.png}
\end{itemize}

\subsubsection{Semantische Integrität relationaler Datenbanken}
\textbf{Integrität in Datenbanken}
\begin{itemize}
\item In kommerziellen Datenbanken wurde die automatische, zentrale Datenüberwachung erst im SQL-89-Standard verbindlicher.
\item Referentielle Integrität muss gewährleistet werden, damit z.B. keine \glqq dangling reference\grqq{} (Fremdschlüsselverweise ohne zugehörigen Primärschlüssel) entstehen.
\end{itemize}
\textbf{Strukturelle Integrität in SQL}
\begin{itemize}
\item \textit{unique} bezeichnet einen eindeutigen Schlüssel
\item \textit{primary key} markiert den Primärschlüssel und setzt automatisch auf not null
\item \textit{foreign key} definiert den Fremdschlüssel (er wird nicht automatisch auf not null gesetzt).
\item \textit{unique foreign key} modelliert eine 1:1-Beziehung
\item Beispiel für den Umgan mit referentieller Integrität:\\
Kaskadierendes Löschen: wurde für einen Fremdschlüssel \textit{on update cascade} angegeben, so wird auch der referenzierte Schlüssel gelöscht
\end{itemize}
\textbf{Einfache statische Integritätsbedingungen}\\
Werteunabhängige Integritäten mit einfachen Booleschen Ausdrücken werden mit der check-Anweisung implementiert:
\begin{itemize}
\item check (Semester between 1 and 13)
\item check Rang in ('C2', 'C3', 'C4')
\end{itemize}





\end{document}