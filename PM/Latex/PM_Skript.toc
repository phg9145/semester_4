\babel@toc {german}{}
\contentsline {section}{\numberline {1}Grundlagen}{4}%
\contentsline {subsection}{\numberline {1.1}Merkmale eines Projekts}{4}%
\contentsline {subsubsection}{\numberline {1.1.1}Das Magische Projektdreieck}{5}%
\contentsline {subsubsection}{\numberline {1.1.2}Das Teufelsquadrat nach Sneed}{5}%
\contentsline {subsection}{\numberline {1.2}Projektmanagement}{6}%
\contentsline {subsection}{\numberline {1.3}Grundsätze des Projektmanagements}{6}%
\contentsline {subsubsection}{\numberline {1.3.1}Der Projektmanagement-Regelkreis}{6}%
\contentsline {subsubsection}{\numberline {1.3.2}PDCA-Zyklus nach Deming}{6}%
\contentsline {section}{\numberline {2}Projektorganisation}{7}%
\contentsline {subsection}{\numberline {2.1}Formen der Projektorganisation}{7}%
\contentsline {subsubsection}{\numberline {2.1.1}Projektmanagement in der Linie}{8}%
\contentsline {subsubsection}{\numberline {2.1.2}Einfluss-Projektorganisation}{9}%
\contentsline {subsubsection}{\numberline {2.1.3}Matrix-Projektorganisation}{10}%
\contentsline {subsubsection}{\numberline {2.1.4}Reine Projektorganisation}{11}%
\contentsline {subsection}{\numberline {2.2}Unternehmensübergreifende Projektorganisation}{12}%
\contentsline {subsubsection}{\numberline {2.2.1}Einzelauftragsorganisation}{12}%
\contentsline {subsubsection}{\numberline {2.2.2}Konsortialorganisation}{12}%
\contentsline {subsubsection}{\numberline {2.2.3}Generalunternehmerorganisation (GU-Projekt)}{12}%
\contentsline {subsection}{\numberline {2.3}Rollen in einer Projektorganisation}{12}%
\contentsline {section}{\numberline {3}Projektphasen}{13}%
\contentsline {subsection}{\numberline {3.1}Der Projektmanagement-Regelkreis}{13}%
\contentsline {subsection}{\numberline {3.2}Vorgehensmodelle}{14}%
\contentsline {subsubsection}{\numberline {3.2.1}Wasserfallmodell}{15}%
\contentsline {subsubsection}{\numberline {3.2.2}V-Modell}{16}%
\contentsline {subsubsection}{\numberline {3.2.3}Iterativ inkrementelles Vorgehensmodell}{17}%
\contentsline {subsection}{\numberline {3.3}Projektziele}{19}%
\contentsline {subsection}{\numberline {3.4}Umfeldanalyse}{20}%
\contentsline {subsubsection}{\numberline {3.4.1}Projektumfeld-Analyse}{20}%
\contentsline {subsubsection}{\numberline {3.4.2}Stakeholder-Analyse}{20}%
\contentsline {subsection}{\numberline {3.5}Projektrisikoabschätzung}{23}%
\contentsline {subsection}{\numberline {3.6}Projekt-Kick-Off}{26}%
\contentsline {section}{\numberline {4}Projektplanung}{27}%
\contentsline {subsection}{\numberline {4.1}Der Projektstrukturplan (PSP)}{28}%
\contentsline {subsubsection}{\numberline {4.1.1}Erstellung eines PSP}{30}%
\contentsline {subsection}{\numberline {4.2}Termin- und Ablaufplanung}{32}%
\contentsline {subsubsection}{\numberline {4.2.1}Planung der Fristen und Termine}{32}%
\contentsline {subsubsection}{\numberline {4.2.2}Projektphasen und Meilensteine}{32}%
\contentsline {subsubsection}{\numberline {4.2.3}Balkenpläne (Gantt-Diagramme)}{33}%
\contentsline {subsubsection}{\numberline {4.2.4}Netzplantechnik - Generell}{34}%
\contentsline {subsubsection}{\numberline {4.2.5}Netzplantechnik - Vorgangsknoten-Netzplan (VKN) - Methode MPM}{35}%
\contentsline {subsubsection}{\numberline {4.2.6}Netzplantechnik - Vorgangspfeil-Netzplan (VPN) - Methode CPM}{39}%
\contentsline {subsubsection}{\numberline {4.2.7}Netzplantechnik - Ereignisknoten-Netzplan (EKN) - Methode PERT}{39}%
\contentsline {subsection}{\numberline {4.3}Feinplanung \& Optimierung}{40}%
\contentsline {subsubsection}{\numberline {4.3.1}Terminbeschleunigung}{40}%
\contentsline {subsubsection}{\numberline {4.3.2}Feinplanung}{41}%
\contentsline {section}{\numberline {5}Projektcontrolling}{42}%
\contentsline {subsection}{\numberline {5.1}Projektfortschrittskontrolle}{43}%
\contentsline {subsection}{\numberline {5.2}Trendanalysen}{45}%
\contentsline {subsubsection}{\numberline {5.2.1}Meilenstein-Trendanalyse (MTA)}{45}%
\contentsline {subsubsection}{\numberline {5.2.2}Kosten-Trendanalyse (KTA)}{45}%
\contentsline {subsubsection}{\numberline {5.2.3}Earned Value Analysis}{47}%
\contentsline {section}{\numberline {6}Agiles Projektmanagement - Scrum}{49}%
\contentsline {subsection}{\numberline {6.1}Das Agile Manifest}{49}%
\contentsline {subsection}{\numberline {6.2}Rollen}{50}%
\contentsline {subsubsection}{\numberline {6.2.1}Team}{50}%
\contentsline {subsubsection}{\numberline {6.2.2}Product Owner}{50}%
\contentsline {subsubsection}{\numberline {6.2.3}Scrum-Master}{50}%
\contentsline {subsection}{\numberline {6.3}Bestandteile - Backlogs}{50}%
\contentsline {subsubsection}{\numberline {6.3.1}Product Backlog}{50}%
\contentsline {subsubsection}{\numberline {6.3.2}Sprint Backlog}{50}%
\contentsline {subsection}{\numberline {6.4}Ablauf}{51}%
\contentsline {subsection}{\numberline {6.5}Verbesserung von Scrum}{51}%
\contentsline {subsection}{\numberline {6.6}Zusammenfassung}{51}%
\contentsline {section}{\numberline {7}Aufwandsschätzung}{53}%
\contentsline {subsection}{\numberline {7.1}Methoden}{54}%
\contentsline {subsubsection}{\numberline {7.1.1}Bottom-Up}{54}%
\contentsline {subsubsection}{\numberline {7.1.2}Top-Down}{54}%
\contentsline {subsection}{\numberline {7.2}Kostenschätzung mit Expertengruppen}{54}%
\contentsline {subsubsection}{\numberline {7.2.1}Schätzklausur}{54}%
\contentsline {subsubsection}{\numberline {7.2.2}Die Delphi-Methode}{55}%
\contentsline {subsection}{\numberline {7.3}Zahlen für die Abschätzung}{55}%
\contentsline {subsubsection}{\numberline {7.3.1}Die Zwei-Punkt-Schätzung}{55}%
\contentsline {subsubsection}{\numberline {7.3.2}Die Drei-Punkt-Schätzung}{55}%
\contentsline {subsection}{\numberline {7.4}Unter- / Überschätzung}{56}%
\contentsline {subsection}{\numberline {7.5}Projektverlängerung \& Zeitdruck}{56}%
\contentsline {subsection}{\numberline {7.6}Ursachen für mangelnde Schätzgenauigkeit}{56}%
\contentsline {section}{\numberline {8}Kostenplanung}{57}%
\contentsline {subsection}{\numberline {8.1}Basis}{57}%
\contentsline {subsection}{\numberline {8.2}Kostentransparenz}{57}%
\contentsline {subsubsection}{\numberline {8.2.1}Kostenart}{57}%
\contentsline {subsubsection}{\numberline {8.2.2}Kostenträger}{58}%
\contentsline {subsubsection}{\numberline {8.2.3}Kostenstelle}{58}%
\contentsline {subsubsection}{\numberline {8.2.4}Beispielzuordnung}{58}%
\contentsline {subsection}{\numberline {8.3}Kostenminimale Projektbeschleunigung}{58}%
\contentsline {subsubsection}{\numberline {8.3.1}Projektkostenarten}{59}%
\contentsline {subsubsection}{\numberline {8.3.2}Vorbereitung}{59}%
\contentsline {subsubsection}{\numberline {8.3.3}Ford-Fulkerson-Algorithmus}{59}%
