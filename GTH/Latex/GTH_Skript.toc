\babel@toc {german}{}
\contentsline {section}{\numberline {1}Einleitung}{3}%
\contentsline {subsection}{\numberline {1.1}Grundbegriffe}{4}%
\contentsline {subsection}{\numberline {1.2}Grammatik}{5}%
\contentsline {subsection}{\numberline {1.3}Chomsky-Hierarchie}{6}%
\contentsline {subsection}{\numberline {1.4}Wortproblem}{8}%
\contentsline {subsection}{\numberline {1.5}Backus-Naur-Form}{9}%
\contentsline {section}{\numberline {2}Reguläre Sprachen}{10}%
\contentsline {subsection}{\numberline {2.1}Endliche Automaten (EA)}{10}%
\contentsline {subsubsection}{\numberline {2.1.1}Deterministische Endliche Automaten (DEA)}{10}%
\contentsline {subsubsection}{\numberline {2.1.2}Nichtdeterministische endliche Automaten}{13}%
\contentsline {subsubsection}{\numberline {2.1.3}NEA mit $\varepsilon $-Kanten}{17}%
\contentsline {subsection}{\numberline {2.2}Reguläre Ausdrücke, RA}{19}%
\contentsline {subsection}{\numberline {2.3}Rechnen mit regulären Ausdrücken}{22}%
\contentsline {subsubsection}{\numberline {2.3.1}Algebraische Umformungen}{22}%
\contentsline {subsubsection}{\numberline {2.3.2}Rechenregeln für reuläre Ausdrücke}{23}%
\contentsline {subsection}{\numberline {2.4}Das Pumping Lemma}{25}%
\contentsline {subsection}{\numberline {2.5}Der Satz von Myhill/Nerode}{26}%
\contentsline {subsubsection}{\numberline {2.5.1}Definition}{26}%
\contentsline {subsubsection}{\numberline {2.5.2}Hilfsmittel}{26}%
\contentsline {subsubsection}{\numberline {2.5.3}Beweis}{27}%
\contentsline {subsection}{\numberline {2.6}DEA-Minimierung}{28}%
\contentsline {section}{\numberline {3}Kontextfreie Sprachen}{31}%
\contentsline {subsection}{\numberline {3.1}Normalformen}{32}%
\contentsline {subsection}{\numberline {3.2}Pumping Lemma für kontextfreie Sprachen}{34}%
\contentsline {subsection}{\numberline {3.3}Abschlusseigenschaften}{34}%
\contentsline {subsection}{\numberline {3.4}Der CYK-Algorithmus}{35}%
\contentsline {subsection}{\numberline {3.5}Kellerautomaten}{37}%
\contentsline {section}{\numberline {4}Turing-Maschinen}{39}%
\contentsline {section}{\numberline {5}Berechenbarkeit}{47}%
\contentsline {subsection}{\numberline {5.1}Grundlagen}{47}%
\contentsline {subsubsection}{\numberline {5.1.1}Diagonalverfahren 1 von Cantor}{48}%
\contentsline {subsubsection}{\numberline {5.1.2}Nicht abzählbare Mengen}{48}%
\contentsline {subsubsection}{\numberline {5.1.3}Diagonalverfahren 2 von Cantor}{49}%
\contentsline {subsubsection}{\numberline {5.1.4}Hilfsfunktion für die Definition der Turing-Berechenbarkeit}{49}%
\contentsline {subsection}{\numberline {5.2}Funktion $f_T$ der Turing-Maschine}{49}%
\contentsline {subsection}{\numberline {5.3}Turing-berechenbare Funktion}{50}%
