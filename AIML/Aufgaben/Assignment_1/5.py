def mergeSort(eingabe):
    if len(eingabe) == 2:
        st = bubbleSort(eingabe[0], eingabe[1])
        print(st)
        return st
    
    middle = int(len(eingabe)/2)
    st = bubbleSort(mergeSort(eingabe[:middle]), mergeSort(eingabe[middle:]))
    print(st)
    return st
    

def bubbleSort(eingabe1, eingabe2):
    st = list(eingabe1)
    st.extend(list(eingabe2))
    
    changed = True
    
    while changed:
        changed = False
        for x in range(len(st) -1):
            if ord(st[x]) > ord(st[x+1]):
                n = st[x]
                st[x] = st[x+1]
                st[x+1] = n
                changed = True
        #if changed:
            #print("".join(st))
    return "".join(st)

if __name__== "__main__":
    mergeSort(input())