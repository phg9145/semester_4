def doSomething():
    st = input()
    
    newSt = st[0]
    
    if len(st) == 1:
        print(st)
    
    for x in range(1, len(st)):
        #print("--------------------")
        """Get element to insert"""
        el = st[x]
        #print(el)
        
        #print("newSt: " + newSt)
        #print("st: " + st)
        #print("Element: " + el)
        
        """Insert element into list"""
        inserted = False
        for y in range(len(newSt)):
            if ord(el) < ord(newSt[y]):
                s = newSt[:y] + el + newSt[y:]
                inserted = True
                newSt = s
                break
        if not inserted:
            newSt += el
        
        """Print list with inserted element + rest of st"""
        print(newSt + st[x+1:])
    

if __name__== "__main__":
    doSomething()