# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 12:40:40 2020

@author: PerpPhil
"""

class node:
    
    def __init__(self, name):
        self.name = name
        self.neighbors = {}
        self.way = ""
        self.visited = False
        self.finalValue = 0
    
    def setNeighbor(self, neighbor, value):
        self.neighbors[neighbor] = int(value)
    
    def setWay(self, way):
        self.way = way
    
    def getNeighbors(self):
        return self.neighbors
    
    def getVisited(self):
        return self.visited
    
    def visit(self, value):
        self.visited = True
        self.finalValue = value
    
    def getVal(self):
        return self.finalValue
    
    def getWay(self):
        return self.way
    
    def printAll(self):
        print("--------")
        print("Node " + self.name)
        for n in self.neighbors:
            print("Neighbor: " + n + ", " + str(self.neighbors[n]))
        print("Way: " + self.way)
        print("Visited: " + str(self.visited))
        print("Final Value: " + str(self.finalValue))
        print("--------")


def Dijkstra():
    #with open("input2.txt") as f:
        #lines = f.readlines()
    
    allLines = []
    #for l in lines:
        #allLines.append(l.rstrip())
    while True:
        try:
            allLines.append(input())
        except EOFError:
            break
    
    #print(allLines)
    
    allNodes = {}
    for l in allLines:
        sp = l.split("-")
        name = sp[0]
        nei = sp[1]
        val = sp[2]
        if name not in allNodes:
            allNodes[name] = node(name)
        n = allNodes[name]
        n.setNeighbor(nei, val)
    
    costs = {}
    
    for n in allNodes:
        #allNodes[n].printAll()
        costs[n] = 10000
    
    #print(costs)
    #for n in allNodes:
        #allNodes[n].printAll()
    
    firstNode = allLines[0][0]
    #print("First Node: " + firstNode)
    costs[firstNode] = 0
    
    while not allVisited(allNodes):
        currentNodeId = getLowest(costs, allNodes)
        currentNode = allNodes[currentNodeId]
        currentNode.visit(costs[currentNodeId])
        #del costs[currentNodeId]
        neighbors = currentNode.getNeighbors()
        for n in neighbors:
            newCost = neighbors[n] + costs[currentNodeId]
            if not allNodes[n].getVisited() and newCost < costs[n]:
                costs[n] = newCost
                allNodes[n].setWay(currentNodeId + "-" + currentNode.getWay())
    
    #for n in allNodes:
        #allNodes[n].printAll()
    
    for n in sorted(allNodes):
        print(n + "-" + allNodes[n].getWay() + str(allNodes[n].getVal()))
    

def getLowest(costs, allNodes):
    low = 100000
    lowNode = ""
    for n in costs:
        if costs[n] < low and not allNodes[n].getVisited():
            lowNode = n
            low = costs[n]
    #print("In getLowest, lowest: " + lowNode + ",costs: " + str(costs))
    return lowNode

def allVisited(allNodes):
    for n in allNodes:
        if not allNodes[n].getVisited():
            return False
    return True

if __name__== "__main__":
    Dijkstra()