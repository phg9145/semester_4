# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 12:40:40 2020

@author: PerpPhil
"""

def inputSyntax():
    
    allLinks = []
    while True:
        try:
            allLinks.append(input())
        except EOFError:
            break

def BFS():
    with open("inputBFS.txt") as f:
        lines = f.readlines()
    
    allLinks = []
    for l in lines:
        allLinks.append(l.rstrip())
    
    #print(allLinks)
    
    adjList = {}
    for l in allLinks:
        liste = []
        for x in range(2, len(l)):
            liste.append(l[x])
        adjList[l[0]] = liste
    
    #print(adjList)
    
    queue = []
    visited = {}
    
    for node in adjList:
        visited[node] = False
    
    #current = next(iter(adjList))
    current = list(adjList.keys())[0]
    #print(adjList.keys())
    visited[current] = True
    queue = adjList[current]
    st = current
    #print("Current: " + current)
    
    while len(queue) != 0:
        current = queue.pop(0)
        if not visited[current]:
            queue.extend(adjList[current])
            visited[current] = True
            #print("Current: " + current)
            #print("Queue: " + str(queue))
            st += current
    
    print(st)

def DFS():
    with open("inputDFS.txt") as f:
        lines = f.readlines()
    
    allLinks = []
    for l in lines:
        allLinks.append(l.rstrip())
    #print(allLinks)

    adjList = {}
    for l in allLinks:
        liste = []
        for x in range(2, len(l)):
            liste.append(l[x])
        adjList[l[0]] = liste
    
    
    queue = []
    visited = {}
    
    for node in adjList:
        visited[node] = False
    
    current = next(iter(adjList))
    visited[current] = True
    #queue = adjList[current]
    l = adjList[current]
    l.reverse()
    queue.extend(l)
    st = current
    
    while len(queue) != 0:
        print(queue)
        current = queue.pop()
        if not visited[current]:
            print(current)
            l = adjList[current]
            l.reverse()
            queue.extend(l)
            visited[current] = True
            st += current
    
    print(st)


def DFS_rekursiv():
    with open("input.txt") as f:
        lines = f.readlines()
    
    allLinks = []
    for l in lines:
        allLinks.append(l.rstrip())
    global adjList
    
    adjList = {}
    for l in allLinks:
        liste = []
        for x in range(2, len(l)):
            liste.append(l[x])
        adjList[l[0]] = liste
    
    global visited
    visited = {}
    
    for node in adjList:
        visited[node] = False
    
    print(DFS_rek(next(iter(adjList))))


def DFS_rek(node):
    global visited
    global adjList
    if not visited[node]:
        visited[node] = True
        st = "" + node
        for n in adjList[node]:
            s = DFS_rek(n)
            if s != None:
                st += str(s)
        return st
    

if __name__== "__main__":
    #BFS()
    #DFS_rekursiv()
    DFS()