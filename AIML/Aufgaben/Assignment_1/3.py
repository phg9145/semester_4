# -*- coding: utf-8 -*-
"""
Created on Sun Mar 22 15:58:08 2020

@author: Philipp
"""

def doSomething():
    st = list(input())
    
    changed = True
    
    while changed:
        changed = False
        for x in range(len(st) -1):
            if ord(st[x]) > ord(st[x+1]):
                n = st[x]
                st[x] = st[x+1]
                st[x+1] = n
                changed = True
        if changed:
            print("".join(st))

if __name__== "__main__":
    doSomething()