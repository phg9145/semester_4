# -*- coding: utf-8 -*-
"""
Created on Sun Mar 22 15:58:08 2020

@author: Philipp
"""

def doSomething():
    st = ""
    e = EOFError
    e = ValueError
    
    while True:
        try:
            st += str(int(input())*2) + "\n"
        except e:
            break
    print(st)

if __name__== "__main__":
    doSomething()