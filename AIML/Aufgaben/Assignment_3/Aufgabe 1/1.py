# -*- coding: utf-8 -*-


import math


def hopfield():
    allLines = []

    with open("input_2.txt") as f:
        for l in f.readlines():
            allLines.append(l.rstrip().replace("*", "2").replace(".", "0"))
   # while True:
   #     try:
   #         allLines.append(input().rstrip().replace("*", "2").replace(".", "0"))
   #     except EOFError:
   #         break

    all_figures = []
    current = []

    final_input = False
    final_figures = []

    for l in allLines:
        if l == "---":
            all_figures.append(current)
            current = []
            final_input = True

        elif l != "-":
            current.extend(l)
        else:
            if final_input:
                final_figures.append(current)
            else:
                all_figures.append(current)
            current = []

    final_figures.append(current)

    for f in all_figures:
        print("Figure: " + str(f))
        print("Length: " + str(len(f)))

    print("--------------------------------------")

    for f in final_figures:
        print("Final Figure: " + str(f))
        print("Length: " + str(len(f)))


    weights = []
    for i in range(200):
        n = []
        for j in range(200):
            n.append(0)
        weights.append(n)

    number_of_muster = len(all_figures)
    print("number_of_muster: " + str(number_of_muster))

    for i in range(200):
        for j in range(i+1, 200):
            w = weights[i][j]

            for figure in all_figures:
                w += (int(figure[i])-1)*(int(figure[j])-1)

            weights[i][j] = w
            weights[j][i] = w

    # Trainingsphase vorüber, jetzt gehts rund
    schwelle = 0.5
    all_Print = []
    for fin in final_figures:
        for i in range(200):
            sum = 0
            for j in range(200):
                s_j = int(fin[j])-1
                sum += weights[i][j] * s_j
            if sum > schwelle:
                fin[i] = "2"
            else:
                fin[i] = "0"

        #fin = fin.replace("2","*").replace("0",".")
        line = ""
        for f in range(200):
            if f%20 == 0 and f != 0:
                #print(line)
                all_Print.append(line)
                line = ""
            if int(fin[f]) == 2:
                line += "*"
            else:
                line += "."
            #print(fin[f])
        #print(line)
        all_Print.append(line)
        #print()
        all_Print.append("-")

    for i in range(len(all_Print)-1):
        print(all_Print[i])





if __name__ == "__main__":
    hopfield()
