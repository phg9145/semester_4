# -*- coding: utf-8 -*-


import math
import random
import matplotlib.pyplot as plt



class Point:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def adjust(self, x, y, learning):
        self.x += (x - self.x) * learning
        self.y += (y - self.y) * learning


def cluster():
    training_runs = 200
    learning_rate = 0.005

    final_expected = 0.003967
    #final_expected = 9232.37937
    #final_expected = -1687.545882

    allLines = []

    with open("input_1.txt") as f:
        for l in f.readlines():
            allLines.append(l.rstrip())

    number = int(allLines.pop(0))

    x_max = 0
    x_min = 1000
    y_max = 0
    y_min = 1000

    all_Points = []

    for l in allLines:
        sp = l.split(",")
        x = float(sp[0])
        y = float(sp[1])
        p = Point(x, y)
        all_Points.append(p)
        if x > x_max:
            x_max = x
        if x < x_min:
            x_min = x

        if y > y_max:
            y_max = y
        if y < y_min:
            y_min = y

    x_middle = (x_max + x_min)/2
    y_middle = (y_max + y_min)/2

    x_new_max = x_middle + 0.2*(x_max - x_middle)
    x_new_min = x_middle - 0.2*(x_middle - x_min)
    y_new_max = y_middle + 0.2*(y_max - y_middle)
    y_new_min = y_middle - 0.2*(y_middle - y_min)

    print("x_min: " + str(x_new_min))
    print("x_max: " + str(x_new_max))
    print("y_min: " + str(y_new_min))
    print("y_max: " + str(y_new_max))
    print()

    all_center = []
    for i in range(number):
        x = random.uniform(x_new_min, x_new_max)
        y = random.uniform(y_new_min, y_new_max)
        p = Point(x, y)
        all_center.append(p)
        print("New center point: " + str(x) + "|" + str(y))

    for run in range(training_runs):
        random.shuffle(all_Points)
        for p in all_Points:

            # Bestimme das Zentrum, das am nächsten am gewählten Punkt liegt
            distance = 1000
            min_index = 0
            for c_index in range(number):
                new_distance = getDistance(p, all_center[c_index])
                if new_distance < distance:
                    distance = new_distance
                    min_index = c_index

            # Das nächste Zentrum ist jetzt bestimmt, jetzt muss dieses Zentrum noch angepasst werden
            all_center[min_index].adjust(p.getX(), p.getY(), learning_rate)

    final_sum = 0
    for p in all_center:
        final_sum += p.getX()
        final_sum += p.getY()

    x_values = []
    y_values = []
    for p in all_Points:
        x_values.append(p.getX())
        y_values.append(p.getY())

    center_x_values = []
    center_y_values = []
    for p in all_center:
        center_x_values.append(p.getX())
        center_y_values.append(p.getY())

    x_min_plot = x_min - 0.001
    x_max_plot = x_max + 0.001
    y_min_plot = y_min - 0.001
    y_max_plot = y_max + 0.001

    #print(final_sum)
    print("Final sum: " + str(final_sum))
    print("Differenz zum gewünschten Output: " + str(final_sum - final_expected))

    plt.plot(x_values, y_values, 'r.')
    plt.plot(center_x_values, center_y_values, 'b.')
    plt.axis([x_min_plot, x_max_plot, y_min_plot, y_max_plot])
    plt.show()




def getDistance(p1, p2):
    x_1 = p1.getX()
    y_1 = p1.getY()
    x_2 = p2.getX()
    y_2 = p2.getY()

    distance = math.sqrt((x_1-x_2)**2 + (y_1-y_2)**2)
    return distance


if __name__ == "__main__":
    cluster()
