# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 14:07:06 2020

@author: Philipp
"""

import math
import random
learning = 0.01

class node_hidden():
    
    def __init__(self, index, name, nextNodes):
        self.name = name
        self.index = index
        self.nextNodes = nextNodes
        self.gradient = 0
        self.output = 0
    
    def initWeights(self, weights):
        self.weights = weights
        
    def getOutput(self, inputs):
        summe = 0
        for i in range(len(inputs)):
            summe += inputs[i]*self.weights[i]
        self.output = math.tanh(summe + self.weights[-1])
        return self.output

    def calcGradient(self, inputs):
        #current = self.getOutput(inputs)
        current = self.output
        c = (1-current*current)
        gradient = 0
        for node in self.nextNodes:
            inputWeights = node.getWeights()
            #print(inputWeights)
            weight = inputWeights[self.index]
            gradientNextNode = node.getGradient()
            gradient += gradientNextNode*weight*c
        self.gradient = gradient
    
    def adjust(self, inputs):
        global learning
        for i in range(len(inputs)):
            newWeight = learning*self.gradient*inputs[i]
            self.weights[i] += newWeight
        self.weights[-1] += learning*self.gradient
    
    def getWeights(self):
        return self.weights
    
    def getGradient(self):
        return self.gradient
    
    def getName(self):
        return self.name













class node_output():
    
    def __init__(self, name):
        self.name = name
        self.gradient = 0
    
    def initWeights(self, weights):
        self.weights = weights
    
    def getOutput(self, inputs):
        summe = 0
        for i in range(len(inputs)):
            summe += inputs[i]*self.weights[i]
        
        return math.tanh(summe + self.weights[-1])
        #return round(math.tanh(x1+y1))
        
    def calcGradient(self, inputs, expected):
        current = self.getOutput(inputs)
        self.gradient = (-2)*(current - expected)*(1-current*current)

    def adjust(self, inputs):
        global learning
        for i in range(len(inputs)):
            newWeight = learning*self.gradient*inputs[i]
            self.weights[i] += newWeight
        self.weights[-1] += learning*self.gradient
    
    def getWeights(self):
        return self.weights
    
    def getGradient(self):
        return self.gradient
    
    def getName(self):
        return self.name



def normalize(inputList):
    absList1 = []
    absList2 = []
    for l in inputList:
        if l != "0,0,0":
            sp = l.split(',')
            #absList1.append(abs(float(sp[0])))
            #absList2.append(abs(float(sp[1])))
            absList1.append(float(sp[0]))
            absList2.append(float(sp[1]))
    maxElement1 = max(absList1)
    minElement1 = min(absList1)
    maxElement2 = max(absList2)
    minElement2 = min(absList2)
    difference1 = maxElement1-minElement1
    difference2 = maxElement2-minElement2
    output = []
    #print("maxElement1: " + str(maxElement1))
    #print("minElement: " + str(minElement))
    for l in inputList:
        if l != "0,0,0":
            sp = l.split(',')
            st = ""
            val1 = (float(sp[0])-minElement1)/(difference1) - 0.5
            val2 = (float(sp[1])-minElement2)/(difference2) - 0.5
            st += str(val1) + "," + str(val2)
            if len(sp) > 2:
                st += "," + sp[2]
            output.append(st)
        else:
            output.append(l)
    
    return output

def testRun():
    
    allLines = []

    while True:
        try:
            allLines.append(input())
        except EOFError:
            break
    
    happened = False
    training = []
    test = []
    
    allLines = normalize(allLines)
    
    for l in allLines:
        if l == "0,0,0":
            happened = True
        else:
            if not happened:
                training.append(l)
            else:
                test.append(l)
            
    node_2_0 = node_output("Node 2_0")
    
    node_1_0 = node_hidden(0, "Node 1_0", [node_2_0])
    node_1_1 = node_hidden(1, "Node 1_1", [node_2_0])
    node_1_2 = node_hidden(2, "Node 1_2", [node_2_0])
    node_1_3 = node_hidden(3, "Node 1_3", [node_2_0])
    
    layer1 = [node_1_0, node_1_1, node_1_2, node_1_3]
    
    node_0_0 = node_hidden(0, "Node 0_0", layer1)
    node_0_1 = node_hidden(1, "Node 0_1", layer1)
    node_0_2 = node_hidden(2, "Node 0_2", layer1)
    node_0_3 = node_hidden(3, "Node 0_3", layer1)
    
    layer0 = [node_0_0, node_0_1, node_0_2, node_0_3]
    
    for n in layer0:
        w = []
        for i in range(2):
            w.append(random.randint(-30, 30)/100)
        n.initWeights(w)
    
    for n in layer1:
        w = []
        for i in range(4):
            w.append(random.randint(-30, 30)/100)
        n.initWeights(w)
    
    w = []
    for i in range(4):
        w.append(random.randint(-30, 30)/100)
    node_2_0.initWeights(w)
    
    for runCounter in range(5000):
        for x in training:
            sp = x.split(',')
            x = float(sp[0])
            y = float(sp[1])
            
            ################################
            
            outL0 = []
            
            for n in layer0:
                outL0.append(n.getOutput([x,y]))
            
            outL1 = []
            
            for n in layer1:
                outL1.append(n.getOutput(outL0))
            
            outFinal = node_2_0.getOutput(outL1)
            
            ################################
            
            node_2_0.calcGradient(outL1, int(sp[2]))
            
            for n in layer1:
                n.calcGradient(outL0)
            
            for n in layer0:
                n.calcGradient([x,y])
            
            ################################
            
            node_2_0.adjust(outL1)
            
            for n in layer1:
                n.adjust(outL0)
            
            for n in layer0:
                n.adjust([x,y])
    
    for x in test:
        sp = x.split(',')
        x_Test = float(sp[0])
        y_Test = float(sp[1])
        
        outL0 = []
        for n in layer0:
            outL0.append(n.getOutput([x_Test, y_Test]))
        
        outL1 = []
        
        for n in layer1:
            outL1.append(n.getOutput(outL0))
        
        outL2 = node_2_0.getOutput(outL1)
        if outL2 < 0:
            print("-1")
        else:
            print("+1")
        
    
if __name__== "__main__":
    testRun()
    #print("Fertig")