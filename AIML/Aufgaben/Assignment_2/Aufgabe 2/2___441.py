# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 14:07:06 2020

@author: Philipp
"""

import math
import random
#learning = 0.251
learning = 0.021
learningOrig = 0.1
decay = 0.02

class node_hidden():
    
    def __init__(self, index, name, nextNodes):
        #global initWeight1
        #global initWeight2
        #self.weight0 = initWeight1
        #self.weight1 = initWeight2
        #self.biasWeight = initBiasWeight
        self.name = name
        self.index = index
        self.nextNodes = nextNodes
        self.gradient = 0
        self.output = 0
    
    def initWeights(self, weights):
        self.weights = weights
        
    def getOutput1(self, x, y):
        x1 = x*self.weight0
        y1 = y*self.weight1
        #exponent = 2*(x1+y1+self.biasWeight)
        #return 1-(2/(exp(exponent) + 1))
        return math.tanh(x1+y1 + self.biasWeight)
        #return round(math.tanh(x1+y1))

    def getOutput(self, inputs):
        summe = 0
        for i in range(len(inputs)):
            summe += inputs[i]*self.weights[i]
        s = math.tanh(summe + self.weights[-1])
        self.output = s
        return s
        #return round(math.tanh(x1+y1))
    
    def calcGradient1(self, x, y):
        """
        Error is calculated:
            Error_this = Error_final * weight * (1-output_this)
        """
        current = self.getOutput(x,y)
        gradient = 0
        for node in self.nextNodes:
            inputWeights = node.getWeights()
            #print(inputWeights)
            weight = float(inputWeights[self.index])
            gradientNextNode = node.getGradient()
            gradient += gradientNextNode*weight*(1-current*current)
        self.gradient = gradient
        #return gradient

    def calcGradient(self, inputs):
        """
        Error is calculated:
            Error_this = Error_final * weight * (1-output_this)
        """
        #current = self.getOutput(inputs)
        current = self.output
        c = (1-current*current)
        gradient = 0
        for node in self.nextNodes:
            inputWeights = node.getWeights()
            #print(inputWeights)
            weight = inputWeights[self.index]
            gradientNextNode = node.getGradient()
            gradient += gradientNextNode*weight*c
        self.gradient = gradient
        #return gradient
    
    def adjust1(self, x, y):
        global learning
        dWeight0 = learning*self.gradient*x
        dWeight1 = learning*self.gradient*y
        dWeightBias = learning*self.gradient
        
        self.weight0 += dWeight0
        self.weight1 += dWeight1
        self.biasWeight += dWeightBias

    def adjust(self, inputs):
        global learning
        l = float(learning)*self.gradient
        for i in range(len(inputs)):
            newWeight = l*inputs[i]
            self.weights[i] += newWeight
        self.weights[-1] += l
    
    def getWeights(self):
        return self.weights
    
    def getGradient(self):
        return self.gradient
    
    def getName(self):
        return self.name













class node_output():
    
    def __init__(self, name):
        #global initWeight1
        #global initWeight2
        #self.weight0 = initWeight1
        #self.weight1 = initWeight2
        #self.biasWeight = initBiasWeight
        self.name = name
        self.gradient = 0
        self.output = 0
    
    def initWeights(self, weights):
        self.weights = weights
    
    def getOutput(self, inputs):
        summe = 0
        for i in range(len(inputs)):
            summe += inputs[i]*self.weights[i]
        self.output = math.tanh(summe + self.weights[-1])
        return self.output
        #return round(math.tanh(x1+y1))
    
    def calcGradient1(self, x, y, expected):
        current = self.getOutput(x,y)
        self.gradient = (-2)*(current - expected)*(1-current*current)
        
    def calcGradient(self, inputs, expected):
        #current = self.getOutput(inputs)
        current = self.output
        self.gradient = (-2)*(current - expected)*(1-current*current)
    
    def adjust1(self, x, y):
        global learning
        dWeight0 = learning*self.gradient*x
        dWeight1 = learning*self.gradient*y
        dWeightBias = learning*self.gradient
        
        self.weight0 += dWeight0
        self.weight1 += dWeight1
        self.biasWeight += dWeightBias

    def adjust(self, inputs):
        global learning
        l = float(learning)*self.gradient
        for i in range(len(inputs)):
            newWeight = l*inputs[i]
            self.weights[i] += newWeight
        self.weights[-1] += l
    
    def getWeights(self):
        return self.weights
    
    def getGradient(self):
        return self.gradient
    
    def getName(self):
        return self.name



def normalize(inputList):
    absList1 = []
    absList2 = []
    for l in inputList:
        if l != "0,0,0":
            sp = l.split(',')
            #absList1.append(abs(float(sp[0])))
            #absList2.append(abs(float(sp[1])))
            absList1.append(float(sp[0]))
            absList2.append(float(sp[1]))
    maxElement1 = max(absList1)
    minElement1 = min(absList1)
    maxElement2 = max(absList2)
    minElement2 = min(absList2)
    difference1 = maxElement1-minElement1
    difference2 = maxElement2-minElement2
    output = []
    for l in inputList:
        if l != "0,0,0":
            sp = l.split(',')
            st = ""
            val1 = (float(sp[0])-minElement1)/(difference1) - 0.5
            val2 = (float(sp[1])-minElement2)/(difference2) - 0.5
            st += str(val1) + "," + str(val2)
            if len(sp) > 2:
                st += "," + sp[2]
            output.append(st)
        else:
            output.append(l)
    
    return output

def testRun():
    global learningOrig
    global decay
    print("Doing something")
    allLines = []
    expected = []
    with open("input_2.txt") as f:
        for l in f.readlines():
            allLines.append(l.rstrip())
            
    with open("output_2.txt") as f:
        for l in f.readlines():
            expected.append(int(l.rstrip()))
    
    happened = False
    training = []
    test = []
    
    allLines = normalize(allLines)
    
    for l in allLines:
        if l == "0,0,0":
            happened = True
        else:
            if not happened:
                training.append(l)
            else:
                test.append(l)
    
    
    #for l in training:
        #print("------------")
        #print(l)
        #sp = l.split(',')
        #print("x: " + str(float(sp[0])) + ", y: " + str(float(sp[1])) + ", expected: " + str(int(sp[2])))
    
    #print("size Training: " + str(len(training)))
    #print("size Test: " + str(len(test)))
    
    #with open("programm_output.txt", 'w') as outFile:
    with open("programm_output.txt", 'a') as outFile:
        print("Start")
        
        for a in range(1):
            
            node_2_0 = node_output("Node 2_0")
            
            node_1_0 = node_hidden(0, "Node 1_0", [node_2_0])
            node_1_1 = node_hidden(1, "Node 1_1", [node_2_0])
            node_1_2 = node_hidden(2, "Node 1_2", [node_2_0])
            node_1_3 = node_hidden(3, "Node 1_3", [node_2_0])
            
            layer1 = [node_1_0, node_1_1, node_1_2, node_1_3]
            
            node_0_0 = node_hidden(0, "Node 0_0", layer1)
            node_0_1 = node_hidden(1, "Node 0_1", layer1)
            node_0_2 = node_hidden(2, "Node 0_2", layer1)
            node_0_3 = node_hidden(3, "Node 0_3", layer1)
            
            layer0 = [node_0_0, node_0_1, node_0_2, node_0_3]
            
            for n in layer0:
                w = []
                for i in range(2):
                    w.append(float(random.randint(-30, 30)/100))
                outFile.write("Weights " + str(n.name) + ":\n")
                outFile.write(str(w) + "\n")
                n.initWeights(w)
            print("------------------")
            for n in layer1:
                w = []
                for i in range(4):
                    w.append(float(random.randint(-30, 30)/100))
                outFile.write("Weights " + str(n.name) + ":\n")
                outFile.write(str(w) + "\n")
                n.initWeights(w)
            
            w = []
            for i in range(4):
                w.append(float(random.randint(-30, 30)/100))
            outFile.write("Weights Layer 2:\n")
            outFile.write(str(w) + "\n")
            node_2_0.initWeights(w)
        
        for t in range(1):
            print("---------------")
            for runCounter in range(150):
                global learning
                global learningOrig
                global decay
                learning = learningOrig/(1+decay*runCounter)
                for x in training:
                    sp = x.split(',')
                    x = float(sp[0])
                    y = float(sp[1])
                    
                    ################################
                    
                    outL0 = []
                    
                    for n in layer0:
                        outL0.append(n.getOutput([x,y]))
                    
                    outL1 = []
                    
                    for n in layer1:
                        outL1.append(n.getOutput(outL0))
                    
                    outFinal = node_2_0.getOutput(outL1)
                    
                    ################################
                    
                    #node_2_0.calcGradient(out_10, out_11, int(sp[2]))
                    
                    #node_1_0.calcGradient(out_00, out_01)
                    #node_1_1.calcGradient(out_00, out_01)
                    
                    #node_0_0.calcGradient(x, y)
                    #node_0_1.calcGradient(x, y)
                    
                    node_2_0.calcGradient(outL1, int(sp[2]))
                    
                    for n in layer1:
                        n.calcGradient(outL0)
                    
                    for n in layer0:
                        n.calcGradient([x,y])
                    
                    ################################
                    
                    #node_2_0.adjust(out_10, out_11)
                    
                    #node_1_0.adjust(out_00, out_01)
                    #node_1_1.adjust(out_00, out_01)
                    
                    #node_0_0.adjust(x, y)
                    #node_0_1.adjust(x, y)
                    
                    node_2_0.adjust(outL1)
                    
                    for n in layer1:
                        n.adjust(outL0)
                    
                    for n in layer0:
                        n.adjust([x,y])
                    
                
            outputList = []
            for x in test:
                sp = x.split(',')
                x_Test = float(sp[0])
                y_Test = float(sp[1])
                
                outL0 = []
                for n in layer0:
                    outL0.append(n.getOutput([x_Test, y_Test]))
                
                outL1 = []
                
                for n in layer1:
                    outL1.append(n.getOutput(outL0))
                
                outL2 = node_2_0.getOutput(outL1)
                if outL2 < 0:
                    outL2 = -1
                else:
                    outL2 = 1
                outputList.append(outL2)
                #output_00 = node_0_0.getOutput(x_Test, y_Test)
                #output_01 = node_0_1.getOutput(x_Test, y_Test)
                #output_10 = node_1_0.getOutput(output_00, output_01)
                #output_11 = node_1_1.getOutput(output_00, output_01)
                #outputList.append(round(node_2_0.getOutput(output_10, output_11)))
                
            counter = 0
            for i in range(len(outputList)):
                if outputList[i] != expected[i]:
                    counter += 1
            #print("Run " + str(runCounter) + ", Fehler: " + str(counter))
            outFile.write("-------------------\n")
            outFile.write("Run " + str(t) + ", Counter: " + str(counter) + "\n")
            outFile.write("Learning rate: " + str(learning) + "\n")
            outFile.write("learningOrig: " + str(learningOrig) + "\n")
            outFile.write("decay: " + str(decay) + "\n")
            outFile.write("==============================================\n")
            
            # outFile.write("Final weights:\n\n")
            # outFile.write("Layer 0:\n")
            # for n in layer0:
            #     st = n.getName() + ": "
            #     for w in n.getWeights():
            #         st += str(w) + "|"
            #     st = st[:-1]
            #     st += "\n"
            #     outFile.write(st)
            
            # outFile.write("Layer 1:\n")
            # for n in layer1:
            #     st = n.getName() + ": "
            #     for w in n.getWeights():
            #         st += str(w) + "|"
            #     st = st[:-1]
            #     st += "\n"
            #     outFile.write(st)
            
            # outFile.write("Output Layer:\n")
            # st = "Node 2_0: "
            # for w in node_2_0.getWeights():
            #     st += str(w) + "|"
            # st = st[:-1]
            # st += "\n"
            # outFile.write(st)
            # #outFile.write("Node  2_0: " + str(node_2_0.getWeight0()) + "|" + str(node_2_0.getWeight1()) + "|" + str(node_2_0.getBiasWeight()) + "\n")
            outFile.write("==============================================\n")
        
        
    
    



def run():
    global learningOrig
    global decay
    with open("programm_output.txt", 'w') as outFile:
        outFile.write("")
    
    for d in range(20):
        decay = d/100
        for lO in range(1, 31):
            learningOrig = lO/100
            testRun()

if __name__== "__main__":
    #testRun()
    run()
    print("Fertig")

def asdf():
    allLines = []
    with open("input_2.txt") as f:
        for l in f.readlines():
            allLines.append(l.rstrip())
    
    norm = normalize(allLines)
    for x in range(len(allLines)):
        print(allLines[x] + "-------------" + norm[x])