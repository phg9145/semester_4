# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 14:07:06 2020

@author: Philipp
"""

import math
learning = 0.002

class node_hidden():
    
    def __init__(self, index, initWeight1, initWeight2, initBiasWeight):
        #global initWeight1
        #global initWeight2
        self.weight0 = initWeight1
        self.weight1 = initWeight2
        self.biasWeight = initBiasWeight
        self.index = index
    
    def getOutput(self, x, y):
        x1 = x*self.weight0
        y1 = y*self.weight1
        #exponent = 2*(x1+y1+self.biasWeight)
        #return 1-(2/(exp(exponent) + 1))
        return math.tanh(x1+y1 + self.biasWeight)
        #return round(math.tanh(x1+y1))
    
    def getGradient(self, x, y, errorOutputNode, outputNode):
        """
        Error is calculated:
            Error_this = Error_final * weight * (1-output_this)
        """
        current = self.getOutput(x,y)
        inputWeights = outputNode.getInputWeights()
        #print(inputWeights)
        weight = float(inputWeights[self.index])
        return errorOutputNode*weight*(1-current*current)
    
    def adjust(self, x, y, error):
        global learning
        dWeight0 = learning*error*x
        dWeight1 = learning*error*y
        dWeightBias = learning*error
        
        self.weight0 += dWeight0
        self.weight1 += dWeight1
        self.biasWeight += dWeightBias
    
    def getWeight0(self):
        return self.weight0
    
    def getWeight1(self):
        return self.weight1
    
    def getBiasWeight(self):
        return self.biasWeight
    
    def getInputWeights(self):
        l = []
        l.append(self.getWeight0())
        l.append(self.getWeight1())
        l.append(self.getBiasWeight())
        return l














class node_output():
    
    def __init__(self, initWeight1, initWeight2, initBiasWeight):
        #global initWeight1
        #global initWeight2
        self.weight0 = initWeight1
        self.weight1 = initWeight2
        self.biasWeight = initBiasWeight
    
    def getOutput(self, x, y):
        x1 = x*self.weight0
        y1 = y*self.weight1
        #exponent = 2*(x1+y1+self.biasWeight)
        #return 1-(2/(exp(exponent) + 1))
        return math.tanh(x1+y1 + self.biasWeight)
        #return round(math.tanh(x1+y1))
    
    def getGradient(self, x, y, expected):
        current = self.getOutput(x,y)
        return (-2)*(current - expected)*(1-current*current)
    
    def adjust(self, x, y, error):
        global learning
        dWeight0 = learning*error*x
        dWeight1 = learning*error*y
        dWeightBias = learning*error
        
        self.weight0 += dWeight0
        self.weight1 += dWeight1
        self.biasWeight += dWeightBias
        
    
    def getWeight0(self):
        return self.weight0
    
    def getWeight1(self):
        return self.weight1
    
    def getBiasWeight(self):
        return self.biasWeight
    
    def getInputWeights(self):
        l = []
        l.append(self.getWeight0())
        l.append(self.getWeight1())
        l.append(self.getBiasWeight())
        return l




def normalize(inputList):
    absList = []
    for l in inputList:
        if l != "0,0,0":
            sp = l.split(',')
            absList.append(abs(float(sp[0])))
            absList.append(abs(float(sp[1])))
    maxElement = max(absList)
    minElement = min(absList)
    difference = maxElement-minElement
    output = []
    print("maxElement: " + str(maxElement))
    print("minElement: " + str(minElement))
    for l in inputList:
        if l != "0,0,0":
            sp = l.split(',')
            st = ""
            val1 = (float(sp[0])-minElement)/(difference) - 0.5
            val2 = (float(sp[1])-minElement)/(difference) - 0.5
            st += str(val1) + "," + str(val2)
            if len(sp) > 2:
                st += "," + sp[2]
            output.append(st)
        else:
            output.append(l)
    
    return output

def testRun():
    print("Doing something")
    allLines = []
    expected = []
    with open("input_2.txt") as f:
        for l in f.readlines():
            allLines.append(l.rstrip())
            
    with open("output_2.txt") as f:
        for l in f.readlines():
            expected.append(int(l.rstrip()))
    
    happened = False
    training = []
    test = []
    
    allLines = normalize(allLines)
    
    
    for l in allLines:
        if l == "0,0,0":
            happened = True
        else:
            if not happened:
                training.append(l)
            else:
                test.append(l)
    
    
    #for l in training:
        #print("------------")
        #print(l)
        #sp = l.split(',')
        #print("x: " + str(float(sp[0])) + ", y: " + str(float(sp[1])) + ", expected: " + str(int(sp[2])))
    
    #print("size Training: " + str(len(training)))
    #print("size Test: " + str(len(test)))
    
    with open("programm_output.txt", 'w') as outFile:

        node_0_0 = node_hidden(0, -0.1, 0.12, -0.15)
        node_0_1 = node_hidden(1, 0.11, -0.15, 0.2)
        node_1_0 = node_output(-0.21, 0.1, -0.1)
        
        for runCounter in range(100000):
            for x in training:
                sp = x.split(',')
                x = float(sp[0])
                y = float(sp[1])
                
                out_00 = node_0_0.getOutput(x,y)
                out_01 = node_0_1.getOutput(x,y)
                
                errorOutput = node_1_0.getGradient(out_00, out_01, int(sp[2]))
                error_00 = node_0_0.getGradient(x, y, errorOutput, node_1_0)
                error_01 = node_0_1.getGradient(x, y, errorOutput, node_1_0)
                
                node_1_0.adjust(out_00, out_01, errorOutput)
                node_0_0.adjust(x, y, error_00)
                node_0_1.adjust(x, y, error_01)
            
            outputList = []
            for x in test:
                sp = x.split(',')
                output_00 = node_0_0.getOutput(float(sp[0]), float(sp[1]))
                output_01 = node_0_1.getOutput(float(sp[0]), float(sp[1]))
                outputList.append(round(node_1_0.getOutput(output_00, output_01)))
                
            counter = 0
            for i in range(len(outputList)):
                if outputList[i] != expected[i]:
                    counter += 1
            print("Run " + str(runCounter) + ", Fehler: " + str(counter))
            outFile.write("Run " + str(runCounter) + ", Fehler: " + str(counter) + "\n")
            
    
    
if __name__== "__main__":
    testRun()

def asdf():
    allLines = []
    with open("input_2.txt") as f:
        for l in f.readlines():
            allLines.append(l.rstrip())
    
    norm = normalize(allLines)
    for x in range(len(allLines)):
        print(allLines[x] + "-------------" + norm[x])