# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 14:07:06 2020

@author: Philipp
"""

import math
#import random

#initWeight1 = 0.1
#initWeight2 = 0.15
learning = 0.002

class node():
    
    def __init__(self, initWeight1, initWeight2, initWeight3):
        #global initWeight1
        #global initWeight2
        self.weight0 = initWeight1
        self.weight1 = initWeight2
        self.weight2 = initWeight3
    
    def getOutput(self, x, y):
        x1 = x*self.weight0
        y1 = y*self.weight1
        #exponent = 2*(x1+y1+self.weight2)
        #return 1-(2/(exp(exponent) + 1))
        return math.tanh(x1+y1 + self.weight2)
        #return round(math.tanh(x1+y1))
    
    def adjust(self, x, y, expected):
        global learning
        current = self.getOutput(x,y)
        newWeight0 = self.weight0 + learning*(-2)*(current-expected)*(1-current*current)*x
        newWeight1 = self.weight1 + learning*(-2)*(current-expected)*(1-current*current)*y
        newWeight2 = self.weight2 + learning*(-2)*(current-expected)*(1-current*current)
        
        self.weight0 = newWeight0
        self.weight1 = newWeight1
        self.weight2 = newWeight2
    
    def getWeight0(self):
        return self.weight0
    
    def getWeight1(self):
        return self.weight1
    
    def getWeight2(self):
        return self.weight2






def normalize(inputList):
    absList = []
    for l in inputList:
        if l != "0,0,0":
            sp = l.split(',')
            absList.append(abs(float(sp[0])))
            absList.append(abs(float(sp[1])))
    maxElement = max(absList)
    minElement = min(absList)
    difference = maxElement-minElement
    output = []
    print("maxElement: " + str(maxElement))
    print("minElement: " + str(minElement))
    for l in inputList:
        if l != "0,0,0":
            sp = l.split(',')
            st = ""
            val1 = (float(sp[0])-minElement)/(difference) - 0.5
            val2 = (float(sp[1])-minElement)/(difference) - 0.5
            st += str(val1) + "," + str(val2)
            if len(sp) > 2:
                st += "," + sp[2]
            output.append(st)
        else:
            output.append(l)
    
    return output

def testRun():
    print("Doing something")
    allLines = []
    expected = []
    with open("input_3.txt") as f:
        for l in f.readlines():
            allLines.append(l.rstrip())
            
    with open("output_3.txt") as f:
        for l in f.readlines():
            expected.append(int(l.rstrip()))
    
    happened = False
    training = []
    test = []
    
    allLines = normalize(allLines)
    
    
    for l in allLines:
        if l == "0,0,0":
            happened = True
        else:
            if not happened:
                training.append(l)
            else:
                test.append(l)
    
    
    #for l in training:
        #print("------------")
        #print(l)
        #sp = l.split(',')
        #print("x: " + str(float(sp[0])) + ", y: " + str(float(sp[1])) + ", expected: " + str(int(sp[2])))
    
    #print("size Training: " + str(len(training)))
    #print("size Test: " + str(len(test)))
    
    with open("programm_output.txt", 'w') as outFile:

        node1 = node(0.11, 0.12, -0.15)
        
        for runCounter in range(100000):
            for x in training:
                sp = x.split(',')
                node1.adjust(float(sp[0]), float(sp[1]), int(sp[2]))
            
            outputList = []
            for x in test:
                sp = x.split(',')
                outputList.append(round(node1.getOutput(float(sp[0]), float(sp[1]))))
                
            counter = 0
            for i in range(len(outputList)):
                if outputList[i] != expected[i]:
                    counter += 1
            print("Run " + str(runCounter) + ", Fehler: " + str(counter))
            outFile.write("Run " + str(runCounter) + ", Fehler: " + str(counter) + ", weight0: " + str(node1.getWeight0()) + ", weight1: " + str(node1.getWeight1()) + ", weight2: " + str(node1.getWeight2()) + "\n")
    
    
if __name__== "__main__":
    testRun()

def asdf():
    allLines = []
    with open("input_2.txt") as f:
        for l in f.readlines():
            allLines.append(l.rstrip())
    
    norm = normalize(allLines)
    for x in range(len(allLines)):
        print(allLines[x] + "-------------" + norm[x])