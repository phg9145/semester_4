# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 14:07:06 2020

@author: Philipp
"""

import math
#import random

#initWeight1 = 0.1
#initWeight2 = 0.15
learning = 0.01

class node():
    
    def __init__(self, initWeight1, initWeight2, initWeight3):
        #global initWeight1
        #global initWeight2
        self.weight0 = initWeight1
        self.weight1 = initWeight2
        self.weight2 = initWeight3
    
    def getOutput(self, x, y):
        x1 = x*self.weight0
        y1 = y*self.weight1
        #exponent = 2*(x1+y1+self.weight2)
        #return 1-(2/(exp(exponent) + 1))
        return math.tanh(x1+y1 + self.weight2)
        #return round(math.tanh(x1+y1))
    
    def adjust(self, x, y, expected):
        global learning
        current = self.getOutput(x,y)
        newWeight0 = self.weight0 + learning*(-2)*(current-expected)*(1-current*current)*x
        newWeight1 = self.weight1 + learning*(-2)*(current-expected)*(1-current*current)*y
        newWeight2 = self.weight2 + learning*(-2)*(current-expected)*(1-current*current)
        
        self.weight0 = newWeight0
        self.weight1 = newWeight1
        self.weight2 = newWeight2
    
    def getWeight0(self):
        return self.weight0
    
    def getWeight1(self):
        return self.weight1
    
    def getWeight2(self):
        return self.weight2






def normalize(inputList):
    absList1 = []
    absList2 = []
    for l in inputList:
        if l != "0,0,0":
            sp = l.split(',')
            #absList1.append(abs(float(sp[0])))
            #absList2.append(abs(float(sp[1])))
            absList1.append(float(sp[0]))
            absList2.append(float(sp[1]))
    maxElement1 = max(absList1)
    minElement1 = min(absList1)
    maxElement2 = max(absList2)
    minElement2 = min(absList2)
    difference1 = maxElement1-minElement1
    difference2 = maxElement2-minElement2
    output = []
    #print("maxElement1: " + str(maxElement1))
    #print("minElement: " + str(minElement))
    for l in inputList:
        if l != "0,0,0":
            sp = l.split(',')
            st = ""
            val1 = (float(sp[0])-minElement1)/(difference1) - 0.5
            val2 = (float(sp[1])-minElement2)/(difference2) - 0.5
            st += str(val1) + "," + str(val2)
            if len(sp) > 2:
                st += "," + sp[2]
            output.append(st)
        else:
            output.append(l)
    
    return output

def testRun1():
    allLines = []
    while True:
        try:
            allLines.append(input())
        except EOFError:
            break

def testRun():
    print("Doing something")
    allLines = []
    expected = []
    with open("input_1.txt") as f:
        for l in f.readlines():
            allLines.append(l.rstrip())
            
    with open("output_1.txt") as f:
        for l in f.readlines():
            expected.append(int(l.rstrip()))
    
    happened = False
    training = []
    test = []
    
    allLines = normalize(allLines)
    
    
    for l in allLines:
        if l == "0,0,0":
            happened = True
        else:
            if not happened:
                training.append(l)
            else:
                test.append(l)

    node1 = node(0.11, 0.12, -0.15)
    with open("programm_output.txt", 'w') as outFile:
        outFile.write("")
    for t in range(5):
        for runCounter in range(1000):
            for x in training:
                sp = x.split(',')
                node1.adjust(float(sp[0]), float(sp[1]), int(sp[2]))
        
        outputList = []
        counter = 0
        for x in test:
            sp = x.split(',')
            ergebnis = round(node1.getOutput(float(sp[0]), float(sp[1])))
            if ergebnis == 1:
                outputList.append(1)
            else:
                outputList.append(-1)
        
        with open("programm_output.txt", 'a') as outFile:
            for i in range(len(outputList)):
                if outputList[i] != expected[i]:
                    counter += 1
            #print("Run " + str(runCounter) + ", Fehler: " + str(counter))
            outFile.write("-------------------\n")
            outFile.write("Run " + str(t+1) + ", Counter: " + str(counter) + "\n")
            outFile.write("-------------------\n")
        
            print("Run " + str(t) + " durch")
    for x in test:
        sp = x.split(',')
        ergebnis = round(node1.getOutput(float(sp[0]), float(sp[1])))
        if ergebnis == 1:
            print("+1")
        else:
            print("-1")
                
    
    
if __name__== "__main__":
    testRun()