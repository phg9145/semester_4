# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 14:07:06 2020

@author: Philipp
"""

import math
#import random

#initWeight1 = 0.1
#initWeight2 = 0.15
learning = 0.1

class node():
    
    def __init__(self, initWeight1, initWeight2, initWeight3):
        #global initWeight1
        #global initWeight2
        self.weight0 = initWeight1
        self.weight1 = initWeight2
        self.weight2 = initWeight3
    
    def getOutput(self, x, y):
        x1 = x*self.weight0
        y1 = y*self.weight1
        #exponent = 2*(x1+y1+self.weight2)
        #return 1-(2/(exp(exponent) + 1))
        return math.tanh(x1+y1)
        #return round(math.tanh(x1+y1))
    
    def adjust(self, x, y, expected):
        global learning
        current = self.getOutput(x,y)
        newWeight0 = self.weight0 + (-2)*learning*x*(current-expected)
        newWeight1 = self.weight1 + (-2)*learning*y*(current-expected)
        #newWeight2 = self.weight2 + (-2)*learning*(current-expected)
        
        self.weight0 = newWeight0
        self.weight1 = newWeight1
        #self.weight2 = newWeight2
    
    def getWeight0(self):
        return self.weight0
    
    def getWeight1(self):
        return self.weight1
        

def testRun():
    print("Doing something")
    allLines = []
    expected = []
    with open("input_1.txt") as f:
        for l in f.readlines():
            allLines.append(l.rstrip())
            
    with open("output_1.txt") as f:
        for l in f.readlines():
            expected.append(int(l.rstrip()))
    
    happened = False
    training = []
    test = []
    
    for l in allLines:
        if l == "0,0,0":
            happened = True
        else:
            if not happened:
                training.append(l)
            else:
                test.append(l)
    
    #for l in training:
        #print("------------")
        #print(l)
        #sp = l.split(',')
        #print("x: " + str(float(sp[0])) + ", y: " + str(float(sp[1])) + ", expected: " + str(int(sp[2])))
    
    #print("size Training: " + str(len(training)))
    #print("size Test: " + str(len(test)))
    factor = 1000
    factorLR = 100
    with open("programm_output.txt", 'w') as outFile:
        for lR in range(30,50):
            global learning
            learning = lR/factorLR
            #learningRate = 0.001
            
            for w1 in range(1,11):
                for w2 in range(1,11):
                    for w3 in range(1,11):
                        node1 = node(w1/factor, w2/factor, w3/factor)
                        
                        for x in training:
                            sp = x.split(',')
                            node1.adjust(float(sp[0]), float(sp[1]), int(sp[2]))
                        
                        outputList = []
                        for x in test:
                            sp = x.split(',')
                            outputList.append(round(node1.getOutput(float(sp[0]), float(sp[1]))))
                            
                            #print(str(round(node1.getOutput(float(sp[0]), float(sp[1])))))
                            #print(str(node1.getOutput(float(sp[0]), float(sp[1]))))
                        counter = 0
                        for i in range(len(outputList)):
                            if outputList[i] != expected[i]:
                                counter += 1
                        print("lR: " + str(learning) + ", w1: " + str(w1/factor) + ", w2: " + str(w2/factor) + ", w3: " + str(w3/factor) + ", Fehler: " + str(counter))
                        outFile.write("lR: " + str(learning) + ", w1: " + str(w1/factor) + ", w2: " + str(w2/factor) + ", w3: " + str(w3/factor) + ", Fehler: " + str(counter) + "\n")
    
    
if __name__== "__main__":
    testRun()