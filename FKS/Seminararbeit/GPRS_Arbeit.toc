\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Einleitung}{4}{section.1}%
\contentsline {subsection}{\numberline {1.1}Hintergrund}{4}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Vorteile eines paketorientierten Dienstes}{4}{subsection.1.2}%
\contentsline {subsubsection}{\numberline {1.2.1}Vorteile für Nutzer}{4}{subsubsection.1.2.1}%
\contentsline {subsubsection}{\numberline {1.2.2}Vorteile für Internetanbieter}{4}{subsubsection.1.2.2}%
\contentsline {section}{\numberline {2}GSM Grundarchitektur}{5}{section.2}%
\contentsline {section}{\numberline {3}GPRS Systemarchitektur}{6}{section.3}%
\contentsline {subsection}{\numberline {3.1}Die \aclp {GSN}}{6}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}\acf {SGSN}}{7}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}\acf {GGSN}}{7}{subsubsection.3.1.2}%
\contentsline {subsection}{\numberline {3.2}Interfaces}{7}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}\acs {PLMN}-Backbones}{8}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Zusätzliche Hardware}{8}{subsection.3.4}%
\contentsline {section}{\numberline {4}GPRS-Qualität und Services}{8}{section.4}%
\contentsline {subsection}{\numberline {4.1}Klassifizierung}{8}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Fähigkeits-Klassen}{9}{subsubsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.2}Multislot-Klassen}{9}{subsubsection.4.1.2}%
\contentsline {subsection}{\numberline {4.2}Services}{9}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}\acl {QoS}}{10}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Service-Priorität}{10}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Verlässlichkeit}{10}{subsubsection.4.3.2}%
\contentsline {subsubsection}{\numberline {4.3.3}Verzögerung}{10}{subsubsection.4.3.3}%
\contentsline {subsubsection}{\numberline {4.3.4}Durchsatz}{11}{subsubsection.4.3.4}%
\contentsline {section}{\numberline {5}Protokolle}{12}{section.5}%
\contentsline {subsection}{\numberline {5.1}Verbindungsaufbau und -abbruch}{12}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Session Management und \ac {PDP}-Kontext}{12}{subsection.5.2}%
\contentsline {subsubsection}{\numberline {5.2.1}Statische Zuweisung}{12}{subsubsection.5.2.1}%
\contentsline {subsubsection}{\numberline {5.2.2}Dynamische Zuweisung}{12}{subsubsection.5.2.2}%
\contentsline {subsubsection}{\numberline {5.2.3}Kommunikation bei einer Zuweisung}{12}{subsubsection.5.2.3}%
\contentsline {subsection}{\numberline {5.3}Positionsverwaltung}{13}{subsection.5.3}%
\contentsline {subsubsection}{\numberline {5.3.1}Intra-\ac {SGSN} Routing Area Update}{14}{subsubsection.5.3.1}%
\contentsline {subsubsection}{\numberline {5.3.2}Inter-\ac {SGSN} Routing Area Update}{15}{subsubsection.5.3.2}%
\contentsline {subsection}{\numberline {5.4}Routing}{16}{subsection.5.4}%
\contentsline {section}{\numberline {6}Zusammenfassung}{17}{section.6}%
