\babel@toc {german}{}
\contentsline {section}{\numberline {1}2D-Grafik und Rasterisierung}{5}%
\contentsline {subsection}{\numberline {1.1}Koordinatensysteme}{5}%
\contentsline {subsubsection}{\numberline {1.1.1}Darstellung eines Punkts}{5}%
\contentsline {subsubsection}{\numberline {1.1.2}Rasterisierung von Punkten}{5}%
\contentsline {subsubsection}{\numberline {1.1.3}Transformation LKOS $\rightarrow $ GKOS}{6}%
\contentsline {subsubsection}{\numberline {1.1.4}Rundung}{6}%
\contentsline {subsection}{\numberline {1.2}Liniendarstellung}{6}%
\contentsline {subsubsection}{\numberline {1.2.1}Mathematische Geradengleichung}{6}%
\contentsline {subsubsection}{\numberline {1.2.2}Bresenham-Algorithmus}{7}%
\contentsline {subsubsection}{\numberline {1.2.3}Anmerkungen}{9}%
\contentsline {subsection}{\numberline {1.3}(Anti-)Aliasing}{9}%
\contentsline {subsection}{\numberline {1.4}Polygonrasterung und -füllung}{11}%
\contentsline {subsubsection}{\numberline {1.4.1}Polygonrasterung}{11}%
\contentsline {subsubsection}{\numberline {1.4.2}Polygonfüllung}{12}%
\contentsline {subsection}{\numberline {1.5}Clipping}{13}%
\contentsline {subsubsection}{\numberline {1.5.1}Linien-Clipping an Rechteck mit Cohen-Sutherland}{13}%
\contentsline {subsubsection}{\numberline {1.5.2}Clipping an Polygonen nach Liang-Barsky}{15}%
\contentsline {subsection}{\numberline {1.6}Affine Transformationen}{16}%
\contentsline {subsection}{\numberline {1.7}Angewandte rechnergestützte Geometrie}{19}%
\contentsline {section}{\numberline {2}Graustufen- und Farbdarstellung}{21}%
\contentsline {subsection}{\numberline {2.1}Graustufendarstellung}{21}%
\contentsline {subsection}{\numberline {2.2}Farbwahrnehmung}{22}%
\contentsline {subsubsection}{\numberline {2.2.1}CIE-Kegel}{22}%
\contentsline {subsubsection}{\numberline {2.2.2}CIE-Diagramm}{22}%
\contentsline {subsubsection}{\numberline {2.2.3}Metamerie}{23}%
\contentsline {subsection}{\numberline {2.3}Farbmodelle}{23}%
\contentsline {subsubsection}{\numberline {2.3.1}RGB-Modell}{23}%
\contentsline {subsubsection}{\numberline {2.3.2}RGB-Farbspezifikation}{23}%
\contentsline {subsubsection}{\numberline {2.3.3}CMY-Modell}{24}%
\contentsline {subsubsection}{\numberline {2.3.4}Zusammenhang RGB $\leftrightarrow $ CMY}{24}%
\contentsline {subsubsection}{\numberline {2.3.5}Farbmodell}{24}%
\contentsline {subsubsection}{\numberline {2.3.6}HSV-Modell}{24}%
\contentsline {subsubsection}{\numberline {2.3.7}Andere Modelle}{25}%
\contentsline {subsection}{\numberline {2.4}Berechnungen mit RGB-Werten, Speicherung}{25}%
\contentsline {subsubsection}{\numberline {2.4.1}Rechnen mit RGB-Farbwerten}{25}%
\contentsline {subsubsection}{\numberline {2.4.2}Clamping/Lineare Abbildung}{25}%
\contentsline {subsubsection}{\numberline {2.4.3}Speicherung der RGB-Farbinformation}{25}%
\contentsline {section}{\numberline {3}Kurven}{26}%
\contentsline {subsection}{\numberline {3.1}Bézier-Kurven}{28}%
\contentsline {subsection}{\numberline {3.2}B-Splines}{33}%
\contentsline {subsection}{\numberline {3.3}Non Uniform Rational B-Splines (NURBS)}{36}%
\contentsline {section}{\numberline {4}3D-Modellierung}{37}%
\contentsline {subsection}{\numberline {4.1}Szenengraph}{37}%
\contentsline {subsection}{\numberline {4.2}Modellierungsarten}{39}%
\contentsline {subsection}{\numberline {4.3}Volumenmodellierung}{39}%
\contentsline {subsubsection}{\numberline {4.3.1}Normzellen}{39}%
\contentsline {subsubsection}{\numberline {4.3.2}Octrees (dt. Oktalbäume)}{40}%
\contentsline {subsubsection}{\numberline {4.3.3}Constructive Solid Geometry (CSG)}{41}%
\contentsline {subsection}{\numberline {4.4}Kanten- und Oberflächenmodellierung}{42}%
\contentsline {subsubsection}{\numberline {4.4.1}Drahtmodell}{43}%
\contentsline {subsubsection}{\numberline {4.4.2}Oberflächendarstellung (BRep)}{43}%
\contentsline {subsection}{\numberline {4.5}Hybridschemata}{44}%
\contentsline {subsection}{\numberline {4.6}Sweep-Körper / Extrusion}{44}%
\contentsline {section}{\numberline {5}Oberflächendarstellung}{45}%
\contentsline {subsection}{\numberline {5.1}Topologische Struktur}{45}%
\contentsline {subsubsection}{\numberline {5.1.1}Flächen-Knoten-Speicherung}{45}%
\contentsline {subsubsection}{\numberline {5.1.2}Kantenspeicherung}{46}%
\contentsline {subsubsection}{\numberline {5.1.3}Winged-Edge-Repräsentation}{47}%
\contentsline {subsubsection}{\numberline {5.1.4}Half-Edge}{48}%
\contentsline {subsection}{\numberline {5.2}Euler-Operationen}{48}%
\contentsline {subsubsection}{\numberline {5.2.1}Polyedersatz}{48}%
\contentsline {subsubsection}{\numberline {5.2.2}Euler-Operationen}{48}%
\contentsline {subsection}{\numberline {5.3}Flächen}{49}%
\contentsline {subsubsection}{\numberline {5.3.1}Polygone}{49}%
\contentsline {subsubsection}{\numberline {5.3.2}Bilineare Interpolation}{49}%
\contentsline {subsubsection}{\numberline {5.3.3}Coon's Patch}{49}%
\contentsline {subsubsection}{\numberline {5.3.4}Freiformflächen}{50}%
\contentsline {section}{\numberline {6}Transformationen und Projektionen}{51}%
\contentsline {subsection}{\numberline {6.1}Koordinatensysteme}{51}%
\contentsline {subsection}{\numberline {6.2}Affine Transformationen}{52}%
\contentsline {subsubsection}{\numberline {6.2.1}Einfache affine Transformation}{52}%
\contentsline {subsubsection}{\numberline {6.2.2}Rotation um eine Achse im Raum}{55}%
\contentsline {subsection}{\numberline {6.3}Alternative Darstellungen von Rotationen}{56}%
\contentsline {subsubsection}{\numberline {6.3.1}Quaternionen}{56}%
\contentsline {subsubsection}{\numberline {6.3.2}Rotationsmatrizen}{57}%
\contentsline {subsection}{\numberline {6.4}Perspektivische Abbildung}{57}%
\contentsline {subsection}{\numberline {6.5}Projektive Abbildungen}{58}%
\contentsline {section}{\numberline {7}3D zu 2D}{59}%
\contentsline {subsection}{\numberline {7.1}Ansichtstransformation}{60}%
\contentsline {subsubsection}{\numberline {7.1.1}Sichtkoordinatensystem}{60}%
\contentsline {subsubsection}{\numberline {7.1.2}Überführung Weltkoordinaten $\rightarrow $ Sichtkoordinaten}{61}%
\contentsline {subsection}{\numberline {7.2}Projektionstransformation}{61}%
\contentsline {subsubsection}{\numberline {7.2.1}Orthogonalprojektion}{62}%
\contentsline {subsubsection}{\numberline {7.2.2}Perspektivische Projektion}{64}%
\contentsline {subsection}{\numberline {7.3}Clipping / Normalisierung}{66}%
\contentsline {subsection}{\numberline {7.4}Bildschirmtransformation}{67}%
\contentsline {section}{\numberline {8}Sichtbarkeitsentscheid}{68}%
\contentsline {subsection}{\numberline {8.1}3D-Clipping}{68}%
\contentsline {subsection}{\numberline {8.2}Rückseitenentfernung}{68}%
\contentsline {subsubsection}{\numberline {8.2.1}Variante 1}{68}%
\contentsline {subsubsection}{\numberline {8.2.2}Variante 2}{69}%
\contentsline {subsection}{\numberline {8.3}Tiefenpuffer-Algorithmus}{69}%
\contentsline {subsection}{\numberline {8.4}List-Priority-Verfahren}{71}%
\contentsline {subsection}{\numberline {8.5}Raycasting}{71}%
\contentsline {subsubsection}{\numberline {8.5.1}Grundlagen}{71}%
\contentsline {subsubsection}{\numberline {8.5.2}Beschleunigung}{72}%
\contentsline {section}{\numberline {9}Beleuchtung und Schattierung}{72}%
\contentsline {subsection}{\numberline {9.1}Lokale Beleuchtung}{72}%
\contentsline {subsubsection}{\numberline {9.1.1}Ambiente Reflexion}{73}%
\contentsline {subsubsection}{\numberline {9.1.2}Punktförmige Lichtquellen mit diffuser Reflexion}{73}%
\contentsline {subsubsection}{\numberline {9.1.3}Spekulare Reflexion}{74}%
\contentsline {subsubsection}{\numberline {9.1.4}Kombination der drei Beleuchtungsarten}{75}%
\contentsline {subsubsection}{\numberline {9.1.5}Entfernungsabhängigkeit und Nebel}{75}%
\contentsline {subsection}{\numberline {9.2}Schattierung}{76}%
\contentsline {subsubsection}{\numberline {9.2.1}Konstante Schattierung (flat shading)}{76}%
\contentsline {subsubsection}{\numberline {9.2.2}Interpolierte Schattierung}{77}%
\contentsline {subsection}{\numberline {9.3}Transparenz}{78}%
\contentsline {subsubsection}{\numberline {9.3.1}Transparenz ohne Brechung des Lichts}{78}%
\contentsline {subsubsection}{\numberline {9.3.2}Transparenz mit Brechung des Lichts}{79}%
\contentsline {section}{\numberline {10}Globale Beleuchtungsmodelle}{80}%
\contentsline {subsection}{\numberline {10.1}Raytracing}{80}%
\contentsline {subsection}{\numberline {10.2}Andere strahlenbasierte Verfahren}{82}%
\contentsline {subsubsection}{\numberline {10.2.1}Photon Mapping}{82}%
\contentsline {subsubsection}{\numberline {10.2.2}Ambient Occlusion}{82}%
\contentsline {subsubsection}{\numberline {10.2.3}Image Based Lighting}{82}%
\contentsline {subsection}{\numberline {10.3}Radiosity-Verfahren}{83}%
\contentsline {subsubsection}{\numberline {10.3.1}Berechnung für Strahlungswerte}{83}%
\contentsline {subsubsection}{\numberline {10.3.2}Berechnung der Formfaktoren}{83}%
\contentsline {subsubsection}{\numberline {10.3.3}Darstellung des Bildes}{84}%
\contentsline {subsubsection}{\numberline {10.3.4}Vergleich Raytracing / Radiosity}{84}%
