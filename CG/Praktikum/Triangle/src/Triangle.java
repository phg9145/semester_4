import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Triangle extends JPanel {

	/** Dimension des Zeichen-Panels */
	private int width = 500;
	private int height = 500;

	/**
	 * Einfache Klasse, die einen 2D-Punkt spezifiziert.
	 */
	class Point {
		public int x;
		public int y;

		Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

	/** Eine Liste mit Point2D */
	List<Point> points = new LinkedList<Point>();

	/** wird nach dem dritten Punkt auf false gesetzt */
	boolean setTriangle = true;
	/** wird nach dem vierten Punkt auf false gesetzt */
	boolean setPoints = true;

	/** Konstruktor des Panels */
	public Triangle() {
		setPreferredSize(new Dimension(width, height));

		/** Koordinaten eines Mausklicks abfragen */
		this.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				if (setPoints) {
					int x = evt.getX();
					int y = evt.getY();
					points.add(new Point(x, y));
				}
				// Nach drei Punkten ist das Dreieck fertig
				if (points.size() == 3)
					setTriangle = false;
				// Nach vier Punkten werden keine Mausklickkoordinaten mehr
				// ermittelt.
				if (points.size() == 4)
					setPoints = false;
			}
		});
		
	}


	/**
	 * Inhalt des Zeichen-Panels.
	 */
	public void paintComponent(Graphics graphics) {
		super.paintComponent(graphics);
		this.setBackground(Color.BLACK);

		Graphics2D g2d = (Graphics2D) graphics;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setStroke(new BasicStroke(2));
		g2d.setColor(Color.WHITE);

		// TODO: Hier Ihr Code...
		// Bitte mit Methoden arbeiten!
		
		for(Point p : this.points) {
		    graphics.drawRect(p.x-2, p.y-2, 5, 5);
		}
		
		if(!this.setTriangle) {
		    Point a = this.points.get(0);
            Point b = this.points.get(1);
            Point c = this.points.get(2);
		    graphics.drawLine(a.x, a.y, b.x, b.y);
            graphics.drawLine(b.x, b.y, c.x, c.y);
            graphics.drawLine(c.x, c.y, a.x, a.y);
            
            int ca_x = a.x-c.x;
            int ca_y = a.y-c.y;
            
            int cb_x = b.x-c.x;
            int cb_y = b.y-c.y;
            
            double area = 0.5*(Math.abs(ca_x*cb_y - cb_x*ca_y));
            graphics.drawString("Area: " + Double.toString(area), 100, 100);
            
            if(!this.setPoints) {
                Point p = this.points.get(3);

                double pa_x = a.x-p.x;
                double pa_y = a.y-p.y;
                double pa_abs = Math.sqrt(pa_x*pa_x + pa_y*pa_y);
//                System.out.println("pa_x: " + pa_x);
//                System.out.println("pa_y: " + pa_y);
//                System.out.println("pa_abs: " + pa_abs);

                double pb_x = b.x-p.x;
                double pb_y = b.y-p.y;
                double pb_abs = Math.sqrt(pb_x*pb_x + pb_y*pb_y);
//                System.out.println("pb_x: " + pb_x);
//                System.out.println("pb_y: " + pb_y);
//                System.out.println("pb_abs: " + pb_abs);

                double pc_x = c.x-p.x;
                double pc_y = c.y-p.y;
                double pc_abs = Math.sqrt(pc_x*pc_x + pc_y*pc_y);
//                System.out.println("pc_x: " + pc_x);
//                System.out.println("pc_y: " + pc_y);
//                System.out.println("pc_abs: " + pc_abs);
                
                double w_apb = Math.toDegrees(Math.acos((pa_x*pb_x + pa_y*pb_y)/(pa_abs*pb_abs)));
                double w_bpc = Math.toDegrees(Math.acos((pb_x*pc_x + pb_y*pc_y)/(pb_abs*pc_abs)));
                double w_cpa = Math.toDegrees(Math.acos((pc_x*pa_x + pc_y*pa_y)/(pc_abs*pa_abs)));
                
                graphics.drawString("Winkel aPb: " + Double.toString(w_apb), 100, 120);
                graphics.drawString("Winkel bPc: " + Double.toString(w_bpc), 100, 140);
                graphics.drawString("Winkel cPa: " + Double.toString(w_cpa), 100, 160);
                
                double gesamt = w_apb + w_bpc + w_cpa;
                String st = "Winkelsumme " + gesamt + " --> Punkt innerhalb des Dreiecks? ";
                if(Math.abs(gesamt) == 360.0) {
                    st += "Ja";
                }
                else {
                    st += "Nein";
                }
                graphics.drawString(st, 100, 180);
            }
		}
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("Dreiecksoperationen");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Triangle triang = new Triangle();
		frame.add(triang);
		frame.pack();
		frame.setVisible(true);
		while (true) {
			// Neuzeichnen anstoßen
			frame.repaint();
			try {
				Thread.sleep(10L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
