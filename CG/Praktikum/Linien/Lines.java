import java.awt.Graphics;

public class Lines {
	private Graphics graphics;

	/**
	 * Verschiedene Methoden zum Zeichnen von Linien
	 * 
	 * @param graphics Grafik-Kontext, in den gezeichnet wird
	 */
	public Lines(Graphics graphics) {
		this.graphics = graphics;
	}

	/**
	 * Methode zum Zeichnen eines Punktes.
	 * 
	 * HACK: Zeichne Punkt als Linie der Länge 0. Es gibt in Java2D keine Methode
	 * zum Zeichnen eines Einzelpunkts!
	 * 
	 * @param x x-Koordinate
	 * @param y y-Koordinate
	 */
	void setPixel(int x, int y) {
		graphics.drawLine(x, y, x, y);
	}

	/**
	 * Konventionelle Linien-Berechnung über y = m*x + b
	 * 
	 * @param x0 x-Koordinate Startpunkt
	 * @param y0 y-Koordinate Startpunkt
	 * @param x1 x-Koordinate Endpunkt
	 * @param y1 y-Koordinate Endpunkt
	 */
	void drawLineEquation(int x0, int y0, int x1, int y1) {
		// TODO Hier Code einfuegen ...
	    double m = (y1-y0)/((double)(x1)-x0);
	    //System.out.println("m: " + m);
	    double b = y0 - m*x0;
	    double y = y0;
	    for(int x = x0; x <= x1; x++) {
	        y = m*x + b;
            this.setPixel(x, (int)(y+0.5));
	    }
	}

	// Shift geeignet bis Fensterhöhe 8192
	private final static int SHIFT = 18;
	private final static int GAMMA = (1 << (SHIFT - 1));

	/**
	 * Linien-Berechnung über Digital Differential Analyzer (DDA)
	 * 
	 * @param x0 x-Koordinate Startpunkt
	 * @param y0 y-Koordinate Startpunkt
	 * @param x1 x-Koordinate Endpunkt
	 * @param y1 y-Koordinate Endpunkt
	 */
	void drawDda(int x0, int y0, int x1, int y1) {
		// TODO Hier Code einfuegen ...
	    int m = (int) ( ((y1-y0) << SHIFT)/(x1-x0) );
	    int y = (y0 << SHIFT) + GAMMA;
        //this.setPixel(x0, y >> SHIFT);
	    for(int x = x0; x <= x1; x++) {
	        this.setPixel(x, y >> SHIFT);
	        y += m;
	    }
	}

	/**
	 * Linien-Berechnung über Bresenham
	 * 
	 * @param x0 x-Koordinate Startpunkt
	 * @param y0 y-Koordinate Startpunkt
	 * @param x1 x-Koordinate Endpunkt
	 * @param y1 y-Koordinate Endpunkt
	 */
	void drawBresenham(int x0, int y0, int x1, int y1) {
		// TODO Hier Code einfuegen ...
	    int dx = x1 - x0;
	    int dy = y1 - y0;
	    int D = 2*dy - dx;
	    int dE = 2*dy;
	    int dNE = 2*(dy-dx);
	    int x = x0;
	    int y = y0;
	    
	    while(x <= x1) {
	        this.setPixel(x, y);
	        if(D < 0) {
	            D += dE;
	            x++;
	        }
	        else {
	            D += dNE;
	            x++;
	            y++;
	        }
	    }
	}
}
