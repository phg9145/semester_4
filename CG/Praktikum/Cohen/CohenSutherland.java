import java.awt.Graphics;


/**
 * Clipping nach Cohen-Sutherland.
 */
public class CohenSutherland {
	/** Zum Zeichnen */
	private Graphics graphics;

	/** Dimension des Clipping-Rechtecks */
	private int xmin;
	private int xmax;
	private int ymin;
	private int ymax;
	
	

	/**
	 * Ctor.
	 * 
	 * @param graphics Zum Zeichnen
	 * @param xmin     minimale x-Koordinate
	 * @param ymin     minimale y-Koordinate
	 * @param xmax     maximale x-Koordinate
	 * @param ymax     maximale y-Koordinate
	 */
	public CohenSutherland(Graphics graphics, int xmin, int ymin, int xmax, int ymax) {
		super();
		this.graphics = graphics;
		this.xmin = xmin;
		this.ymin = ymin;
		this.xmax = xmax;
		this.ymax = ymax;
	}

	/**
	 * Berechne den Cohen-Sutherland-Outputcode für einen Punkt.
	 * 
	 * @formatter:off
	 * viertletztes Bit = 1 <=> y > ymax 
	 * drittletztes Bit = 1 <=> y < ymin
	 * vorletztes Bit = 1 <=> x > xmax 
	 * letztes Bit = 1 <=> x < xmin
	 * @formatter:on
	 * 
	 * Die 4 Bits werden sehr verschwenderisch in einem int untergebracht.
	 * 
	 * Warum kein byte? Die bitweisen Operationen sind für Datentyp byte nicht
	 * definiert! Genauer werden z.B. die Bytes bei byte1 | byte2 zu ints gecastet
	 * und das Ergebnis ist ein int.
	 * Mehr Details: <a href="https://stackoverflow.com/questions/27582233/why-byte-and-short-values-are-promoted-to-int-when-an-expression-is-evaluated">Stack Overflow</a>
	 * 
	 * @param x x-Koordinate Punkt
	 * @param y y-Koordinate Punkt
	 * @return Outputcode
	 */
	int outputCode(int x, int y) {

		// TODO: Ihr Code hier ...
	    int c = 0;
	    if(y > this.ymax) {
	        //st += "10";
			c += Area.GTYMAX;
	    }
	    else if(y < this.ymin) {
	        c += Area.LTYMIN;
	    }
	    if(x > this.xmax) {
            c += Area.GTXMAX;
        }
        else if(x < this.xmin) {
            c += Area.LTXMIN;
        }
	    return c;
	}

	/**
	 * Clipping nach Cohen-Sutherland. Die Linie von (xA,yA) nach (xE,yE) wird an
	 * dem durch die Attribute (xmin,ymin) und (xmax,ymax) definierten Rechteck
	 * geclippt und der sichtbare Teil der Linie gezeichnet.
	 * 
	 * @param xA x-Koordinate Anfangspunkt Linie
	 * @param yA y-Koordinate Anfangspunkt Linie
	 * @param xE x-Koordinate Endpunkt Linie
	 * @param yE y-Koordinate Endpunkt Linie
	 */
	void clipLine(int xA, int yA, int xE, int yE) {

		// TODO: Ihr Code hier ...
	    int kA = this.outputCode(xA,  yA);
        int kE = this.outputCode(xE,  yE);
        //System.out.println("kA: " + kA);
        //System.out.println("kE: " + kE);
        //System.out.println("kA|kE: " + (kA|kE));
        
        if((kA | kE) == 0) {
            // Linie vollständig sichtbar
            this.graphics.drawLine(xA,  yA,  xE,  yE);
        }
        else if((kA & kE) != 0) {
            // Linie unsichtbar, nicht muss gemacht werden
        }
        else {
            int combined = (kA | kE);
            int xSchnitt = 0;
            int ySchnitt = 0;
            
            boolean yC = false;
            boolean anfangAussen = false;
            
            // b1 überprüfen
            if(combined >= Area.GTYMAX) {
                ySchnitt = this.ymax;
                yC = true;
                if(kA >= Area.GTYMAX) {
                    anfangAussen = true;
                }
            }
            // b2 überprüfen
            else if(combined >= Area.LTYMIN) {
                ySchnitt = this.ymin;
                yC = true;
                if(kA >= Area.LTYMIN) {
                    anfangAussen = true;
                }
            }
            // b3 überprüfen
            else if(combined >= Area.GTXMAX) {
                xSchnitt = this.xmax;
                if(kA >= Area.GTXMAX) {
                    anfangAussen = true;
                }
            }
            // b4 überprüfen
            else if(combined >= Area.LTXMIN) {
                xSchnitt = this.xmin;
                if(kA >= Area.LTXMIN) {
                    anfangAussen = true;
                }
            }
            
            
            if(yC) {
                xSchnitt = (int)((xE-xA)/((double)yE-yA))*(ySchnitt-yE) + xE;
            }
            
            else {
                ySchnitt = (int)((yE-yA)/((double)xE-xA))*(xSchnitt-xE) + yE;
            }

            if(anfangAussen) {
                this.clipLine(xSchnitt, ySchnitt, xE, yE);
            }
            else {
                this.clipLine(xA, yA, xSchnitt, ySchnitt);
            }
            
        }
        
	}
}
