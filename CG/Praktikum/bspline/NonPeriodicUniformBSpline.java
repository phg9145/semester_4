import java.util.List;

public class NonPeriodicUniformBSpline extends BSpline{

    
    public NonPeriodicUniformBSpline(List<Point> points, int k, double h) {
        super(points, k, h);
        int totalSize = points.size() + k;
        this.knotVector = new double[totalSize];
        
        System.out.println("k: " + k);
        System.out.println("h: " + h);
        
        for(int i = 0; i < k; i++) {
            this.knotVector[i] = 0;
        }
        for(int i = 1; i < totalSize-2*k+1; i++) {
            this.knotVector[k+i-1] = i;
        }
        int last = totalSize-2*k+1;
        for(int i = totalSize-k; i < totalSize; i++) {
            this.knotVector[i] = last;
        }
        
        String kvString = "";
        for(int i = 0; i < totalSize; i++) {
            kvString += this.knotVector[i] + "|";
        }
        System.out.println(kvString);
    }
    
    
}
