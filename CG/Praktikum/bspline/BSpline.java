import java.awt.Graphics;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

abstract class BSpline {
	
	/** Ordnung der Spline-Kurve (= Grad + 1) */
	protected int k;

	/** Knotenvektor */
	protected double[] knotVector;

	/** Auflösung der Kurve, Schrittweite, Zeichengenauigkeit */
	private double h;
	
	/**
	 * Eine Liste von Point-Objekten Dies sind die Stütz- und Kontrollpunkte
	 */
	protected List<Point> points;

	

	/**
	 * B-Spline-Kurve 
	 * 
	 * @param points Kontrollpunkte
	 * @param k Grad der B-Spline + 1
	 * @param h Schrittweite beim Zeichnen der B-Spline
	 */
	BSpline(List<Point> points, int k, double h) {
		// TODO: Hier Ihr Code...
	    this.k = k;
	    this.points = points;
	    this.h = h;
	}

	/**
	 * Zeichne eine B-Spline mit Stütz- und Kontrollpunkten aus points.
	 * 
	 * @param graphics Grafikobjekt
	 */
	void render(Graphics graphics) {
		// TODO: Ihr Code hier
	    List<Point> allPoints = new LinkedList<Point>();
	    for(double i = this.knotVector[this.k-1]; i < this.knotVector[this.points.size()]; i += this.h) {
	        allPoints.add(this.bSpline(i));
	    }
	    
	    for(int i = 0; i < allPoints.size()-1; i++) {
	        Point p1 = allPoints.get(i);
	        Point p2 = allPoints.get(i+1);
	        graphics.drawLine((int)(p1.x+0.5), (int)(p1.y+0.5), (int)(p2.x+0.5), (int)(p2.y+0.5));
	    }
	    Point last = allPoints.get(allPoints.size()-1);
	    Point lastDraw = this.bSpline(this.knotVector[this.points.size()]);
	    graphics.drawLine((int)(last.x+0.5), (int)(last.y+0.5), (int)(lastDraw.x+0.5), (int)(lastDraw.y+0.5));
	    
	}

	/**
	 * Berechne ein Punkt-Objekt, das die zweidimensionale Koordinate der
	 * BSpline-Kurve für einen gegebenen Parameterwert errechnet.
	 * 
	 * @param t Kurvenparameter
	 * @return 2D-Koordinate der B-Spline-Kurve für Parameter t
	 */
    Point bSpline(double t) {
        // TODO: Ihr Code hier
        Point p = new Point(0,0);
        int n = this.points.size() + k;
        for(int i = 0; i < n-k; i++) {
            double nik = 0;
            if(t >= this.knotVector[i] && t <= this.knotVector[i+this.k]) {
                nik = this.nik(i, this.k, t);
            }
            p = new Point(1, p, nik, this.points.get(i));
        }
        
        return p;
    }

	/**
	 * Berechne den Wert der B-Spline-Basisfunktion N_{i,k}(t) analog zu Casteljau.
	 * 
	 * @param i Index der B-Spline-Basisfunktion N_{i,k}
	 * @param k Grad der B-Spline-Basisfunktion N_{i,k}
	 * @param t Parameter, an dem N_{i,k} ausgewertet wird
	 * @return N_{i,k}(t)
	 */
	double nik(int i_input, int k, double t) {
		// TODO: Ihr Code hier
//	    System.out.println("################################");
//	    System.out.println("i_input: " + i_input);
//        System.out.println("k: " + k);
//        System.out.println("t: " + t);
//        System.out.println();
	    
	    double kV[] = this.knotVector;
	    
	    double nik_values[][] = new double[k][k];
//        System.out.println("Leer:");
//        for(int printCount = k-1; printCount >= 0; printCount--) {
//            System.out.println(Arrays.toString(nik_values[printCount]));
//        }
//        System.out.println();
	    
	    // Initialisieren
        for(int ic = 0; ic < k; ic++) {
            int val = 0;
            int i = i_input + ic;
            if(kV[i] <= t && t < kV[i+1]) {
                val = 1;
            }
            nik_values[ic][0] = val;
        }
        
//        System.out.println("After Init:");
//        for(int printCount = k-1; printCount >= 0; printCount--) {
//            System.out.println(Arrays.toString(nik_values[printCount]));
//        }
//        System.out.println();
        
	    
	    
	    for(int k_counter = 2; k_counter <= k; k_counter++) {
	        
	        for(int i_counter = 0; i_counter <= (k-k_counter); i_counter++) {
	            int i = i_input + i_counter;
	            
                double summand_1;
                double nenner_1 = kV[i+k_counter-1] - kV[i];
                if(nenner_1 == 0) {
                    summand_1 = 0;
                }
                else {
                    summand_1 = (t - kV[i])/nenner_1;
                    summand_1 *= nik_values[i_counter][k_counter-2];
                }
                
                
                double summand_2;
                double nenner_2 = kV[i+k_counter] - kV[i+1];
                if(nenner_2 == 0) {
                    summand_2 = 0;
                }
                else {
                    summand_2 = (kV[i+k_counter] - t)/nenner_2;
                    summand_2 *= nik_values[i_counter+1][k_counter-2];
                }
	            
	            nik_values[i_counter][k_counter-1] = summand_1 + summand_2;
	        }
	        for(int rest = (k-k_counter)+1; rest < k; rest++) {
	            nik_values[rest][k_counter-1] = 9.9;
	        }
	        
	    }
//        System.out.println("Full:");
//        for(int printCount = k-1; printCount >= 0; printCount--) {
//            System.out.println(Arrays.toString(nik_values[printCount]));
//        }
//	    
//
//        System.out.println("################################");
	    
	    return nik_values[0][k-1];
	}
}
