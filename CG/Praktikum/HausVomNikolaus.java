import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class HausVomNikolaus extends JPanel {

	/** Dimension des Zeichen-Panels */
	private int width = 500;
	private int height = 500;
	/**
	 * Affine Transformation, die auf das grafische Objekt angewandt werden soll
	 */
	private AffineTransform affineTransform = new AffineTransform();

	/** Das Haus vom Nikolaus */
	private GeneralPath hausVomNikolaus;

	/** Winkelinkrement pro step-Aufruf */
	private double winkel = 0.001;

	public HausVomNikolaus() {
		this.setPreferredSize(new Dimension(width, height));

		hausVomNikolaus = createHausVomNikolaus(width / 2, height / 2);

		// TODO: Definieren Sie hier affineTransform!
	}

	/**
	 * Generiert ein Haus vom Nikolaus
	 * 
	 * @param xcenter
	 *            x-Koordinate Mittelpunkt des HvN
	 * @param ycenter
	 *            y-Koordinate Mittelpunkt des HvN
	 * @return Haus vom Nikolaus-Pfad
	 */
	private GeneralPath createHausVomNikolaus(int xcenter, int ycenter) {
		GeneralPath generalPath = new GeneralPath();
		generalPath.moveTo(xcenter - 100, ycenter + 100);
		generalPath.lineTo(xcenter - 100, ycenter - 100);
		generalPath.lineTo(xcenter, ycenter - 225);
		generalPath.lineTo(xcenter + 100, ycenter - 100);
		generalPath.lineTo(xcenter + 100, ycenter + 100);
		generalPath.lineTo(xcenter - 100, ycenter - 100);
		generalPath.lineTo(xcenter + 100, ycenter - 100);
		generalPath.lineTo(xcenter - 100, ycenter + 100);
		generalPath.lineTo(xcenter + 100, ycenter + 100);
		generalPath.closePath();
		return generalPath;
	}

	/**
	 * Inhalt des Zeichen-Panels.
	 */
	public void paintComponent(Graphics graphics) {
		super.paintComponent(graphics);
		this.setBackground(Color.BLACK);

		Graphics2D graphics2d = (Graphics2D) graphics;
		// Anti-Aliasing
		graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		graphics2d.setStroke(new BasicStroke(2));
		graphics.setColor(Color.RED);
		// hier wird das Haus gezeichnet
		graphics2d.draw(hausVomNikolaus);
	}

	/**
	 *  Animations-Routine
	 */
	private void step() {
		// Anwendung der affinen Transformation auf das Haus
		hausVomNikolaus.transform(affineTransform);
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("Haus vom Nikolaus");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		HausVomNikolaus haus = new HausVomNikolaus();
		frame.add(haus);
		frame.pack();
		frame.setVisible(true);
		while (true) {
			// Neuzeichnen anstossen
			frame.repaint();
			haus.step();
			try {
				Thread.sleep(1L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
