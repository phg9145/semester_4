import java.awt.Graphics;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

/**
 * Rastert Polygone mit Scanline-Verfahren.
 * 
 * Zur Vereinfachung nehmen wir an, dass sich Polygone immer komplett im
 * Viewport befinden, den Rand also nicht schneiden.
 */
public class PolygonRasterer {
	/** Zum Zeichnen */
	private Graphics graphics;
	/** Höhe des Zeichenfensters */
	private int height;
	/** Die Edge Table */
	private LinkedList<Edge> edgeTable = new LinkedList<>();
	/** Die Active Edge Table */
	private LinkedList<Edge> activeEdgeTable = new LinkedList<>();

	public PolygonRasterer(int height) {
		this.height = height;
	}

	/**
	 * Umsetzung des Scan-Line-Verfahrens
	 */
	public void rasterize() {
		// TODO: Ihr Code hier...
	    for(int y = 0; y < this.height; y++) {
	        for(Edge e : this.edgeTable) {
	            if(e.getyMin() == y) {
	                activeEdgeTable.add(new Edge(e));
	            }
	        }

	        Collections.sort(this.activeEdgeTable, new Comparator<Edge>(){
	           @Override
	           public int compare(Edge e1, Edge e2) {
	               return (int) (e1.getxIntersect() - e2.getxIntersect());
	           }
	        });

	        for(int i = 0; i < this.activeEdgeTable.size()/2; i++) {
	            int x_1 = (int) (this.activeEdgeTable.get(2*i).getxIntersect()+0.5);
	            int x_2 = (int) (this.activeEdgeTable.get(2*i+1).getxIntersect()+0.5);
	            this.graphics.drawLine(x_1, y, x_2, y);
	        }
	        
	        
	        List<Edge> removeList = new ArrayList<>();
	        for(Edge e : this.activeEdgeTable) {
	            if(e.getyMax() == y) {
	                removeList.add(e);
	            }
	            else {
	                double x_schnitt = e.getxIntersect();
	                x_schnitt += e.getmReci();
	                e.setxIntersect(x_schnitt);
	            }
	        }
            this.activeEdgeTable.removeAll(removeList);
	        
	    }
	}

	public void setGraphics(Graphics graphics) {
		this.graphics = graphics;
	}

	public void addEdges(LinkedList<Edge> edges) {
		edgeTable.addAll(edges);
	}
}
