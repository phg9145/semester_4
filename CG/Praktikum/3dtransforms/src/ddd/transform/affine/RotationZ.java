package ddd.transform.affine;

import ddd.transform.Transform;

/**
 * Winkel-phi-Rotation um z-Achse
 */
public class RotationZ extends Transform {
	public RotationZ(double phi) {
		this(Math.sin(phi), Math.cos(phi));
	}
	
	/**
	 * Rotation um z-Achse
	 * @param sinphi Sinus des Rotationswinkels
	 * @param cosphi Kosinus des Rotationswinkels
	 */
	public RotationZ(double sinphi, double cosphi) {
		super();
		// TODO: Ihr Code hier...
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                double v = 0;
                if(i == j) {
                    v = 1.0;
                }
                this.matrix[i][j] = v; 
            }
        }
        this.matrix[0][0] = cosphi;
        this.matrix[0][1] = (-1)*sinphi;
        this.matrix[1][0] = sinphi;
        this.matrix[1][1] = cosphi;
	}
}
