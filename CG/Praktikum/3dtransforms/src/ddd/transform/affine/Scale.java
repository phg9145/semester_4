package ddd.transform.affine;

import ddd.transform.Transform;

/**
 * Skalierung in Achsrichtungen
 */
public class Scale extends Transform {
	/**
	 * Skalierung
	 * @param sx x-Richtung
	 * @param sy y-Richtung
	 * @param sz z-Richtung
	 */
	public Scale(double sx, double sy, double sz) {
		super();
		// TODO: Ihr Code hier...
		for(int i = 0; i < 4; i++) {
		    for(int j = 0; j < 4; j++) {
		        this.matrix[i][j] = 0;
		    }
		}
		this.matrix[0][0] = sx;
        this.matrix[1][1] = sy;
        this.matrix[2][2] = sz;
        this.matrix[3][3] = 1;
	}

	/**
	 * Zentrische Streckung
	 * @param scale Skalierungsfaktor
	 */
	public Scale(double scale) {
		this(scale, scale, scale);
	}
}
