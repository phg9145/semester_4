package ddd.transform.affine;

import ddd.transform.Transform;

/**
 * Allgemeine Rotation
 */
public class GeneralRotation extends Transform {
	/**
	 * Allgemeine Rotationsmatrix mit Fusspunkt, Achse und Winkel.
	 * Es wird nicht davon ausgegangen, dass die Achse normiert ist.
	 * 
	 * @param px
	 *            x-Koordinate Fusspunkt der Rotationsachse
	 * @param py
	 *            y-Koordinate Fusspunkt der Rotationsachse
	 * @param pz
	 *            z-Koordinate Fusspunkt der Rotationsachse
	 * @param rx
	 *            x-Koordinate Rotationsachse
	 * @param ry
	 *            y-Koordinate Rotationsachse
	 * @param rz
	 *            z-Koordinate Rotationsachse
	 * @param phi
	 *            Rotationswinkel im Bogenmaß
	 */
	public GeneralRotation(double px, double py, double pz, double rx,
			double ry, double rz, double phi) {
		super();

		// TODO: Ihr Code hier...
		
		// Achse normieren
		double vector_norm = 1/(Math.sqrt(rx*rx + ry*ry + rz*rz));
		rx *= vector_norm;
        ry *= vector_norm;
        rz *= vector_norm;
        
        
		double h = Math.sqrt(rx*rx + rz*rz);
        
        // Schritt 1
        this.rightMult(new Translation(px, py, pz));
        
        
        // Schritt 2
        if(h != 0) {
            this.rightMult(new RotationY(rx/h, rz/h));
        }
		
        
        // Schritt 3
        this.rightMult(new RotationX(-ry, h));
        
                
        // Schritt 4
        this.rightMult(new RotationZ(phi));
        
        
        // Schritt 5, Inverse zu Schritt 3
        this.rightMult(new RotationX(ry, h));
        
        
        // Schritt 6, Inverse zu Schritt 2
        if(h != 0) {
            this.rightMult(new RotationY(-rx/h, rz/h));
        }
        
        
        // Schritt 7, Inverse zu Schritt 1
        this.rightMult(new Translation(-px, -py, -pz));
	}
}
