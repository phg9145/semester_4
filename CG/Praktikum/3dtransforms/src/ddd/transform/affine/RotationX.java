package ddd.transform.affine;

import ddd.transform.Transform;

/**
 * Winkel-phi-Rotation um x-Achse
 */
public class RotationX extends Transform {
	/**
	 * Rotation um x-Achse
	 * @param phi Rotationswinkel im Bogenmaß
	 */
	public RotationX(double phi) {
		this(Math.sin(phi), Math.cos(phi));
	}
	
	/**
	 * Rotation um x-Achse
	 * @param sinphi Sinus des Rotationswinkels
	 * @param cosphi Kosinus des Rotationswinkels
	 */
	public RotationX(double sinphi, double cosphi) {
		super();
		// TODO: Ihr Code hier...
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                double v = 0;
                if(i == j) {
                    v = 1.0;
                }
                this.matrix[i][j] = v; 
            }
        }
        
        this.matrix[1][1] = cosphi;
        this.matrix[1][2] = (-1)*sinphi;
        this.matrix[2][1] = sinphi;
        this.matrix[2][2] = cosphi;
	}
}
