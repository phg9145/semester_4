import java.awt.Graphics;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

class Bezier {
    
    
	// TODO: Definieren Sie benötigte Attribute hier.
    List<Point> points;
    double h;
    
    
	/**
	 * Berechnet Beziér-Kurven. Der Grad der Beziér-Kurve ist über die Zahl der
	 * Kontrollpunkte festgelegt.
	 * 
	 * @param points Kontrollpunkte.
	 * @param h      Schrittweite beim Zeichnen der Beziér-Kurve
	 */
	Bezier(List<Point> points, double h) {
		// TODO: Hier Ihr Code...
	    this.points = points;
	    this.h = h;
	}
	
	
	
	
	
	

	/**
	 * Berechne ein Punkt-Objekt, das die zweidimensionale Koordinate der
	 * Bézier-Kurve für einen gegebenen Parameterwert errechnet.
	 * 
	 * @param t Kurvenparameter
	 * @return Koordinate der Bézier-Kurve
	 */
	Point casteljau(double t) {
		// TODO: Hier Ihr Code...
	    int n = this.points.size();
	    
	    // Point[a][b] bei P _a ^b 
	    Point[][] casteljau_points = new Point[n][n];
	    
	    for(int a = 0; a < n; a++) {
	        casteljau_points[a][0] = this.points.get(a);
	    }
	    
	    for(int b = 1; b < n; b++) {
	        for(int a = b; a < n; a++) {
	            casteljau_points[a][b] = new Point((1-t), casteljau_points[a-1][b-1], t, casteljau_points[a][b-1]);
	        }
	    }
	    
	    return casteljau_points[n-1][n-1];
	}
	
	
	
	
	
	

	/**
	 * Zeichne eine Bezier-Kurve mit Stütz- und Kontrollpunkten aus points.
	 * 
	 * @param graphics Grafikobjekt
	 */
	void render(Graphics graphics) {
		// TODO: Hier Ihr Code...
	    
	    ArrayList<Point> all_points = new ArrayList<>();
	    
	    Point lastPoint = this.points.get(this.points.size() - 1);
	    
	    // Alle Casteljau-Wegpunkte berechnen (für t = 0 wird der Startpunkt berechnet)
	    int counter = 0;
	    double t = 0;
	    while((t = this.h*counter) <= 1.0) {
	        all_points.add(this.casteljau(t));
	        counter++;
	    }
	    
	    // Alle Wege bis auf den letzten berechnen
	    for(int i = 0; i < all_points.size()-1; i++) {
	        Point p1 = all_points.get(i);
            Point p2 = all_points.get(i+1);
	        graphics.drawLine((int)(p1.x+0.5), (int)(p1.y+0.5), (int)(p2.x+0.5), (int)(p2.y+0.5));
	    }
	    
	    // Letztes Wegstück malen
	    Point lastPointInList = all_points.get(all_points.size() - 1);
	    graphics.drawLine((int)(lastPointInList.x+0.5), (int)(lastPointInList.y+0.5), (int)(lastPoint.x+0.5), (int)(lastPoint.y+0.5));
	    
	    
	}
}
