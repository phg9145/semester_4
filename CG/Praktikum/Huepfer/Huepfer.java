import java.awt.Color;
import java.awt.Graphics;


class Huepfer {

	/** Zum Zeichnen in Panel */
	Graphics graphics;

	/** Breite und Höhe Zeichen-Panel */
	int width, height;

	/** Minimal-/Maximalkoordinaten des logischen Koordinatensystems (LKOS) */
	double xMin, xMax, yMin, yMax;

	/** Hüpfer-Parameter */
	double a, b, c;

	/** Anzahl Punkte */
	int num;

	public Huepfer(Graphics graphics,
			int width, int height,
			double xMin, double xMax, double yMin, double yMax,
			double a, double b, double c, 
			int num) {
		super();
		this.graphics = graphics;
		this.width = width;
		this.height = height;
		this.xMin = xMin;
		this.xMax = xMax;
		this.yMin = yMin;
		this.yMax = yMax;
		this.a = a;
		this.b = b;
		this.c = c;
		this.num = num;
	}

	/**
	 * Methode zum Zeichnen eines Punktes. Die (0,0)-Koordinate wird nach links
	 * unten gelegt.
	 * 
	 * HACK: Zeichne Punkt als Linie der Länge 0. Es gibt in Java keine Methode zum
	 * Zeichne eines Einzelpunkts!
	 * 
	 * @param graphics Grafik-Kontext
	 * @param x x-Koordinate
	 * @param y y-Koordinate
	 */
	void setPixel(int x, int y) {
		graphics.drawLine(x, y, x, y);
	}

	/**
	 * Wandle LKOS-Koordinate in GKOS-Koordinate um.
	 * 
	 * @param x LKOS-Koordinate
	 * @return GKOS-Koordinate
	 */
	int transformX(double x) {
		// TODO Hier Code einfuegen ...
	    double xG = this.width/(this.xMax - this.xMin);
	    xG *= (x - this.xMin);
	    return (int)(xG+0.5);
	}

	/**
	 * Wandle LKOS-Koordinate in GKOS-Koordinate um.
	 * 
	 * @param y LKOS-Koordinate
	 * @return GKOS-Koordinate
	 */
	int transformY(double y) {
		// TODO Hier Code einfuegen ...
        double yG = this.height/(this.yMin - this.yMax);
        yG *= (y - this.xMax);
        return (int)(yG+0.5);
	}

	public void render() {
		// TODO Ihre Implementierung des Hüpfer-Algorithmus ...
	    double x = 0;
	    double y = 0;
	    double xx = 0;
	    double yy = 0;
	    for(int i = 0; i < num; i++) {
	        this.setPixel(this.transformX(x), this.transformY(y));
	        xx = y-Math.signum(x)*Math.sqrt(Math.abs(this.b*x-this.c));
	        yy = this.a - x;
	        x = xx;
	        y = yy;
	        
	        if(i%100 == 0) {
	            graphics.setColor(new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255)));
	        }
	    }
	}
}
