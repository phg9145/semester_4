package ddd.transform.vp;

import ddd.transform.Transform;

/**
 * Perspektivische Transformation.
 * 
 */
public class PerspectiveTransform extends Transform {
	/**
	 * Perspektivische Transformation, wie sie von OpenGL verwendet wird
	 * (glFrustum).
	 * 
	 * @param left   Linker Rand des Sichtvolumens
	 * @param right  Rechter Rand des Sichtvolumens
	 * @param bottom Unterer Rand des Sichtvolumens
	 * @param top    Oberer Rand des Sichtvolumens
	 * @param near   Abstand Front Plane von der Kamera
	 * @param far    Abstand Back Plane von der Kamera
	 */
	public PerspectiveTransform(double left, double right, double bottom, double top, double near, double far) {
		super();

		// TODO: Ihr Code hier...
		
		this.matrix[0][0] = (2*near) / (right-left);
        this.matrix[0][2] = (right+left) / (right-left);

        this.matrix[1][1] = (2*near) / (top-bottom);
        this.matrix[1][2] = (top+bottom) / (top-bottom);

        this.matrix[2][2] = (far+near) / (near-far);
        this.matrix[2][3] = (2*far*near) / (near-far);

        this.matrix[3][2] = -1.0;
        this.matrix[3][3] = 0;

	}

	private static double fH(double fovY, double zNear) {
		return Math.tan(fovY / 360.0 * Math.PI) * zNear;
	}

	/**
	 * Perspektivische Transformation, wie sie von OpenGL verwendet wird
	 * (gluPerspective), mit gebräuchlicheren Parametern im monoskopischen Fall.
	 * 
	 * @param fovY   Vertikaler Öffnungswinkel in Grad (Field of View).
	 * @param aspect Verhältnis horizontale zu vertikale Bildschirmauflösung
	 * @param zNear  Abstand Front Plane von der Kamera
	 * @param zFar   Abstand Back Plane von der Kamera
	 */
	public PerspectiveTransform(double fovY, double aspect, double zNear, double zFar) {
		// Wäre natürlich schön, das fH(fovY, zNear) nur einmal zu berechnen.
		// Kann aber vor dem Konstruktoraufruf nicht durchgeführt werden.
		// "Constructor call must be the first statement in a constructor."
		this(-fH(fovY, zNear) * aspect, fH(fovY, zNear) * aspect, -fH(fovY, zNear), fH(fovY, zNear), zNear, zFar);
	}
}
