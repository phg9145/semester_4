package ddd.transform.vp;

import ddd.transform.Transform;

/**
 * Bildschirmtransformation
 */
public class DisplayTransform extends Transform {
	/**
	 * Bildschirmkoordinaten gehen von LO = (0,0) bis RU = (xmax, ymax)
	 * 
	 * @param xmax maximale x-Koordinate
	 * @param ymax maximale y-Koordinate
	 */
	public DisplayTransform(int xmax, int ymax) {
		super();
		this.matrix[2][2] = 0;
		
        this.matrix[0][0] = 0.5*xmax;
        this.matrix[0][3] = 0.5*xmax;
        this.matrix[1][1] = -0.5*ymax;
        this.matrix[1][3] = 0.5*ymax;

		// TODO: Ihr Code hier...

	}
}
