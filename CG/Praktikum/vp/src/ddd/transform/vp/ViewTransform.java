package ddd.transform.vp;

import ddd.transform.Transform;
import ddd.transform.affine.Translation;
import ddd.Vector3;

public class ViewTransform extends Transform {
	/**
	 * Sichttransformation
	 * @param cameraPos	Position der Kamera in Weltkoordinaten
	 * @param lookAtPoint Punkt, auf den die Kamera ausgerichtet ist
	 * @param upVector Obenvektor
	 */
	public ViewTransform(Vector3 cameraPos, Vector3 lookAtPoint, Vector3 upVector) {
		super();
		
		Vector3 A = cameraPos;
		Vector3 P = lookAtPoint;
		Vector3 o = upVector;
		
		// cameraPos = Augpunkt A
		// lookAtPoint = Referenzpunkt P
		// upVector = Obenvektor o
		
		// TODO: Ihr Code hier...
		
		Vector3 n_vec = Vector3.sub(A, P);
		double n = n_vec.norm();
		
		n_vec.normalize();
		
		// u, v, w initialisieren
		
		Vector3 w = n_vec;
		
		Vector3 u = Vector3.outerProduct(o, w);
		u.normalize();
		
		Vector3 v = Vector3.outerProduct(w, u);
		
		// Matrix initialisieren
		
		this.matrix[0][0] = u.x;
        this.matrix[0][1] = u.y;
        this.matrix[0][2] = u.z;
        
        this.matrix[1][0] = v.x;
        this.matrix[1][1] = v.y;
        this.matrix[1][2] = v.z;
        
        this.matrix[2][0] = w.x;
        this.matrix[2][1] = w.y;
        this.matrix[2][2] = w.z;
        
        // Augpunkt verschieben
        
        Translation T_AN = new Translation(-A.x, -A.y, -A.z);
        this.rightMult(T_AN);
		
				
	}
}
