package ddd.transform.affine;

import ddd.transform.Transform;

/**
 * Winkel-phi-Rotation um y-Achse
 */
public class RotationY extends Transform {
	public RotationY(double phi) {
		this(Math.sin(phi), Math.cos(phi));
	}
	
	/**
	 * Rotation um y-Achse
	 * @param sinphi Sinus des Rotationswinkels
	 * @param cosphi Kosinus des Rotationswinkels
	 */
	public RotationY(double sinphi, double cosphi) {
		super();
		// TODO: Ihr Code hier...
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                double v = 0;
                if(i == j) {
                    v = 1.0;
                }
                this.matrix[i][j] = v; 
            }
        }
        this.matrix[0][0] = cosphi;
        this.matrix[0][2] = sinphi;
        this.matrix[2][0] = (-1)*sinphi;
        this.matrix[2][2] = cosphi;
	}
}
