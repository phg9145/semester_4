package ddd.transform.affine;

import ddd.Vector3;
import ddd.transform.Transform;

/**
 * Translation um (dx, dy, dz)
 */
public class Translation extends Transform {
	/**
	 * Translation
	 * @param trans Translationsvektor
	 */
	public Translation(Vector3 trans) {
		this(trans.x, trans.y, trans.z);
	}
	
	/**
	 * Translation
	 * @param dx x-Richtung
	 * @param dy y-Richtung
	 * @param dz z-Richtung
	 */
	public Translation(double dx, double dy, double dz) {
		super();
		// TODO: Ihr Code hier ...
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                double v = 0;
                if(i == j) {
                    v = 1.0;
                }
                this.matrix[i][j] = v; 
            }
        }
        this.matrix[0][3] = dx;
        this.matrix[1][3] = dy;
        this.matrix[2][3] = dz;
	}
}
