package ddd.render;

import ddd.Line;
import ddd.Primitive;
import ddd.Triangle;
import ddd.Vector3;
import ddd.scenegraph.Node;
import ddd.transform.Transform;

/**
 * Primitiver Wireframe-Renderer mit einfacher paralleler Orthogonalprojektion.
 */
abstract class SimpleRenderer extends Renderer {

	protected Transform renderTransform;

	/**
	 * Nur für JUnit-Tests benötigt
	 * @return Render-Transform
	 */
	public Transform getRenderTransform() {
		return renderTransform;
	}

	@Override
	public void render(Node scene) {
		for (Primitive primitive : scene.getPrimitives()) {
			if (primitive instanceof Triangle) {
				// Von Triangles werden nur die Kanten gezeichnet
				Triangle triangle = (Triangle) primitive;

				// Transformiere Eckpunkte
				Vector3 v0t = renderTransform.multiply(triangle.vertices[0]);
				Vector3 v1t = renderTransform.multiply(triangle.vertices[1]);
				Vector3 v2t = renderTransform.multiply(triangle.vertices[2]);

				// Keine Appearance gesetzt => Wireframe-Zeichnung
				graphics.drawLine((int) (v0t.x + 0.5), (int) (v0t.y + 0.5), (int) (v1t.x + 0.5), (int) (v1t.y + 0.5));
				graphics.drawLine((int) (v1t.x + 0.5), (int) (v1t.y + 0.5), (int) (v2t.x + 0.5), (int) (v2t.y + 0.5));
				graphics.drawLine((int) (v2t.x + 0.5), (int) (v2t.y + 0.5), (int) (v0t.x + 0.5), (int) (v0t.y + 0.5));
			}
			else if (primitive instanceof Line) {
				Line line = (Line) primitive;
				// Transformiere Start- und Endpunkt
				Vector3 v0t = renderTransform.multiply(line.vertices[0]);
				Vector3 v1t = renderTransform.multiply(line.vertices[1]);
				graphics.drawLine((int) (v0t.x + 0.5), (int) (v0t.y + 0.5), (int) (v1t.x + 0.5), (int) (v1t.y + 0.5));
			}
		}
	}
}
