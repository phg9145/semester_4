package ddd.render;

import ddd.Vector3;
import ddd.transform.Transform;
import ddd.transform.vp.DisplayTransform;
import ddd.transform.vp.PerspectiveTransform;
import ddd.transform.vp.ViewTransform;

public class SimplePerspectiveRenderer extends SimpleRenderer {
	/**
	 * Einstellungen für ein einfaches perspektivisches Rendering.
	 * @param position Position Kamera
	 * @param lookAtPoint Punkt, auf den Kamera zeigt
	 * @param upVector Obenvektor
	 * @param fovY Öffnungswinkel in y-Richtung
	 * @param zNear Abstand Front Plane
	 * @param zFar abstand Back Plane
	 * @param width Fensterbreite
	 * @param height Fensterhöhe
	 */
	public SimplePerspectiveRenderer(Vector3 position, Vector3 lookAtPoint, Vector3 upVector, double fovY, double zNear,
			double zFar, int width, int height) {
		super();
		renderTransform = new Transform();

		// TODO: Ihr Code hier...
		
		// DisplayTransform
		Transform display = new DisplayTransform(width, height);
		
		renderTransform.rightMult(display);
		
		
		// PerspectiveTransform
        Transform perspective = new PerspectiveTransform(fovY, ((double)width/height), zNear, zFar);
        
        renderTransform.rightMult(perspective);
		
		
		// ViewTransform
        Transform view = new ViewTransform(position, lookAtPoint, upVector);
        
        renderTransform.rightMult(view);

	}
}
