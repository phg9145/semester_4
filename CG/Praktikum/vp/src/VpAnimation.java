import ddd.Vector3;
import ddd.models.simple.House;
import ddd.models.simple.Plane;
import ddd.models.simple.Tower;
import ddd.render.SimplePerspectiveRenderer;
import ddd.scenegraph.Activity;
import ddd.scenegraph.Node;
import ddd.transform.Transform;
import ddd.transform.affine.GeneralRotation;
import ddd.transform.affine.RotationY;
import ddd.transform.affine.Scale;
import ddd.transform.affine.Translation;
import ddd.visualize.SceneRenderFrame;
import ddd.visualize.SceneRenderPanel;

@SuppressWarnings("serial")
public class VpAnimation extends SceneRenderPanel {

	public VpAnimation() {
		super();
	}

	@Override
	protected void createScene() {		
		// Universum, Szene, root
		scene = new Node();
		// zus. Knoten unterhalb des root-Node einfügen, dessen Transform wir animieren wollen
		Node base = new Node();
		scene.addChild(base);
		
		Node street = new Node();
		Plane plane = new Plane();
		street.addChild(new Scale(10.0, 1.0, 1.0), plane);
		street.addChild(new Scale(1.0, 1.0, 10.0), plane);
		base.addChild(street);
		House house = new House();
		base.addChild(new Translation(3.0, 0.0, 4.0), house);

		// TODO: Weitere Objekte zur Szene hinzufügen...

		
		base.addActivity(new Activity(base) {
			private double angle = 0.0;
			private double a1 = 0.0;
			private double a2 = 0.0;
			private double a3 = 1.0;

			@Override
			public void update() {
				Transform animation = new Scale(0.1);
								
				animation.rightMult(new GeneralRotation(0.0, 0.0, 0.0, a1, a2, a3, angle));
				angle += 0.004;
				a1 = Math.sin(angle);
				a2 = Math.sin(2.0 * angle);
				a3 = Math.cos(angle);
				
				node.setTransform(animation);
			}
		});
	}

	@Override
	protected void setupRenderer() {
		renderer = new SimplePerspectiveRenderer(new Vector3(0.0, 0.0, -1.2), new Vector3(0.0, 0.0, 0.0),
				new Vector3(0.0, 1.0, 0.0), 90.0, 0.1, 1000.0, width, height);
	}

	public static void main(String[] args) {
		VpAnimation vpViewer = new VpAnimation();
		@SuppressWarnings("unused")
		SceneRenderFrame sceneRenderFrame = new SceneRenderFrame(vpViewer);
	}

}
