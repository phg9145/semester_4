
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ddd.transform.Transform;
import ddd.transform.vp.DisplayTransform;
import ddd.transform.vp.PerspectiveTransform;
import ddd.transform.vp.ViewTransform;
import ddd.Vector3;
import ddd.render.SimplePerspectiveRenderer;

public class VpTransformsTest {
	@Test
	public void testDisplayTransform() {
		//@formatter:off
		Transform soll1 = new Transform(new double[][] { 
			{ 50.0, 0.0, 0.0, 50.0 }, 
			{ 0.0, -50.0, 0.0, 50.0 },
			{ 0.0, 0.0, 0.0, 0.0 }, 
			{ 0.0, 0.0, 0.0, 1.0 }, 
			});
		//@formatter:on
		Transform ist1 = new DisplayTransform(100, 100);
		assertEquals("Bildschirmtrafo für xmax=100, ymax=100 sollte sein:\n" + soll1 + "ist:\n" + ist1, 0.0,
				Transform.maxNorm(soll1, ist1), 1.0e-12);

		//@formatter:off
		Transform soll2 = new Transform(new double[][] { 
			{ 960.0, 0.0, 0.0, 960.0 }, 
			{ 0.0, -540.0, 0.0, 540.0 },
			{ 0.0, 0.0, 0.0, 0.0 }, 
			{ 0.0, 0.0, 0.0, 1.0 }, 
			});
		//@formatter:on
		Transform ist2 = new DisplayTransform(1920, 1080);
		assertEquals("Bildschirmtrafo für xmax=1920, ymax=1080 sollte sein:\n" + soll2 + "ist:\n" + ist2, 0.0,
				Transform.maxNorm(soll2, ist2), 1.0e-12);
	}

	@Test
	public void testViewTransform() {
		//@formatter:off
		Transform soll1 = new Transform(new double[][] { 
			{ -1.0, 0.0, 0.0, 0.0 }, 
			{ 0.0, 1.0, 0.0, 0.0 },
			{ 0.0, 0.0, -1.0, -1.0 }, 
			{ 0.0, 0.0, 0.0, 1.0 }, });
		//@formatter:on
		Vector3 cameraPos1 = new Vector3(0.0, 0.0, -1.0);
		Vector3 lookAtPoint1 = new Vector3(0.0, 0.0, 0.0);
		Vector3 upVector1 = new Vector3(0.0, 1.0, 0.0);
		Transform ist1 = new ViewTransform(cameraPos1, lookAtPoint1, upVector1);
		assertEquals(
				"Ansichtstransformation für Kamera-Position " + cameraPos1 + ", LookAt-Punkt " + lookAtPoint1
						+ ", Obenvektor " + upVector1 + " sollte sein:\n" + soll1 + "ist:\n" + ist1,
				0.0, Transform.maxNorm(soll1, ist1), 1.0e-12);
		//@formatter:off
		Transform soll2 = new Transform(new double[][] { 
					{ 0.17609018126512474, -0.17609018126512474, 0.9684959969581861, -1.3206763594884356 },
					{ 0.644318317461068, 0.7644454613944875, 0.021841298896985356, 0.0982858450364342 },
					{ -0.7442084075352509, 0.6201736729460423, 0.24806946917841693, -6.9459451369956735 },
					{ 0.0, 0.0, 0.0, 1.0 }, 
					});
		//@formatter:on
		Vector3 cameraPos2 = new Vector3(-5.0, 4.0, 3.0);
		Vector3 lookAtPoint2 = new Vector3(1.0, -1.0, 1.0);
		Vector3 upVector2 = new Vector3(1.0, 1.0, 0.0);
		Transform ist2 = new ViewTransform(cameraPos2, lookAtPoint2, upVector2);
		assertEquals(
				"Ansichtstransformation für Kamera-Position " + cameraPos2 + ", LookAt-Punkt " + lookAtPoint2
						+ ", Obenvektor " + upVector2 + " sollte sein:\n" + soll2 + "ist:\n" + ist2,
				0.0, Transform.maxNorm(soll2, ist2), 1.0e-12);
	}

	@Test
	public void testPerspectiveTransforms() {
		//@formatter:off
		Transform soll1 = new Transform(new double[][] { 
			{ -1.0, 0.0, 0.0, 0.0 }, 
			{ 0.0, -1.0, 0.0, 0.0 },
			{ 0.0, 0.0, -3.0, 4.0 }, 
			{ 0.0, 0.0, -1.0, 0.0 }, });
		//@formatter:on
		double left1 = -1.0;
		double right1 = 1.0;
		double bottom1 = -1.0;
		double top1 = 1.0;
		double near1 = -1.0;
		double far1 = -2.0;
		Transform ist1 = new PerspectiveTransform(left1, right1, bottom1, top1, near1, far1);
		assertEquals(
				"Perspektivische Transformation für (l,r,b,t,n,f) = (" + left1 + ", " + right1 + ", " + bottom1 + ", "
						+ top1 + ", " + near1 + ", " + far1 + ") sollte sein:\n" + soll1 + "ist:\n" + ist1,
				0.0, Transform.maxNorm(soll1, ist1), 1.0e-12);
		//@formatter:off
		Transform soll2 = new Transform(new double[][] { 
			{ -0.3333333333333333, 0.0, 1.0, 0.0 },
			{ 0.0, -0.3333333333333333, -0.3333333333333333, 0.0 },
			{ 0.0, 0.0, -1.105263157894737, 1.0526315789473684 }, 
			{ 0.0, 0.0, -1.0, 0.0 }, });
		//@formatter:on
		double left2 = 0.0;
		double right2 = 3.0;
		double bottom2 = -2.0;
		double top2 = 1.0;
		double near2 = -0.5;
		double far2 = -10.0;
		Transform ist2 = new PerspectiveTransform(left2, right2, bottom2, top2, near2, far2);
		assertEquals(
				"Perspektivische Transformation für (l,r,b,t,n,f) = (" + left2 + ", " + right2 + ", " + bottom2 + ", "
						+ top2 + ", " + near2 + ", " + far2 + ") sollte sein:\n" + soll2 + "ist:\n" + ist2,
				0.0, Transform.maxNorm(soll2, ist2), 1.0e-12);

	}

	@Test
	public void testViewingPipeline() {
		//@formatter:off
		Transform soll = new Transform( new double[][] {
			{-1000.256235, -181.868626, 296.634147, 1660.627635 },
			{-61.621770,-989.954031,-151.941845,1889.587987 },
			{  0.000000,  0.000000,  0.000000,  0.000000 },
			{ -0.169031, -0.507093,  0.845154,  2.028370 },
		});
		//@formatter:on
		SimplePerspectiveRenderer simplePerspectiveRenderer = new SimplePerspectiveRenderer(new Vector3(1.0, 2.0, -1.0),
				new Vector3(0.0, -1.0, 4.0), new Vector3(0.1, 1.0, -0.1), 45.0, 0.1, 100.0, 1024, 768);
		assertEquals(
				"Gesamtmatrix der Viewing Pipeline sollte sein:\n" + soll + "ist\n"
						+ simplePerspectiveRenderer.getRenderTransform(),
				0.0, Transform.maxNorm(soll, simplePerspectiveRenderer.getRenderTransform()), 1e-6);
	}
}
