package ddd.models.wavefront;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import ddd.Triangle;
import ddd.Vector3;
import ddd.models.Shape;
import ddd.models.TriangleArray;

/**
 * Lade eine Wavefront-Date in ein Shape-Objekt, genauer ein TriangleArray.
 */
public class WavefrontLoader {

	/**
	 * Eigentliche Laderoutine
	 * 
	 * @param fileName Name der *.obj-Datei
	 * @return TriangleArray mit Dreiecks-Facetten
	 * @throws FileNotFoundException falls Datei nicht gefunden
	 */
	public static Shape loadFromFile(String fileName) throws FileNotFoundException {
		// Ihr Code hier...
	    List<String> allFileLines = null;
	    try {
            allFileLines = Files.readAllLines(Paths.get(fileName));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            throw new FileNotFoundException();
        }
	    
	    for(String s : allFileLines) {
	        System.out.println(s);
	    }
	    
	    ArrayList<String> fStringsToAdd = new ArrayList<String>();
        
        ArrayList<Vector3> vector3List = new ArrayList<Vector3>();
        ArrayList<Triangle> triangleList = new ArrayList<Triangle>();
	    
	    for(String s : allFileLines) {
	        String[] sp = s.split(" ");
	        if(s.startsWith("v ")) {
	            
	            int c = 0;

	            Vector3 v = new Vector3(Double.parseDouble(sp[1]), Double.parseDouble(sp[2]), Double.parseDouble(sp[3]));
	            vector3List.add(v);
	        }
	        else if(s.startsWith("f")) {
	            fStringsToAdd.add(s);
	        }
	    }
	    
	    
	    for(String s : fStringsToAdd) {
	        String[] sp = s.split(" ");
	        
	        
	        if(s.contains("/")) {
	            for(int i = 1; i <= 3; i++) {
	                sp[i] = sp[i].split("/")[0];
	            }
	        }
	        
	        Vector3 v1 = vector3List.get(Integer.parseInt(sp[1])-1);
            Vector3 v2 = vector3List.get(Integer.parseInt(sp[2])-1);
            Vector3 v3 = vector3List.get(Integer.parseInt(sp[3])-1);
            
            Triangle t = new Triangle(v1, v2, v3);
            triangleList.add(t);
	    }
	    
	    return new TriangleArray(triangleList);
	}
}
