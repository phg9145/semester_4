\babel@toc {german}{}
\contentsline {section}{\numberline {1}2D-Grafik und Rasterisierung}{5}%
\contentsline {subsection}{\numberline {1.1}Koordinatensysteme}{5}%
\contentsline {subsubsection}{\numberline {1.1.1}Darstellung eines Punkts}{5}%
\contentsline {subsubsection}{\numberline {1.1.2}Rasterisierung von Punkten}{5}%
\contentsline {subsubsection}{\numberline {1.1.3}Transformation LKOS $\rightarrow $ GKOS}{5}%
\contentsline {subsubsection}{\numberline {1.1.4}Rundung}{6}%
\contentsline {subsection}{\numberline {1.2}Liniendarstellung}{6}%
\contentsline {subsubsection}{\numberline {1.2.1}Mathematische Geradengleichung}{6}%
\contentsline {subsubsection}{\numberline {1.2.2}Bresenham-Algorithmus}{7}%
\contentsline {subsubsection}{\numberline {1.2.3}Anmerkungen}{7}%
\contentsline {subsection}{\numberline {1.3}(Anti-)Aliasing}{7}%
\contentsline {subsection}{\numberline {1.4}Polygonrasterung und -füllung}{9}%
\contentsline {subsubsection}{\numberline {1.4.1}Polygonrasterung}{9}%
\contentsline {subsubsection}{\numberline {1.4.2}Polygonfüllung}{10}%
\contentsline {subsection}{\numberline {1.5}Clipping}{11}%
\contentsline {subsubsection}{\numberline {1.5.1}Linien-Clipping an Rechteck mit Cohen-Sutherland}{11}%
\contentsline {subsubsection}{\numberline {1.5.2}Clipping an Polygonen nach Liang-Barsky}{12}%
\contentsline {subsection}{\numberline {1.6}Affine Transformationen}{13}%
\contentsline {subsection}{\numberline {1.7}Angewandte rechnergestützte Geometrie}{15}%
\contentsline {section}{\numberline {2}Graustufen- und Farbdarstellung}{16}%
\contentsline {subsection}{\numberline {2.1}Graustufendarstellung}{16}%
\contentsline {subsection}{\numberline {2.2}Farbmodelle}{17}%
\contentsline {subsubsection}{\numberline {2.2.1}RGB-Modell}{17}%
\contentsline {subsubsection}{\numberline {2.2.2}CMY-Modell}{17}%
\contentsline {subsubsection}{\numberline {2.2.3}Zusammenhang RGB $\leftrightarrow $ CMY}{17}%
\contentsline {subsubsection}{\numberline {2.2.4}CMYK-Modell}{17}%
\contentsline {subsubsection}{\numberline {2.2.5}HSV-Modell}{18}%
\contentsline {subsection}{\numberline {2.3}Berechnungen mit RGB-Werten, Speicherung}{18}%
\contentsline {subsubsection}{\numberline {2.3.1}Rechnen mit RGB-Farbwerten}{18}%
\contentsline {subsubsection}{\numberline {2.3.2}Clamping/Lineare Abbildung}{18}%
\contentsline {subsubsection}{\numberline {2.3.3}Speicherung der RGB-Farbinformation}{18}%
\contentsline {section}{\numberline {3}Kurven}{19}%
\contentsline {subsection}{\numberline {3.1}Bézier-Kurven}{21}%
\contentsline {subsection}{\numberline {3.2}B-Splines}{26}%
\contentsline {subsubsection}{\numberline {3.2.1}Periodische uniforme B-Splinefunktionen}{27}%
\contentsline {subsubsection}{\numberline {3.2.2}Nicht-Periodische uniforme B-Splinefunktionen}{28}%
\contentsline {subsubsection}{\numberline {3.2.3}Bézier-Kurven}{28}%
\contentsline {subsubsection}{\numberline {3.2.4}Genereller Einfluss des Knotenvektors}{29}%
\contentsline {subsection}{\numberline {3.3}Non Uniform Rational B-Splines (NURBS)}{29}%
\contentsline {section}{\numberline {4}3D-Modellierung}{30}%
\contentsline {subsection}{\numberline {4.1}Szenengraph}{30}%
\contentsline {subsection}{\numberline {4.2}Modellierungsarten}{31}%
\contentsline {subsection}{\numberline {4.3}Volumenmodellierung}{31}%
\contentsline {subsubsection}{\numberline {4.3.1}Normzellen}{31}%
\contentsline {subsubsection}{\numberline {4.3.2}Octrees (dt. Oktalbäume)}{32}%
\contentsline {subsubsection}{\numberline {4.3.3}Constructive Solid Geometry (CSG)}{33}%
\contentsline {subsection}{\numberline {4.4}Kanten- und Oberflächenmodellierung}{34}%
\contentsline {subsubsection}{\numberline {4.4.1}Drahtmodell}{34}%
\contentsline {subsubsection}{\numberline {4.4.2}Oberflächendarstellung (BRep)}{35}%
\contentsline {subsection}{\numberline {4.5}Hybridschemata}{36}%
\contentsline {subsection}{\numberline {4.6}Sweep-Körper / Extrusion}{36}%
\contentsline {section}{\numberline {5}Oberflächendarstellung}{37}%
\contentsline {subsection}{\numberline {5.1}Topologische Struktur}{37}%
\contentsline {subsubsection}{\numberline {5.1.1}Flächen-Knoten-Speicherung}{37}%
\contentsline {subsubsection}{\numberline {5.1.2}Kantenspeicherung}{38}%
\contentsline {subsubsection}{\numberline {5.1.3}Winged-Edge-Repräsentation}{39}%
\contentsline {subsubsection}{\numberline {5.1.4}Half-Edge}{40}%
\contentsline {subsection}{\numberline {5.2}Euler-Operationen}{40}%
\contentsline {subsubsection}{\numberline {5.2.1}Polyedersatz}{40}%
\contentsline {subsubsection}{\numberline {5.2.2}Euler-Operationen}{40}%
\contentsline {subsection}{\numberline {5.3}Flächen}{41}%
\contentsline {subsubsection}{\numberline {5.3.1}Polygone}{41}%
\contentsline {subsubsection}{\numberline {5.3.2}Bilineare Interpolation}{41}%
\contentsline {subsubsection}{\numberline {5.3.3}Coon's Patch}{41}%
\contentsline {subsubsection}{\numberline {5.3.4}Freiformflächen}{42}%
\contentsline {section}{\numberline {6}Transformationen und Projektionen}{42}%
\contentsline {subsection}{\numberline {6.1}Koordinatensysteme}{43}%
\contentsline {subsection}{\numberline {6.2}Affine Transformationen}{44}%
\contentsline {subsubsection}{\numberline {6.2.1}Einfache affine Transformation}{44}%
\contentsline {subsubsection}{\numberline {6.2.2}Rotation um eine Achse im Raum}{47}%
\contentsline {subsection}{\numberline {6.3}Alternative Darstellungen von Rotationen}{47}%
\contentsline {subsubsection}{\numberline {6.3.1}Quaternionen}{47}%
\contentsline {subsubsection}{\numberline {6.3.2}Rotationsmatrizen}{48}%
\contentsline {subsection}{\numberline {6.4}Perspektivische Abbildung}{48}%
\contentsline {subsection}{\numberline {6.5}Projektive Abbildungen}{49}%
\contentsline {section}{\numberline {7}3D zu 2D}{50}%
\contentsline {subsection}{\numberline {7.1}Ansichtstransformation}{50}%
\contentsline {subsubsection}{\numberline {7.1.1}Sichtkoordinatensystem}{51}%
\contentsline {subsubsection}{\numberline {7.1.2}Überführung Weltkoordinaten $\rightarrow $ Sichtkoordinaten}{52}%
\contentsline {subsection}{\numberline {7.2}Projektionstransformation}{52}%
\contentsline {subsubsection}{\numberline {7.2.1}Orthogonalprojektion}{53}%
\contentsline {subsubsection}{\numberline {7.2.2}Perspektivische Projektion}{55}%
\contentsline {subsection}{\numberline {7.3}Clipping / Normalisierung}{57}%
\contentsline {subsection}{\numberline {7.4}Bildschirmtransformation}{58}%
\contentsline {section}{\numberline {8}Sichtbarkeitsentscheid}{59}%
\contentsline {subsection}{\numberline {8.1}3D-Clipping}{59}%
\contentsline {subsection}{\numberline {8.2}Rückseitenentfernung}{59}%
\contentsline {subsubsection}{\numberline {8.2.1}Variante 1}{59}%
\contentsline {subsubsection}{\numberline {8.2.2}Variante 2}{60}%
\contentsline {subsection}{\numberline {8.3}Tiefenpuffer-Algorithmus}{60}%
\contentsline {subsection}{\numberline {8.4}List-Priority-Verfahren}{62}%
\contentsline {subsection}{\numberline {8.5}Raycasting}{62}%
\contentsline {subsubsection}{\numberline {8.5.1}Grundlagen}{62}%
\contentsline {subsubsection}{\numberline {8.5.2}Beschleunigung}{63}%
\contentsline {section}{\numberline {9}Beleuchtung und Schattierung}{63}%
\contentsline {subsection}{\numberline {9.1}Lokale Beleuchtung}{63}%
\contentsline {subsubsection}{\numberline {9.1.1}Ambiente Reflexion}{64}%
\contentsline {subsubsection}{\numberline {9.1.2}Punktförmige Lichtquellen mit diffuser Reflexion}{64}%
\contentsline {subsubsection}{\numberline {9.1.3}Spekulare Reflexion}{65}%
\contentsline {subsubsection}{\numberline {9.1.4}Kombination der drei Beleuchtungsarten}{66}%
\contentsline {subsubsection}{\numberline {9.1.5}Entfernungsabhängigkeit und Nebel}{66}%
\contentsline {subsection}{\numberline {9.2}Schattierung}{67}%
\contentsline {subsubsection}{\numberline {9.2.1}Konstante Schattierung (flat shading)}{67}%
\contentsline {subsubsection}{\numberline {9.2.2}Interpolierte Schattierung}{68}%
\contentsline {subsection}{\numberline {9.3}Transparenz}{69}%
\contentsline {subsubsection}{\numberline {9.3.1}Transparenz ohne Brechung des Lichts}{69}%
\contentsline {subsubsection}{\numberline {9.3.2}Transparenz mit Brechung des Lichts}{70}%
\contentsline {section}{\numberline {10}Globale Beleuchtungsmodelle}{71}%
\contentsline {subsection}{\numberline {10.1}Raytracing}{71}%
\contentsline {subsection}{\numberline {10.2}Andere strahlenbasierte Verfahren}{73}%
\contentsline {subsubsection}{\numberline {10.2.1}Photon Mapping}{73}%
\contentsline {subsubsection}{\numberline {10.2.2}Ambient Occlusion}{73}%
\contentsline {subsubsection}{\numberline {10.2.3}Image Based Lighting}{73}%
\contentsline {subsection}{\numberline {10.3}Radiosity-Verfahren}{74}%
\contentsline {subsubsection}{\numberline {10.3.1}Berechnung für Strahlungswerte}{74}%
\contentsline {subsubsection}{\numberline {10.3.2}Berechnung der Formfaktoren}{74}%
\contentsline {subsubsection}{\numberline {10.3.3}Darstellung des Bildes}{75}%
\contentsline {subsubsection}{\numberline {10.3.4}Vergleich Raytracing / Radiosity}{75}%
